#!/bin/sh
#
# Tests for the MOVE protocol command.
#
MAX_TESTS=10

. $AM_SRCDIR/common.sh
. $AM_SRCDIR/import-common.sh

init_tests() {
    do_import list
    cat >$OUTDIR/config << EOF
[global]
log_level=9
#enable_logging=true
invoking_user=$USER
[move]
passphrase_file=$WDIR/passphrase.key
EOF
    mv -f data/list data/move || bail_out "Could not move data/list."
    launch_pwmd move
}

test_1() {
    run_pwmc "move" $DEVNULL <<EOF
MOVE a a
EOF
    test_failure $test_n $? 536903715 "Fail to move src to src."
}

test_2() {
    run_pwmc "move" $DEVNULL <<EOF
MOVE non-existant a
EOF
    test_failure $test_n $? 536871048 "Fail to move non-existent element."
}

test_3() {
    run_pwmc "move" $DEVNULL <<EOF
MOVE a b	c
EOF
    test_failure $test_n $? 536903715 "Fail to move child with target to root of same node."
}

test_4() {
    run_pwmc "-S move" $DEVNULL <<EOF
MOVE a	c a	b	c	d	e
EOF
    list_recurse move
    test_result $test_n $? move "Move child to sibling child with target."
}

test_5() {
    run_pwmc "-S move" $DEVNULL <<EOF
MOVE a	b	c	d	e	c a
EOF
    list_recurse move
    test_result $test_n $? move "Move child with target to parent sibling."
}

test_6() {
    run_pwmc "-S move" $DEVNULL <<EOF
MOVE a	b	c	d 
EOF
    list_recurse move
    test_result $test_n $? move "Overwrite existing root tree."
}

test_7() {
    run_pwmc "-S move" $DEVNULL <<EOF
MOVE d c	e
EOF
    list_recurse move
    test_result $test_n $? move "Show child elements whose target contains an error."
}

test_8() {
    run_pwmc "-S move" $DEVNULL <<EOF
MOVE x z
EOF
    list_recurse move
    test_result $test_n $? move "Move node whose target value is a document root node."
}

test_9() {
    run_pwmc "-S move" $DEVNULL <<EOF
MOVE z	x	y z
EOF
    list_recurse move
    test_result $test_n $? move "Move node whose target value is a parent."
}

test_10() {
    run_pwmc "-S move" $DEVNULL <<EOF
MOVE a	c c	f
EOF
    list_recurse move
    test_result $test_n $? move "Create new path from non-existing dest."
}

run_tests $@
