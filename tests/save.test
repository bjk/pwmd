#!/bin/sh
#
# Tests for the SAVE protocol command.
#
MAX_TESTS=10

. $AM_SRCDIR/common.sh

init_tests() {
    launch_pwmd
}

test_1() {
    $PWMD --homedir $OUTDIR --import $WDIR/datafile.xml -o data/save \
	--userid "Pwmd SAVE test" --passphrase-file /dev/null 2>/dev/null
    test_result $test_n $? "Import from command line without passphrase."
}

test_2() {
    run_pwmc "save" >result $DEVNULL <<EOF
KEYINFO
EOF
    e=$?
    if [ $e -eq 0 -a -s "result" ]; then
	SIGN_KEYID="`grep '^S' result | cut -b 2-`"
	mv -f result save.result1
	echo -n $SIGN_KEYID >save.result2
    else
	e=1
    fi
    test_result $test_n $e "Obtain encryption and signing key ID's."
}

test_3() {
    $PWMD --homedir $OUTDIR --import $WDIR/datafile.xml -o data/sym \
	--symmetric --sign-keyid=$SIGN_KEYID \
	--passphrase-file $WDIR/passphrase.key
    test_result $test_n $? "Import symmetric with key file and sign using keyid $SIGN_KEYID."
}

test_4() {
    run_pwmc "--key-file $WDIR/passphrase.key sym" >result $DEVNULL <<EOF
KEYINFO
EOF
    e=$?
    if [ $e -eq 0 -a -s "result" ]; then
	SIGN_KEYID="`grep '^S' result | cut -b 2-`"
	echo -n $SIGN_KEYID >result
    else
	e=1
    fi
    cp -f save.result2 save.result4 || bail_out "Could not copy save.result2."
    test_result $test_n $e save "Compare symmetric signing key ID."
}

test_5() {
    run_pwmc "-S --key-file $WDIR/passphrase.key \
	--new-key-file $WDIR/passphrase.key sym" $DEVNULL <<EOF
NOP
EOF
    test_result $test_n $? "Save symmetric and sign using key ID $SIGN_KEYID."
}

test_6() {
    run_pwmc "--key-file $WDIR/passphrase.key sym" >result $DEVNULL <<EOF
KEYINFO
EOF
    e=$?
    if [ $e -eq 0 -a -s "result" ]; then
	SIGN_KEYID="`grep '^S' result | cut -b 2-`"
	echo -n $SIGN_KEYID >result
    else
	e=1
    fi
    cp -f save.result2 save.result6 || bail_out "Could not copy save.result2."
    test_result $test_n $e save "Compare symmetric signing key ID."
}

test_7() {
    KEYID="`grep '^[^S]' save.result1`"
    SIGN_KEYID="`grep '^S' save.result1 | cut -b 2-`"
    $PWMD --homedir $OUTDIR --import $WDIR/datafile.xml -o data/save \
	--keyid=$KEYID --sign-keyid=$SIGN_KEYID 2>/dev/null
    test_result $test_n $? "Import with existing keyid $KEYID and sign using $SIGN_KEYID."
}

test_8() {
    run_pwmc "save" >result $DEVNULL <<EOF
KEYINFO
EOF
    e=$?
    cp -f save.result1 save.result8 || bail_out "Could not copy save.result1."
    test_result $test_n $e save "Compare encryption and signing keys."
}

test_9() {
    run_pwmc "-S save" $DEVNULL <<EOF
NOP
EOF
    test_result $test_n $? "Save to the original key ID's."
}

test_10() {
    run_pwmc "save" >result $DEVNULL <<EOF
KEYINFO
EOF
    e=$?
    cp -f save.result1 save.result10 || bail_out "Could not copy save.result1."
    test_result $test_n $e save "Compare encryption and signing keys."
}

run_tests $@
