bail_out() {
    echo "Bail out! $@"
    exit 1
}

skip_test() {
    echo "1..0 # SKIP $@"
    exit 0
}

on_exit() {
    if [ $PID ]; then
	kill $PID 2>/dev/null
    fi

    agent_socket="`gpgconf --homedir $OUTDIR/.gnupg --list-dirs | grep '^agent-socket:' | awk -F: '{print $2}'`"
    if [ -S "$agent_socket" ]; then
	agent_pid="`echo GETINFO pid | gpg-connect-agent -S $agent_socket 2>/dev/null`"
	agent_pid="`echo $agent_pid | awk '{print $2}' | sed -ne '1p'`"
	if [ $agent_pid ]; then
	    kill $agent_pid 2>/dev/null
	fi
	unset agent_pid
    fi

    rm -f config

    if [ x"$DEBUG" = "x" ]; then
	rm -f pwmc.out
    fi
}

wait_for_socket() {
    local pid="$1"
    local socket="$2"
    local remaining=10
    local abort=0

    until [ -S $OUTDIR/$socket ]; do
	kill -0 $pid 2>/dev/null || bail_out "Could not launch pwmd"
	if [ $remaining -eq 0 ]; then
	    bail_out "Could not connect to pwmd socket"
	fi

	echo "# Waiting for socket. Timeout in ${remaining}s"
	remaining=$((remaining - 1))
	sleep 1
    done

    unset pid
    unset socket
    unset remaining
}

launch_pwmd () {
    args="--homedir $OUTDIR -n $@"

    if [ x"$TESTS" != "x" -o x"$DEBUG" = "x" ]; then
	$PWMD $args 2>/dev/null >/dev/null &
    else
	$PWMD $args &
    fi

    PID=$!
    wait_for_socket $PID socket
}

begin_test() {
    test_n=$(($1 + 1))
}

test_result() {
    e=$2
    msg="$3"

    if [ $# -eq 4 -a $e -eq 0 ]; then
	if [ "$3" = "save" ]; then
	    # The SAVE tests generate dynamic results. The following is needed
	    # for 'make distcheck'.
	    cmp $OUTDIR/${3}.result${test_n} result
	else
	    cmp $WDIR/${3}.result${test_n} result
	fi
	e=$?
	rm -f result
	msg="$4"
    fi

    test_success $1 $e "$msg"
}

test_success() {
    if [ $2 -ne 0 ]; then
	echo "not ok $1 - $3"
	return 1
    else
	echo "ok $1 - $3"
	return 0
    fi
}

test_failure() {
    if [ $# -eq 4 ]; then
	desc="$4"
    else
	desc="$3"
    fi

    if [ $2 -eq 0 ]; then
	echo "# Return code indicated success but should have failed."
	echo "not ok $2 - $desc"
	return 1
    fi

    rc=0
    if [ $# -eq 4 -a x"$TESTS" != "x" -o x"$DEBUG" != "x" ]; then
	if [ -s "pwmc.out" ]; then
	    rc="`grep '^ERR [0-9]*:' < pwmc.out | cut -d ' ' -f 2 | cut -b -9`"
	    if [ "$rc" != "$3" ]; then
		rc=1
	    else
		rc=0
	    fi
	else
	    rc=1
	fi
    fi

    test_success $1 $rc "$desc"
}

run_tests() {
    test_n=0
    if [ "$1" = "-h" ]; then
	echo "Run all or the specified tests from 1 to $MAX_TESTS. Note that" \
	    "some tests depend on"
        echo "the result of previous tests."
	echo "Usage: $0 [testN] [...]"
	return 0
    fi

    init_tests

    if [ $# -eq 0 ]; then
	for t in $(seq 1 $MAX_TESTS); do
	    begin_test $test_n
	    test_${t}
	done
    else
	local ret=0
	for t in $@; do
	    if [ $t -gt $MAX_TESTS ]; then
		echo "$0: No such test $t." 1>&2
		exit 1
	    fi
	    test_n=$t
	    test_${t}
	    r=$?
	    if [ $ret -eq 0 ]; then
		ret=$r
	    fi
	done
	exit $ret
    fi
}

# Common in some tests.
list_recurse() {
    e=$?
    if [ $e -eq 0 ]; then
	$PWMC $PWMC_ARGS $1 >result $DEVNULL <<EOF
LIST --recurse $2
EOF
	e=$?
    fi
    return $e
}

run_pwmc() {
    eval "pwmc $PWMC_ARGS $@"
}

WDIR="$AM_SRCDIR"
OUTDIR="$AM_BUILDDIR"
PWMD="../src/pwmd"
PWMC="pwmc"
PWMC_ARGS="--url $OUTDIR/socket"
PID=
DEVNULL=

$PWMC --version >/dev/null
if [ $? != 0 ]; then
    skip_test "Could not run $PWMC. Please make sure libpwmd is installed."
fi

if [ x"$TESTS" = "x" -a x"$DEBUG" = "x" ]; then
    PWMC_ARGS="$PWMC_ARGS --quiet"
    DEVNULL="2>/dev/null"
else
    DEVNULL="2>pwmc.out"
fi

trap on_exit EXIT

if [ $WDIR != $OUTDIR ]; then
    cp -Rf $WDIR/.gnupg $OUTDIR 2>/dev/null \
	|| bail_out "Could not copy .gnupg."
    chmod -R u+w $OUTDIR/.gnupg
fi

cp -f $WDIR/.gnupg/trustdb.gpg.dist $OUTDIR/.gnupg/trustdb.gpg 2>/dev/null \
    || bail_out "Could not copy trustdb.gpg."
cp -f $WDIR/.gnupg/pubring.kbx.dist $OUTDIR/.gnupg/pubring.kbx 2>/dev/null \
    || bail_out "Could not copy pubring.kbx."

echo "1..${MAX_TESTS}"
