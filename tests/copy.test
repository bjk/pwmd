#!/bin/sh
#
# Tests for the COPY protocol command.
#
MAX_TESTS=6

. $AM_SRCDIR/common.sh
. $AM_SRCDIR/import-common.sh

init_tests() {
    do_import list
    cat >$OUTDIR/config << EOF
[global]
invoking_user=$USER
log_level=9
#enable_logging=true
[copy]
passphrase_file=$WDIR/passphrase.key
EOF
    mv -f data/list data/copy || bail_out "Could not move data/list."
    launch_pwmd copy
}

test_1() {
    run_pwmc "-S copy" $DEVNULL <<EOF
COPY a a
EOF
    test_failure $test_n $? 536903715 "Fail to copy element to self."
}

test_2() {
    run_pwmc "-S copy" $DEVNULL <<EOF
COPY a new
EOF
    list_recurse copy
    test_result $test_n $? copy "Copy element to new root."
}

test_3() {
    run_pwmc "-S copy" $DEVNULL <<EOF
COPY new a	new
EOF
    list_recurse copy
    test_result $test_n $? copy "Copy element to new child."
}

test_4() {
    run_pwmc "-S copy" $DEVNULL <<EOF
COPY new a
EOF
    list_recurse copy
    test_result $test_n $? copy "Copy element to root and overwrite."
}

test_5() {
    run_pwmc "-S copy" $DEVNULL <<EOF
COPY new a	b
EOF
    list_recurse copy
    test_result $test_n $? copy "Copy element to child and overwrite."
}

test_6() {
    run_pwmc "-S copy" $DEVNULL <<EOF
COPY new b
EOF
    list_recurse copy
    test_result $test_n $? copy "Copy element to target."
}

run_tests $@
