pwmd v3.3.8
-----------
Fixed negating ACL entries.

ACL's now allow a command name in an element '_acl' attribute just as in the
'allowed' configuration parameter.

No longer attempt to do a backtrace upon crash. Instead write a crash log to
homedir/crash-[pid].log.

Check for /proc/PID/{file,exe} during build time to determine whether a system
supports command names in an ACL. And other portability fixes.

Fixed SAVE with recent gpgme versions.

Fixed changing the ownership of a new element (inherited _acl) when connecting
as an 'invoking_user'.


pwmd v3.3.7
-----------
Add the STATUS_MODIFIED status message to inform all clients that further
modifications will fail with a checksum error. This is sent after a SAVE
command succeeds.

A few build fixes and m4 macro updates.

Allow an underscore in the local username regex.

No longer require an opened data file during DELETKEY. Being an invoking_user
should be enough to use this command.


PWMD v3.3.6
-----------
A few potential multi-threading data race and deadlock fixes.

Portability fix.

A couple other bugfixes.


PWMD v3.3.5
-----------
Fixed security issue with the IMPORT command. It would previously allow
overwriting an element without permission of the current client.

A couple build and portability fixes.


PWMD v3.3.4
-----------
A COPY command bugfix.


PWMD v3.3.3
-----------
Fix the valid username regex to allow a username beginning with caps.

Add ./configure option --with-invoking-user with the default being "nobody".


PWMD v3.3.2
-----------
LIST command performance tweak.

Now inherits the _acl attribute of the parent element when creating a new
element. Use the new --no-inherit-acl option of the STORE command to prevent
it.


PWMD v3.3.1
-----------
Added configuration parameters tls_ca_file, tls_crl_file, tls_server_cert and
tls_server_key. The defaults are the same as previous versions.

Fixed "LIST --sexp" truncating attribute values containing a space.

A few build and other fixes. See ChangeLog for details.


PWMD v3.3.0
-----------
When an ACL for an element is the empty string, treat the element as having a
non-existent _acl attribute. Now also tests for valid characters in a UNIX
username in an _acl value.

Now appends the current git commit hash to the version string to aid in
development.

Fixed to allow for larger PID values to be able to properly terminate or start
another daemon process.

Reworked the tests to use the TAP harness. Run 'make check' rather then 'make
tests' to run the tests. See tests/README for details.

Disabled TLS1.1 and CBC cipher suites by default.

Fixed creating a Debian package with 'make deb'.

An "allowed" configuration parameter value may now contain a whitelist of
local client command names that are allowed to connect or open a data file.
This is Linux specific for now.

Added the "--sexp" option to the LIST command to show elements along with
their attributes in an s-expression format.

Releases are now signed using a new signing key. The fingerprint of the new
key is 6078FEB430EFA427499E6E78555B69666326961C and is cross-signed with my
new primary key which is cross-signed with my old primary key. I don't believe
the old keys to be compromised; it is only to rotate them and update to newer
standards.


PWMD v3.2.2
-----------
Disable gpg-agent caching of the key for symmetrically encrypted data files.
The gpg-agent options to do this were previously mentioned in the pwmd
documentation, but we will pass the option to gpgme explicitly to prevent
misuse.

Portability fix.


PWMD v3.2.1
-----------
Fixed creating a Debian package from a tarball.

Fixed a crash during recursion loop detection.

Fixed a crash in the LIST command.

Fixed the GENKEY command to work properly when passed --expire due to a typo.

Fixed the BULK command to sometimes not work when not used as an --inquire.

Fixed the cache potentially returning a stale document.

Fixed the OPEN command to update the checksum for a reopened file.

Fixed a bug in the STORE command refusing the create an element path even with
proper access rights.


PWMD v3.2
---------
Add the BULK protocol command to allow sending multiple commands using an
(semi) s-expression syntax. This can speed up remote connections quite a bit
since less socket IO is needed. This also adds a BULK status message to inform
the client of the current bulk command being run which may be needed when a
command inquires data from the client.

No longer flood a TLS client with assuan protocol comment lines when spinning
in a read.

The _mtime and _ctime element attributes can no longer be modified or removed
by a client.

All known attributes to pwmd are now prefixed with an _:
    "target" -> "_target"
    "expire" -> "_expire"
    "expire_increment" -> "_age"

Added a "_version" attribute to the document root element to hold the current
version of pwmd.


PWMD v3.1.1
-----------
The XFER status message is now sent only once and before the transfer starts
leaving it to the client to calculate the amount of data transferred.

Added "LS --verbose" to include the filesystem atime, mtime and ctime
timestamps of data files.

Build, portability and other bug fixes. See ChangeLog for details.


PWMD v3.1
---------
The project has moved to GitLab (https://gitlab.com/bjk/pwmd/wikis). Downloads
are still available at SourceForge but the issue tracker, git repository and
wiki are now hosted at GitLab.

Re-added the "tls_use_crl" configuration parameter although it is disabled by
default.

DELETE no longer does any confirmation before deleting public and private
keys.

Portability and undefined behavior bugfixes. See ChangeLog for details.


PWMD v3.1-beta3
---------------
The LS command now sorts filenames.

Fixed a long standing memory leak related to a client's thread name.

Key expiration is now ignored when OPEN'ing a data file. The next SAVE will
fail if using an expired key. See docs for details about what to do.

The CACHETIMEOUT command now requires an opened data file and no longer
considers an "invoking_user". The syntax has also changed to require only a
timeout parameter.

Added GENKEY --no-expire to allow creating keys that do not expire.

GENKEY now requires an opened data file.

Bug fixes. See ChangeLog for details.


PWMD v3.1-beta2
---------------
New GENKEY command to generate a new keypair or subkey of an existing key
without saving the data file to disk.

Removed key generation altogether from the SAVE command. You must generate a
new key with the GENKEY command or use an existing key and pass the
fingerprint to the --keyid option.  The --sign-keyid option is also required
for new data files.

The SAVE command now allows only a single signer. Although, multiple
recipients may be specified.

LISTKEYS no longer requires an open data file.

Added configuration parameter "strict_open" to prevent a (non-invoking) client
from creating a new data file.

New command DELETEKEY to remove the private key associated with the currently
opened data file from the keyring.

Added copy-on-write for commands that modify the document.

The DUMP command no longer does a checksum test.


PWMD v3.1-beta1
---------------
Ported to libgpgme. Data files are now OpenPGP encrypted and signed. A signer
is required when using asymmetric encryption and optional if using symmetric
encryption.

New global configuration parameter "gpg_homedir". The default is
~/.pwmd/.gnupg which will spawn a gpg-agent process with this as it's homedir.
The secret and public keyrings live here. To use your regular keyring set this
to ~/.gnupg. This removes the "gpg_agent_socket" configuration parameter.

Added the pwmd-dump(1) utility to dump the contents of a v3.0.x data file. The
output file can then be imported by using 'pwmd --import'. pwmd-dump can also
update a v3.1 raw XML file to the latest element or attribute changes by
passing the --xml option. See the pwmd-dump(1) manual page for details.

Removed the notion of "literal" elements. Targets are always followed for
elements that have a 'target' attribute. This changes how the ATTR command
works. See the pwmd texinfo or html docs for details. Also note the inherited
and reserved attributes for elements with a target.

The LIST command now always implies the deprecated '--verbose' and
'--with-target' switches and adds the '--recurse' switch to replace the also
deprecated '--all' switch.

Rewrote the LIST command to better handle recursion loops. Still needs some
work, though. See KnownBugs.

New special attributes 'expire' and 'expire_increment'. See docs for details.
This also adds a new STATUS_EXPIRE status message.

New command LISTKEYS to show available (secret) keyid's.

New command KEYINFO to show encryption and signing keyid's for an opened data
file and --learn to create private key stubs for keys stored on a smartcard.

New global configuration parameters "encrypt_to" and "always_trust".

Added connected timestamp field to 'GETINFO --verbose CLIENTS'.

New status messages PASSPHRASE_HINT and PASSPHRASE_INFO when pinentry is
disabled.

New global configuration option "backlog" to set the TCP backlog for TLS
connections. The default is 128 (Linux default, too).

All clients now are cancelled upon SIGINT and SIGTERM without waiting for them
to disconnect themselves.

Added OPTION CLIENT-STATE to allow a client to opt-in to receive client STATE
status messages. By default, no client state is sent.

CACHETIMEOUT now allows access to an invoking user.

TLS rehandshake support upon SIGHUP.

New configuration option "tls_dh_params_file". This removes "tls_dh_level".

Removed configuration option "tcp_wait".

OPEN: Always allow an invoking user.

CLEARCACHE: Reworked to test the client ACL for each data file.
