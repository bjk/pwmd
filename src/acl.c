/*
    Copyright (C) 2006-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of pwmd.

    Pwmd is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.

    Pwmd is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Pwmd.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#include <sys/types.h>
#include <pwd.h>
#include <grp.h>

#include "common.h"
#include "acl.h"
#include "rcfile.h"
#include "util-misc.h"
#include "util-string.h"
#include "mutex.h"
#include "mem.h"
#ifdef WITH_GNUTLS
#include "tls.h"
#endif

static char *
acl_get_proc (pid_t pid, gpg_error_t *rc)
{
#ifdef WITH_PROC_SYMLINK
  char buf[64];
  char path[PATH_MAX];
  char *p;
  ssize_t n;

  /* Catch-all for systems that have a /proc/PID/{exe,file} symlink but do not
   * have a PID associated in assuan_peercred_t.
   */
  if (pid == ASSUAN_INVALID_PID)
    {
      *rc = 0;
      return NULL;
    }

  snprintf (buf, sizeof (buf), "/proc/%lu/%s", (long)pid, WITH_PROC_SYMLINK);
  n = readlink (buf, path, sizeof (path)-1);

  if (n == -1)
    {
      *rc = gpg_error_from_syserror ();
      log_write ("%s: %s", __FUNCTION__, gpg_strerror (*rc));
      return NULL;
    }

  *rc = 0;
  path[n] = 0;

  p = str_dup (path);
  if (!p)
    *rc = GPG_ERR_ENOMEM;

  return p;
#else
  *rc = 0;
  return NULL;
#endif
}

/* Check for further matches of a pathname which may be negated. */
static int
acl_check_proc_dup (char **users, const char *path, int not)
{
  char **p;
  int allowed = 0;

  for (p = users; p && *p; p++)
    {
      char *s = *p;

      if (*s == '!' || *s == '-')
        {
          not = 1;
          s++;
        }

      if (*s != '/')
        continue;

      if (!strcmp (s, path))
        allowed = !not;

      not = 0;
    }

  return allowed;
}

static int
acl_check_proc (char **users, char *user, const char *path, int *have_path)
{
  char *p = user;
  int not = 0;

  if (*p == '!' || *p == '-')
    {
      not = 1;
      p++;
    }

  if (*p != '/')
    return 2;

  *have_path = 1;
  if (!strcmp (user, path))
    return acl_check_proc_dup (users, user, not);

  return 0;
}

gpg_error_t
do_validate_peer (assuan_context_t ctx, const char *section,
		  assuan_peercred_t *peer, char **rpath)
{
  char **users;
  int allowed = 0;
  int have_user = 0;
  gpg_error_t rc;
  struct client_s *client = assuan_get_pointer (ctx);

  if (!client)
    return GPG_ERR_FORBIDDEN;

#ifdef WITH_GNUTLS
  if (client->thd->remote)
    return tls_validate_access (client, section);
#endif

  rc = assuan_get_peercred (ctx, peer);
  if (rc)
    return rc;

  users = config_get_list (section, "allowed");
  if (users)
    {
      char **cmds = NULL;

      for (char **p = users; !rc && *p; p++)
	{
          char *user = *p;
          int not = 0;

          if (*user == '-' || *user == '!')
            {
              not = 1;
              user++;
            }

          if (*user == '/')
            {
              if (strv_printf (&cmds, "%s%s", not ? "!" : "", user) == 0)
                {
                  strv_free (cmds);
                  strv_free (users);
                  return GPG_ERR_ENOMEM;
                }
            }
        }

      for (char **p = users; !rc && *p; p++)
	{
          char *user = *p;

          if (*user == '/' || (*user == '!' && *(user+1) == '/')
              || (*user == '-' && *(user+1) == '/'))
            continue;

          have_user = 1;
          rc = acl_check_common(client, user, (*peer)->uid, (*peer)->gid,
                                &allowed, NULL);
        }

      /* Skip getting process name from a UID that is not our own to prevent
       * an ENOENT error since most modern systems hide process details from
       * other UID's. Access will be determined based on other tests
       * (filesystem ACL to the socket, configuration, etc). */
      int exec_allowed = 0;
      int have_path = 0;
      int same_uid = (*peer)->uid == getuid();
      char *path = NULL;

      if (same_uid)
        {
          gpg_error_t prc = 0;

          if (!client->thd->cmdname)
            {
              path = acl_get_proc ((*peer)->pid, &prc);
              if (rpath)
                *rpath = path;
            }
          else
            path = client->thd->cmdname;

          if (!allowed || rc || prc)
            return allowed && !rc ? prc : rc ? rc : GPG_ERR_FORBIDDEN;

          for (char **p = users; !rc && path && *p; p++)
            {
              exec_allowed = acl_check_proc (users, *p, path, &have_path);
              if (!rc && exec_allowed == 1)
                break;
            }
        }

      strv_free (users);

      if (!rc && cmds && (allowed || !have_user) && same_uid)
        {
          allowed = 0;

          for (char **p = cmds; !rc && p && *p && !allowed; p++)
            {
              rc = acl_check_common (client, *p, client->thd->peer->uid,
                                     client->thd->peer->gid, &allowed, path);
            }
        }

      if (have_path)
        allowed = allowed && exec_allowed == 1;

      strv_free(cmds);
      if (!rpath && client->thd->cmdname != path)
        xfree (path);
    }

  return allowed && !rc ? 0 : rc ? rc : GPG_ERR_FORBIDDEN;
}

/* Test if uid is a member of group 'name'. Invert when 'not' is true. */
#ifdef HAVE_GETGRNAM_R
static gpg_error_t
acl_check_group (const char *name, uid_t uid, gid_t gid, int not, int *allowed)
{
  char *buf;
  struct group gr, *gresult;
  size_t len = sysconf (_SC_GETGR_R_SIZE_MAX);
  int err;
  gpg_error_t rc = 0;

  if (len == -1)
    len = 16384;

  buf = xmalloc (len);
  if (!buf)
    return GPG_ERR_ENOMEM;

  err = getgrnam_r (name, &gr, buf, len, &gresult);
  if (!err && gresult)
    {
      if (gresult->gr_gid == gid)
        {
          xfree (buf);
          *allowed = !not;
          return 0;
        }

      for (char **t = gresult->gr_mem; !rc && *t; t++)
        {
          char *tbuf;
          struct passwd pw;
          struct passwd *result = get_pwd_struct (*t, 0, &pw, &tbuf, &rc);

          if (!rc && result && result->pw_uid == uid)
            {
              xfree (tbuf);
              *allowed = !not;
              break;
            }

          xfree (tbuf);
        }

      xfree (buf);
      return rc;
    }
  else if (err)
    rc = gpg_error_from_errno (err);

  xfree (buf);
  return rc ? rc : !gresult ? 0 : GPG_ERR_EACCES;
}
#else
static gpg_error_t
acl_check_group (const char *name, uid_t uid, gid_t gid, int not, int *allowed)
{
  struct group *gresult = NULL;
  gpg_error_t rc = 0;

  errno = 0;
  gresult = getgrnam (name);
  if (!errno && gresult && gresult->gr_gid == gid)
    {
      *allowed = !not;
      return 0;
    }
  else if (errno)
    rc = gpg_error_from_syserror ();

  if (!gresult)
    return rc;

  for (char **t = gresult->gr_mem; !rc && *t; t++)
    {
      char *buf;
      struct passwd pw;
      struct passwd *result = get_pwd_struct (*t, 0, &pw, &buf, &rc);

      if (!rc && result && result->pw_uid == uid)
        {
          xfree (buf);
          *allowed = !not;
          break;
        }

      xfree (buf);
    }

  return rc;
}
#endif

gpg_error_t
peer_is_invoker(struct client_s *client)
{
  struct invoking_user_s *user;
  int allowed = 0;

  if (client->thd->state == CLIENT_STATE_UNKNOWN)
    return GPG_ERR_EACCES;

  for (user = invoking_users; user; user = user->next)
    {
#ifdef WITH_GNUTLS
      if (client->thd->remote)
        {
          if (user->type == INVOKING_TLS
              && !strcmp(client->thd->tls->fp, user->id))
            allowed = user->not ? 0 : 1;

          continue;
        }
#endif

      if (user->type == INVOKING_GID)
        {
          gpg_error_t rc = acl_check_group (user->id,
                                            client->thd->peer->uid,
                                            client->thd->peer->gid,
                                            user->not, &allowed);
          if (rc)
            return rc;
        }
      else if (user->type == INVOKING_UID && client->thd->peer->uid == user->uid)
        allowed = user->not ? 0 : 1;
    }

  return allowed ? 0 : GPG_ERR_EACCES;
}

/* The 'item' may be a username, TLS fingerprint or command obtained from an
 * ACL via configuration file or '_acl' element attribute. The 'cmdname' is
 * the peer command path when 'item' is a command.
 */
gpg_error_t
acl_check_common (struct client_s *client, const char *item, uid_t uid,
                  gid_t gid, int *allowed, const char *cmdname)
{
  int not = 0;
  int rw = 0;
  int tls = 0;
  int cmd = 0;
  gpg_error_t rc = 0;

  if (!item || !*item)
    return 0;

  if (*item == '-' || *item == '!')
    {
      not = 1;
      item++;
    }

  if (*item == '+') // not implemented yet
    rw = 1;

  if (*item == '#') // TLS fingerprint hash
    tls = 1;

  if (*item == '/') // client command name path
    cmd = 1;

  if (rw || tls)
    item++;

  if (tls)
    {
#ifdef WITH_GNUTLS
      if (client->thd->remote)
        {
          if (!strcasecmp (client->thd->tls->fp, item))
            *allowed = !not;
        }

      return 0;
#else
      (void)client;
      return 0;
#endif
    }
#ifdef WITH_GNUTLS
  else if (client->thd->remote) // Remote client with no FP in the ACL
    return 0;
#endif

  if (*item == '@') // all items in group
    return acl_check_group (item+1, uid, gid, not, allowed);
  else if (!cmd)
    {
      char *buf;
      struct passwd pw;
      struct passwd *pwd = get_pwd_struct (item, 0, &pw, &buf, &rc);

      if (!rc && pwd && pwd->pw_uid == uid)
        *allowed = !not;

      xfree (buf);
    }

  if (!rc && cmd && cmdname)
    {
      if (!strcmp (item, cmdname))
        *allowed = !not;
      else
        *allowed = 0;
    }

  return rc;
}

gpg_error_t
validate_peer (struct client_s *cl)
{
  gpg_error_t rc;
  char *path = NULL;

#ifdef WITH_GNUTLS
  if (cl->thd->remote)
    return tls_validate_access (cl, NULL);
#endif

  MUTEX_LOCK (&cn_mutex);
  pthread_cleanup_push (release_mutex_cb, &cn_mutex);
  rc = do_validate_peer (cl->ctx, "global", &cl->thd->peer, &path);
  pthread_cleanup_pop (1);
  log_write ("peer %s: path=%s, uid=%i, gid=%i, pid=%i, rc=%u",
             !rc ? _("accepted") : _("rejected"), path,
             cl->thd->peer->uid, cl->thd->peer->gid,
             cl->thd->peer->pid, rc);

  if (!rc)
    cl->thd->cmdname = path;
  else
    xfree (path);

  return rc;
}
