/*
    Copyright (C) 2006-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of pwmd.

    Pwmd is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.

    Pwmd is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Pwmd.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#include <err.h>
#include <ctype.h>
#include <string.h>
#ifdef HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif
#include <sys/un.h>
#include <signal.h>
#include <stdarg.h>
#include <sys/wait.h>
#ifdef HAVE_FCNTL_H
#include <fcntl.h>
#endif
#include <pwd.h>
#include <grp.h>
#include <pthread.h>
#include <sys/mman.h>
#ifdef HAVE_TERMIOS_H
#include <termios.h>
#endif
#include <assert.h>
#ifdef HAVE_SYSLOG_H
#include <syslog.h>
#endif
#ifdef HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif
#ifdef HAVE_ARPA_INET_H
#include <arpa/inet.h>
#endif
#ifdef HAVE_NETDB_H
#include <netdb.h>
#endif
#ifdef HAVE_SYS_TIME_H
#include <sys/time.h>
#endif
#include <sys/resource.h>
#include <setjmp.h>
#include <errno.h>
#include <poll.h>

#ifdef TM_IN_SYS_TIME
#include <sys/time.h>
#else
#include <time.h>
#endif

#ifdef HAVE_LIMITS_H
#include <limits.h>
#endif

#ifdef HAVE_GETOPT_LONG
#ifdef HAVE_GETOPT_H
#include <getopt.h>
#endif
#else
#include "getopt_long.h"
#endif

#ifdef HAVE_PR_SET_NAME
#include <sys/prctl.h>
#endif

#include "pwmd-error.h"
#include <gcrypt.h>

#include "util-misc.h"
#include "mem.h"
#include "xml.h"
#include "common.h"
#include "commands.h"
#include "cache.h"
#include "util-string.h"
#include "mutex.h"
#include "rcfile.h"
#include "crypto.h"
#include "acl.h"
#include "version.h"

static int quit;
static int cmdline;
static jmp_buf jmp;
static int nofork;
static int log_fd;
static unsigned assuan_level;
pthread_key_t thread_name_key;
pthread_mutex_t cn_mutex;
struct slist_s *cn_thread_list;
static int reload_rcfile_fds[2];

#ifndef HAVE_PTHREAD_CANCEL
pthread_key_t signal_thread_key;

#define INIT_SIGNAL(s, cb) do {				\
    int *n = xmalloc (sizeof (int));			\
    *n = 0;						\
    pthread_setspecific (signal_thread_key, n);		\
    struct sigaction act;				\
    sigset_t sigset;					\
    sigemptyset (&sigset);				\
    sigaddset (&sigset, s);				\
    pthread_sigmask (SIG_UNBLOCK, &sigset, NULL);	\
    memset (&act, 0, sizeof(act));			\
    act.sa_flags = SA_SIGINFO;				\
    act.sa_mask = sigset;				\
    act.sa_sigaction = cb;				\
    sigaction (s, &act, NULL);				\
  }  while (0)

static void
catch_thread_signal (int sig, siginfo_t *info, void *ctx)
{
  int *n = (int *) pthread_getspecific (signal_thread_key);

  *n = 1;
}
#endif

static void
setup_logging ()
{
  int n = config_get_boolean ("global", "enable_logging");

  if (n)
    {
      char *p = config_get_string ("global", "log_path");

      if (!p || (logfile && p && log_fd != -1 && strcmp(p, logfile)))
        {
          if (log_fd != -1)
            close (log_fd);

          log_fd = -1;
        }

      xfree (logfile);
      logfile = NULL;
      if (p)
        logfile = expand_homedir (p);
      xfree (p);
    }
  else
    {
      xfree (logfile);
      logfile = NULL;
      if (log_fd != -1)
        close(log_fd);

      log_fd = -1;
      closelog ();
    }

  log_syslog = config_get_boolean ("global", "syslog");
  if (log_syslog == 1)
    openlog ("pwmd", LOG_NDELAY | LOG_PID, LOG_DAEMON);
}

static void
reload_rcfile ()
{
  MUTEX_LOCK (&rcfile_mutex);
  struct slist_s *keep = NULL;
  struct slist_s *config;
  int b = disable_list_and_dump;
#ifdef WITH_GNUTLS
  char *prio;
  char *prio2 = NULL;
#endif

  keep = config_keep_save ();
  log_write (_("reloading configuration file '%s'"), rcfile);

#ifdef WITH_GNUTLS
  prio = config_get_string ("global", "tls_cipher_suite");
#endif
  config = config_parse (rcfile, 1);
  if (config)
    {
      config_free (global_config);
      global_config = config;
      setup_logging ();
    }

  config_keep_restore (keep);
  disable_list_and_dump = !disable_list_and_dump ? b : 1;

#ifdef WITH_GNUTLS
  /* Restart listening sockets since they may have changed. */
  tls_start_stop (1);
  tls_start_stop (0);

  prio2 = config_get_string ("global", "tls_cipher_suite");
  if ((prio2 && (!prio || strcmp (prio, prio2))) || (prio && !prio2))
    tls_rehandshake ();

  xfree (prio2);
  xfree (prio);
#endif
  crypto_set_keepalive ();
  MUTEX_UNLOCK (&rcfile_mutex);
}

#define PROCESS_DONE(client,rc) (client && client->bulk_p) ? rc : \
  assuan_process_done (client ? client->ctx : NULL, rc)
gpg_error_t
send_error (assuan_context_t ctx, gpg_error_t e)
{
  struct client_s *client = assuan_get_pointer (ctx);

  if (gpg_err_source (e) == GPG_ERR_SOURCE_UNKNOWN)
    e = gpg_error (e);

  if (client)
    client->last_rc = e;

  if (!e)
    return PROCESS_DONE (client, 0);

  if (!ctx)
    {
      log_write ("ERR %i: %s", e, pwmd_strerror (e));
      return e;
    }

  if (client && client->xml_error)
    {
      log_write ("%s", client->xml_error->message);
      xfree (client->last_error);
      client->last_error = NULL;
      if (client->xml_error->message)
	client->last_error = str_dup (client->xml_error->message);

      e = PROCESS_DONE (client, assuan_set_error (ctx, e,
                                                  client->xml_error->message
                                                  ? client->xml_error->message
                                                  : NULL));
      xmlResetLastError ();
      xmlResetError (client->xml_error);
      xfree (client->xml_error);
      client->xml_error = NULL;
      return e;
    }

  return PROCESS_DONE (client, assuan_set_error (ctx, e, pwmd_strerror (e)));
}

void
log_write (const char *fmt, ...)
{
  char *args;
  va_list ap;
  time_t now;
  char buf[255];
  pthread_t tid = pthread_self ();
  static pthread_mutex_t m = PTHREAD_MUTEX_INITIALIZER;

  if ((!logfile && !nofork && !log_syslog && !cmdline) || !fmt)
    return;

  pthread_mutex_lock (&m);

  if (!cmdline && logfile && log_fd == -1)
    {
      log_fd = open (logfile, O_WRONLY | O_CREAT | O_APPEND, 0600);
      if (log_fd == -1)
	warn ("%s", logfile);
    }

  va_start (ap, fmt);

  if (str_vasprintf (&args, fmt, ap) != -1)
    {
      pthread_cleanup_push (xfree, args);
      if (cmdline)
	{
	  fprintf (stderr, "pwmd: %s\n", args);
	  fflush (stderr);
	}
      else
	{
	  char *name = pthread_getspecific (thread_name_key);
          char *line;

          if (name)
            {
              if (*name == '!')
                snprintf (buf, sizeof (buf), "%s: ", name+1);
              else
                snprintf (buf, sizeof (buf), "%s(%p): ", name,
                          (pthread_t *)tid);
            }
          else
            snprintf (buf, sizeof (buf), "%p: ", (pthread_t *)tid);

	  name = buf;
	  if (!cmdline && log_syslog && !nofork)
	    syslog (LOG_INFO, "%s%s", name, args);

	  time (&now);
          struct tm *tm = localtime (&now);
          char tbuf[21];
	  strftime (tbuf, sizeof (tbuf), "%b %d %Y %H:%M:%S ", tm);
	  tbuf[sizeof (tbuf) - 1] = 0;

	  if (args[strlen (args) - 1] == '\n')
	    args[strlen (args) - 1] = 0;

	  line = str_asprintf ("%s %i %s%s\n", tbuf, getpid (), name, args);
	  if (line)
	    {
	      pthread_cleanup_push (xfree, line);
	      if (logfile && log_fd != -1)
		{
		  ssize_t ret = write (log_fd, line, strlen (line));
                  (void)ret;
		  fsync (log_fd);
		}

	      if (nofork)
		{
		  fprintf (stdout, "%s", line);
		  fflush (stdout);
		}

	      pthread_cleanup_pop (1);
	    }
	}
      pthread_cleanup_pop (1);
    }

  va_end (ap);

  if (log_fd != -1 && log_keepopen <= 0)
    {
      close(log_fd);
      log_fd = -1;
    }

  pthread_mutex_unlock (&m);
}

static gpg_error_t
setup_crypto ()
{
  gpg_error_t rc;

  if (!gpgrt_check_version (REQUIRE_LIBGPGERROR_VERSION))
    {
      fprintf (stderr, _("gpgrt_check_version(): Incompatible libgpg-error. "
                        "Wanted %s, got %s.\n"), REQUIRE_LIBGPGERROR_VERSION,
              gpgrt_check_version (NULL));
      return GPG_ERR_UNKNOWN_VERSION;
    }

  gpgrt_init ();
  gpgrt_set_alloc_func (xrealloc_gpgrt);

  if (!assuan_check_version (REQUIRE_LIBASSUAN_VERSION))
    {
      fprintf (stderr, _("assuan_check_version(): Incompatible libassuan. "
                        "Wanted %s, got %s.\n"), REQUIRE_LIBASSUAN_VERSION,
              assuan_check_version (NULL));
      return GPG_ERR_UNKNOWN_VERSION;
    }

  if (!gcry_check_version (REQUIRE_LIBGCRYPT_VERSION))
    {
      fprintf (stderr, _("gcry_check_version(): Incompatible libgcrypt. "
                        "Wanted %s, got %s.\n"), REQUIRE_LIBGCRYPT_VERSION,
              gcry_check_version (NULL));
      return GPG_ERR_UNKNOWN_VERSION;
    }

  gcry_set_allocation_handler (xmalloc, xmalloc, NULL, xrealloc, xfree);

  if (!gpgme_check_version (REQUIRE_LIBGPGME_VERSION))
    {
      fprintf (stderr, _("gpgme_check_version(): Incompatible libgpgme. "
                        "Wanted %s, got %s.\n"), REQUIRE_LIBGPGME_VERSION,
              gpgme_check_version (NULL));
      return GPG_ERR_UNKNOWN_VERSION;
    }

  rc = gpgme_engine_check_version (GPGME_PROTOCOL_OPENPGP);
  if (rc)
    {
      fprintf (stderr, _("gpgme_engine_check_version(GPGME_PROTOCOL_OPENPGP): %s"), gpgme_strerror (rc));
      return GPG_ERR_UNKNOWN_VERSION;
    }

  //gpgme_set_global_flag ("require-gnupg", REQUIRE_GNUPG_VERSION);
#ifdef ENABLE_NLS
  gpgme_set_locale (NULL, LC_CTYPE, setlocale (LC_CTYPE, NULL));
  gpgme_set_locale (NULL, LC_MESSAGES, setlocale (LC_MESSAGES, NULL));
#endif

#ifdef WITH_GNUTLS
  if (gnutls_global_init ())
    {
      fprintf(stderr, _("gnutls_global_init() failed.\n"));
      return GPG_ERR_UNKNOWN_VERSION;
    }

  if (!gnutls_check_version (REQUIRE_LIBGNUTLS_VERSION))
    {
      fprintf (stderr, _("gnutls_check_version(): Incompatible libgnutls. "
                         "Wanted %s, got %s.\n"), REQUIRE_LIBGNUTLS_VERSION,
               gnutls_check_version (NULL));
      return GPG_ERR_UNKNOWN_VERSION;
    }

  gnutls_global_set_log_function (tls_log);
  gnutls_global_set_audit_log_function (tls_audit_log);
#endif
  return 0;
}

static void
xml_error_cb (void *data, xmlErrorPtr e)
{
  struct client_s *client = data;

  /*
   * Keep the first reported error as the one to show in the error
   * description. Reset in send_error().
   */
  if (client->xml_error)
    return;

  client->xml_error = xcalloc (1, sizeof(xmlError));
  xmlCopyError (e, client->xml_error);
}

static pid_t
hook_waitpid (assuan_context_t ctx, pid_t pid, int action,
	      int *status, int options)
{
  (void)ctx;
  (void)action;
  return waitpid (pid, status, options);
}

static ssize_t
hook_read (assuan_context_t ctx, assuan_fd_t fd, void *data, size_t len)
{
  TEST_CANCEL ();
#ifdef WITH_GNUTLS
  struct client_s *client = assuan_get_pointer (ctx);

  if (client->thd->remote)
    return tls_read_hook (ctx, (int) fd, data, len);
#else
  (void)ctx;
#endif
  return read ((int) fd, data, len);
}

static ssize_t
hook_write (assuan_context_t ctx, assuan_fd_t fd,
	    const void *data, size_t len)
{
  TEST_CANCEL ();
#ifdef WITH_GNUTLS
  struct client_s *client = assuan_get_pointer (ctx);

  if (client->thd->remote)
    return tls_write_hook (ctx, (int) fd, data, len);
#else
  (void)ctx;
#endif
  return write ((int) fd, data, len);
}

int
assuan_log_cb (assuan_context_t ctx, void *data, unsigned cat,
               const char *msg)
{
  struct client_s *client = data;
  const char *str = NULL;

  (void)client;
  (void)ctx;

  if (!(assuan_level & cat))
    return 0;

  if (!msg)
    return 1;

  switch (cat)
    {
    case ASSUAN_LOG_INIT:
      str = "ASSUAN[INIT]";
      break;
    case ASSUAN_LOG_CTX:
      str = "ASSUAN[CTX]";
      break;
    case ASSUAN_LOG_ENGINE:
      str = "ASSUAN[ENGINE]";
      break;
    case ASSUAN_LOG_DATA:
      str = "ASSUAN[DATA]";
      break;
    case ASSUAN_LOG_SYSIO:
      str = "ASSUAN[SYSIO]";
      break;
    case ASSUAN_LOG_CONTROL:
      str = "ASSUAN[CONTROL]";
      break;
    default:
      str = "ASSUAN[UNKNOWN]";
      break;
    }

  log_write ("%s: %s", str, msg);
  return 1;
}

static int
new_connection (struct client_s *cl)
{
  gpg_error_t rc;
  static struct assuan_malloc_hooks mhooks = { xmalloc, xrealloc, xfree };
  static struct assuan_system_hooks shooks = {
    ASSUAN_SYSTEM_HOOKS_VERSION,
    __assuan_usleep,
    __assuan_pipe,
    __assuan_close,
    hook_read,
    hook_write,
    //FIXME
    NULL,			//recvmsg
    NULL,			//sendmsg both are used for FD passing
    __assuan_spawn,
    hook_waitpid,
    __assuan_socketpair,
    __assuan_socket,
    __assuan_connect
  };

#ifdef WITH_GNUTLS
  if (cl->thd->remote)
    {
      char *prio = config_get_string ("global", "tls_cipher_suite");

      cl->thd->timeout = config_get_integer ("global", "tls_timeout");
      if (fcntl (cl->thd->fd, F_SETFL, O_NONBLOCK) == -1)
        return 0;

      cl->thd->tls = tls_init_client (cl->thd->fd, cl->thd->timeout, prio);
      xfree (prio);
      if (!cl->thd->tls)
	return 0;
    }
#endif

  rc = assuan_new_ext (&cl->ctx, GPG_ERR_SOURCE_DEFAULT, &mhooks,
                       assuan_log_cb, cl);
  if (rc)
    goto fail;

  assuan_ctx_set_system_hooks (cl->ctx, &shooks);
  assuan_set_flag (cl->ctx, ASSUAN_CONFIDENTIAL, 1);
  rc = assuan_init_socket_server (cl->ctx, cl->thd->fd,
                                  ASSUAN_SOCKET_SERVER_ACCEPTED);
  if (rc)
    goto fail;

  assuan_set_pointer (cl->ctx, cl);
  assuan_set_hello_line (cl->ctx, PACKAGE_STRING PWMD_GIT_HASH);
  rc = register_commands (cl->ctx);
  if (rc)
    goto fail;

  rc = assuan_accept (cl->ctx);
  if (rc)
    goto fail;

  rc = validate_peer (cl);
  /* May not be implemented on all platforms. */
  if (rc && gpg_err_code (rc) != GPG_ERR_ASS_GENERAL)
    goto fail;

  MUTEX_LOCK (&cn_mutex);
  cl->thd->state = CLIENT_STATE_INIT;
  MUTEX_UNLOCK (&cn_mutex);
  cl->lock_timeout = config_get_integer ("global", "lock_timeout");
  xmlSetStructuredErrorFunc (cl, xml_error_cb);
  return 1;

fail:
  log_write ("%s", pwmd_strerror (rc));
  return 0;
}

/*
 * This is called after a client is cancelled or disconnects. Set with
 * pthread_cleanup_push().
 */
static void
free_client_cb (void *arg)
{
  struct client_thread_s *cn = arg;
  struct client_s *cl = cn->cl;
  char *tmp = NULL;

#ifndef HAVE_PTHREAD_CANCEL
  tmp = pthread_getspecific (signal_thread_key);
  xfree (tmp);
  pthread_setspecific (signal_thread_key, NULL);
#endif

  MUTEX_LOCK (&cn_mutex);
  cn_thread_list = slist_remove (cn_thread_list, cn);
  MUTEX_UNLOCK (&cn_mutex);

  if (cl)
    {
      unlock_flock (&cl->flock_fd);
      reset_client (cl);
      if (cl->xml_error)
	xmlResetError (cl->xml_error);

      xfree (cl->xml_error);

#ifdef WITH_GNUTLS
      if (cn->tls)
	{
	  gnutls_deinit (cn->tls->ses);
	  xfree (cn->tls->fp);
	  xfree (cn->tls);
	}
#endif

      if (cl->ctx)
	assuan_release (cl->ctx);
      else if (cl->thd && cl->thd->fd != -1)
	close (cl->thd->fd);

      if (cl->crypto)
	crypto_free (cl->crypto);

      cl->crypto = NULL;
      xfree (cl);
    }
  else
    {
      if (cn->fd != -1)
	close (cn->fd);
    }

  while (cn->msg_queue)
    {
      struct status_msg_s *msg = cn->msg_queue;

      cn->msg_queue = msg->next;
      xfree (msg->line);
      xfree (msg);
    }

  if (cn->status_msg_pipe[0] != -1)
    close (cn->status_msg_pipe[0]);

  if (cn->status_msg_pipe[1] != -1)
    close (cn->status_msg_pipe[1]);

  pthread_mutex_destroy (&cn->status_mutex);
  log_write (_("exiting, fd=%i"), cn->fd);
  send_status_all (STATUS_CLIENTS, NULL);

  xfree (cn->name);
#ifdef WITH_GNUTLS
  xfree (cn->peeraddr);
#endif

  if (cn->eof) // Not pthread_exit() or pthread_cancel().
    {
      tmp = pthread_getspecific (thread_name_key);
      xfree (tmp);
      pthread_setspecific (thread_name_key, NULL);
    }

  (void)cache_kill_scd ();
  xfree (cn->cmdname);
  xfree (cn);
}

static void
free_all_clients ()
{
  MUTEX_LOCK (&cn_mutex);
  pthread_cleanup_push (release_mutex_cb, &cn_mutex);

  while (slist_length (cn_thread_list))
    {
      struct client_thread_s *thd = slist_nth_data (cn_thread_list, 0);

      free_client_cb (thd);
      thd->eof = 1;
    }

  pthread_cleanup_pop (1);
}

static gpg_error_t
send_msg_queue (struct client_thread_s *thd)
{
  MUTEX_LOCK (&thd->status_mutex);
  gpg_error_t rc = 0;
  char c;
  ssize_t ret;

  ret = read (thd->status_msg_pipe[0], &c, 1);
  rc = gpg_error_from_syserror ();
  if (ret == -1 && gpg_err_code (rc) != GPG_ERR_EAGAIN)
    log_write ("%s (%i): %s", __FUNCTION__, __LINE__, pwmd_strerror (rc));
  else
    rc = 0;

  thd->wrote_status = 0;

  while (thd->msg_queue)
    {
      struct status_msg_s *msg = thd->msg_queue;

      thd->msg_queue = thd->msg_queue->next;
      MUTEX_UNLOCK (&thd->status_mutex);
      pthread_cleanup_push (xfree, msg);
      pthread_cleanup_push (xfree, msg->line);
      rc = send_status (thd->cl->ctx, msg->s, msg->line);
      pthread_cleanup_pop (1);
      pthread_cleanup_pop (1);
      MUTEX_LOCK (&thd->status_mutex);
      if (rc)
	break;
    }

  MUTEX_UNLOCK (&thd->status_mutex);
  if (rc && gpg_err_code (rc) != GPG_ERR_EPIPE)
    log_write ("%s (%i): %s", __FUNCTION__, __LINE__, pwmd_strerror (rc));

  return rc;
}

static void *
client_thread (void *data)
{
  struct client_thread_s *thd = data;
  struct client_s *cl = xcalloc (1, sizeof (struct client_s));
  struct slist_s *list;
  gpg_error_t rc = 0;
#ifndef HAVE_PTHREAD_CANCEL
  INIT_SIGNAL (SIGUSR2, catch_thread_signal);
#endif

#ifdef HAVE_PR_SET_NAME
  prctl (PR_SET_NAME, "client");
#endif
  pthread_setspecific (thread_name_key, str_dup (__FUNCTION__));

  if (!cl)
    {
      log_write ("%s(%i): %s", __FILE__, __LINE__,
		 pwmd_strerror (GPG_ERR_ENOMEM));
      return NULL;
    }

  MUTEX_LOCK (&cn_mutex);
  pthread_cleanup_push (free_client_cb, thd);
  thd->cl = cl;
  cl->thd = thd;
  cl->flock_fd = -1;

  list = slist_append (cn_thread_list, thd);
  if (list)
    cn_thread_list = list;
  else
    {
      log_write ("%s(%i): %s", __FILE__, __LINE__,
		 pwmd_strerror (GPG_ERR_ENOMEM));
      MUTEX_UNLOCK (&cn_mutex);
      return NULL;
    }

  if (fcntl (thd->status_msg_pipe[0], F_SETFL, O_NONBLOCK) == -1)
    rc = gpg_error_from_errno (errno);

  if (!rc)
    if (fcntl (thd->status_msg_pipe[1], F_SETFL, O_NONBLOCK) == -1)
      rc = gpg_error_from_errno (errno);

  MUTEX_UNLOCK (&cn_mutex);

  if (rc)
    {
      log_write ("%s(%i): %s", __FILE__, __LINE__, pwmd_strerror (rc));
      goto done;
    }

  if (new_connection (cl))
    {
      struct pollfd fds[2];

      fds[0].fd = thd->fd;
      fds[0].events = POLLIN;
      fds[1].fd = thd->status_msg_pipe[0];
      fds[1].events = POLLIN;

      send_status_all (STATUS_CLIENTS, NULL);
      rc = send_status (cl->ctx, STATUS_CACHE, NULL);
      if (rc)
	{
	  log_write ("%s(%i): %s", __FILE__, __LINE__, pwmd_strerror (rc));
          goto done;
	}

      for (;;)
	{
          int n;
	  int eof;

          n = poll (fds, sizeof(fds)/sizeof(fds[0]), 100);
	  if (n == -1)
	    {
	      log_write ("%s", strerror (errno));
	      break;
	    }

#ifdef WITH_GNUTLS
          if (thd->remote && thd->tls && thd->tls->rehandshake)
            {
              char *prio;
              int ret;
              const char *e;

              if (thd->tls->rehandshake == 1)
                {
                  prio = config_get_string ("global", "tls_cipher_suite");
                  if (!prio)
                    {
                      thd->tls->rehandshake = 0;
                      continue;
                    }

                  ret = gnutls_priority_set_direct (thd->tls->ses, prio, &e);
                  if (ret == GNUTLS_E_SUCCESS)
                    {
                      rc = send_status (cl->ctx, STATUS_REHANDSHAKE, NULL);
                      if (!rc)
                        {
                          rc = assuan_send_data (cl->ctx, NULL, 0);
                          if (!rc)
                            {
                              ret = gnutls_rehandshake (thd->tls->ses);
                              if (ret)
                                {
                                  log_write ("%s", gnutls_strerror (ret));
                                  thd->tls->rehandshake = 0;
                                }
                              else
                                thd->tls->rehandshake = 2;
                            }
                        }

                      if (rc)
                        log_write ("%s", pwmd_strerror (rc));
                    }
                  else
                    log_write ("%s: %s", gnutls_strerror (ret), e);

                  xfree (prio);
                  continue;
                }
            }
#endif

          if (!n)
            continue;

          if (fds[1].revents & POLLIN)
	    {
#ifdef WITH_GNUTLS
              if (!thd->remote || (thd->tls && !thd->tls->rehandshake))
#endif
                {
                  rc = send_msg_queue (thd);
                  if (rc && gpg_err_code (rc) != GPG_ERR_EPIPE)
                    break;
                }
	    }

#ifdef HAVE_PTHREAD_CANCEL
          if (!(fds[0].revents & POLLIN))
#else
          if (thd->fd != -1 && !(fds[0].revents & POLLIN))
#endif
	    continue;

	  rc = assuan_process_next (cl->ctx, &eof);
	  if (rc || eof)
	    {
	      if (gpg_err_code (rc) == GPG_ERR_EOF || eof)
		break;

	      log_write ("assuan_process_next(): rc=%u %s", rc,
			 pwmd_strerror (rc));
	      if (rc == gpg_error (GPG_ERR_ETIMEDOUT))
		break;

	      rc = send_error (cl->ctx, rc);
	      if (rc)
		{
		  log_write ("assuan_process_done(): rc=%u %s", rc,
			     pwmd_strerror (rc));
		  break;
		}
	    }

	  /* Since the msg queue pipe fd's are non-blocking, check for
	   * pending status msgs here. GPG_ERR_EPIPE can be seen when the
	   * client has already disconnected and will be converted to
	   * GPG_ERR_EOF during assuan_process_next().
	   */
#ifdef WITH_GNUTLS
          if (!thd->remote || (thd->tls && !thd->tls->rehandshake))
#endif
            {
              rc = send_msg_queue (thd);
              if (rc && gpg_err_code (rc) != GPG_ERR_EPIPE)
                break;
            }
	}
    }

done:
  /* Don't do pthread_exit() here because any set pthread_cleanup_push
   * functions would be called after a command failed but then the client
   * exited normally which may lead to a double free. */
  thd->eof = 1;
  pthread_cleanup_pop (1);
  return NULL;
}

static gpg_error_t
xml_import (const char *filename, const char *outfile, char **keyid,
            char *sign_keyid, char *keyfile, const char *userid,
            const char *algo, long expire, int no_passphrase, int symmetric)
{
  xmlDocPtr doc;
  int fd;
  struct stat st;
  int len;
  xmlChar *xmlbuf = NULL;
  gpg_error_t rc = 0;
  struct crypto_s *crypto = NULL;

  if (strcmp (filename, "-"))
    {
      rc = open_check_file (filename, &fd, &st, 0);
      if (rc)
        return rc;

      xmlbuf = xmalloc (st.st_size + 1);
      if (!xmlbuf)
        {
          close (fd);
          return GPG_ERR_ENOMEM;
        }

      if (read (fd, xmlbuf, st.st_size) == -1)
        {
          rc = gpg_error_from_errno (errno);
          close (fd);
          xfree (xmlbuf);
          return rc;
        }

      xmlbuf[st.st_size] = 0;
      close (fd);
    }
  else
    {
#define BUFSIZE 8196
      size_t size = 0, xlen = 0;

      for (;;)
        {
          size_t ret;
          xmlChar *tmp;

          tmp = xrealloc (xmlbuf, size+BUFSIZE+1);
          if (!tmp)
            {
              xfree (xmlbuf);
              return GPG_ERR_ENOMEM;
            }

          xmlbuf = tmp;
          size += BUFSIZE;
          ret = read (STDIN_FILENO, &xmlbuf[xlen], BUFSIZE);
          if (ret == -1)
            {
              rc = gpg_error_from_syserror ();
              xfree (xmlbuf);
              return rc;
            }

          xlen += ret;
          if (!ret || ret < BUFSIZE)
            break;
        }

      xmlbuf[xlen] = 0;
    }

  doc = xmlReadDoc (xmlbuf, NULL, "UTF-8", XML_PARSE_NOBLANKS);
  xfree (xmlbuf);
  if (!doc)
    return GPG_ERR_BAD_DATA;

  xmlNodePtr n = xmlDocGetRootElement (doc);
  if (n && !xmlStrEqual (n->name, (xmlChar *) "pwmd"))
    rc = GPG_ERR_BAD_DATA;

  if (!rc)
    {
      rc = xml_validate_import (NULL, n ? n->children : n);
      if (!rc)
        {
          rc = crypto_init (&crypto, NULL, filename, keyfile != NULL, keyfile);
          if (!rc)
            {
              if (keyfile)
                {
                  crypto->flags |= CRYPTO_FLAG_KEYFILE;
                  crypto->keyfile = str_dup (keyfile);
                }

              xmlDocDumpMemory (doc, &crypto->plaintext, &len);
              if (len > 0)
                crypto->plaintext_size = len;
              else
                rc = GPG_ERR_ENOMEM;
            }
        }
    }

  if (!rc)
    {
      if (!symmetric && !keyid)
        {
          crypto->save.userid = str_dup (userid);
          crypto->save.algo = algo ? str_dup (algo) : NULL;
          crypto->save.expire = expire;
          if (no_passphrase)
            crypto->save.flags |= GPGME_CREATE_NOPASSWD;

          rc = crypto_genkey (NULL, crypto);
        }
      else
        {
          if (keyid)
            crypto->save.pubkey = strv_dup (keyid);

          if (sign_keyid)
            crypto->save.sigkey = str_dup (sign_keyid);
        }

      if (!rc)
        {
          crypto->flags |= symmetric ? CRYPTO_FLAG_SYMMETRIC : 0;
          rc = crypto_encrypt (NULL, crypto);
        }
    }

  if (!rc)
    {
      if (!strcmp (outfile, "-"))
        outfile = NULL;

      xfree (crypto->plaintext);
      crypto->plaintext = NULL;
      xfree (crypto->filename);
      crypto->filename = outfile ? str_dup (outfile) : NULL;
      rc = crypto_write_file (crypto, NULL, NULL);
    }

  xmlFreeDoc (doc);
  crypto_free (crypto);
  return rc;
}

static gpg_error_t
do_cache_push (struct crypto_s *crypto)
{
  gpg_error_t rc;
  xmlDocPtr doc;
  struct cache_data_s *cdata;
  unsigned char *crc;
  size_t len;
  int fd = -1;

  log_write (_("Adding '%s' to the cache..."),
	     crypto->filename);

  if (valid_filename (crypto->filename) == 0)
    {
      log_write (_("%s: Invalid characters in filename"), crypto->filename);
      return GPG_ERR_INV_VALUE;
    }

  rc = lock_flock (NULL, crypto->filename, LOCK_SH, &fd);
  if (!rc)
    rc = crypto_decrypt (NULL, crypto);
  if (rc)
    {
      unlock_flock (&fd);
      return rc;
    }

  rc = xml_parse_doc ((char *) crypto->plaintext, crypto->plaintext_size, &doc);
  if (rc)
    {
      unlock_flock (&fd);
      log_write ("%s", pwmd_strerror (rc));
      return rc;
    }

  cdata = xcalloc (1, sizeof (struct cache_data_s));
  if (!cdata)
    {
      unlock_flock (&fd);
      xmlFreeDoc (doc);
      return GPG_ERR_ENOMEM;
    }

  rc = get_checksum (crypto->filename, &crc, &len);
  unlock_flock (&fd);
  if (rc)
    {
      xmlFreeDoc (doc);
      cache_free_data_once (cdata);
      return rc;
    }

  cdata->crc = crc;
  rc = cache_encrypt (crypto);
  if (!rc)
    {
      cdata->doc = crypto->plaintext;
      cdata->size = crypto->plaintext_size;
      crypto->plaintext = NULL;
      cdata->pubkey = crypto->pubkey;
      cdata->sigkey = crypto->sigkey;
      crypto->pubkey = NULL;
      crypto->sigkey = NULL;
    }
  else
    {
      xmlFreeDoc (doc);
      cache_free_data_once (cdata);
      return rc;
    }

  long timeout = config_get_long (crypto->filename, "cache_timeout");
  rc = cache_add_file (crypto->filename, cdata, timeout, 0);
  return rc;
}

static gpg_error_t
init_client (int fd, const char *addr)
{
  gpg_error_t rc = 0;
  struct client_thread_s *new = xcalloc (1, sizeof (struct client_thread_s));

  if (!new)
    {
      close (fd);
      log_write ("%s: %s", __FUNCTION__, pwmd_strerror (ENOMEM));
      return GPG_ERR_ENOMEM;
    }

  MUTEX_LOCK (&cn_mutex);
  pthread_cleanup_push (release_mutex_cb, &cn_mutex);
  new->conntime = time (NULL);

  if (pipe (new->status_msg_pipe) == -1)
    rc = gpg_error_from_errno (errno);
  else
    pthread_mutex_init (&new->status_mutex, NULL);

  if (!rc)
    {
#ifdef WITH_GNUTLS
      new->remote = addr ? 1 : 0;
      if (addr)
        new->peeraddr = str_dup (addr);
#endif
      new->fd = fd;
      rc = create_thread (client_thread, new, &new->tid, 1);
      if (rc)
	{
	  close (new->status_msg_pipe[0]);
	  close (new->status_msg_pipe[1]);
	  pthread_mutex_destroy (&new->status_mutex);
	}
    }

  if (!rc)
    {
      if (addr)
        log_write (_("new connection: tid=%p, fd=%i, addr=%s"),
                   (pthread_t *) new->tid, fd, addr);
      else
        log_write (_("new connection: tid=%p, fd=%i"),
                   (pthread_t *) new->tid, fd);
    }

  pthread_cleanup_pop (1);

  if (rc)
    {
      xfree (new);
      close (fd);
      log_write ("%s(%i): %s", __FILE__, __LINE__, pwmd_strerror (rc));
    }
  return rc;
}

#ifdef WITH_GNUTLS
static gpg_error_t
do_tls_accept (struct pollfd *fds)
{
  struct sockaddr_storage raddr;
  socklen_t slen = sizeof (raddr);
  int fd;
  char s[INET6_ADDRSTRLEN];

  if (!(fds->revents & POLLIN))
    return 0;

  memset (&raddr, 0, sizeof (raddr));
  fd = accept (fds->fd, (struct sockaddr *) &raddr, &slen);
  if (fd == -1)
    {
      int e = errno;

      if (errno != EAGAIN && !quit)
        log_write ("%s: %s", __FUNCTION__,
                   pwmd_strerror (gpg_error_from_syserror()));

      return gpg_error_from_errno (e);
    }

  inet_ntop (raddr.ss_family, get_in_addr ((struct sockaddr *) &raddr), s,
             sizeof s);
  (void) init_client (fd, s);
  return 0;
}
#endif

static void *
accept_thread (void *arg)
{
  int sockfd = *(int *) arg;
#ifndef HAVE_PTHREAD_CANCEL
  INIT_SIGNAL (SIGUSR2, catch_thread_signal);
#endif

#ifdef HAVE_PR_SET_NAME
  prctl (PR_SET_NAME, "accept");
#endif
  pthread_setspecific (thread_name_key, str_asprintf ("!%s", __FUNCTION__));

#define N_FDS 4
  for (;;)
    {
      socklen_t slen = sizeof (struct sockaddr_un);
      struct sockaddr_un raddr;
      int fd, s = 0;
      struct pollfd fds[N_FDS];

      TEST_CANCEL ();
      memset (fds, 0, sizeof (fds));
      fds[s].fd = sockfd;
      fds[s++].events = POLLIN;

      fds[s].fd = reload_rcfile_fds[0];
      fds[s++].events = POLLIN;

#ifdef WITH_GNUTLS
      if (tls_fd != -1)
        {
          fds[s].fd = tls_fd;
          fds[s++].events = POLLIN;
        }
      else
        fds[s].fd = tls_fd;

      if (tls6_fd != -1)
        {
          fds[s].fd = tls6_fd;
          fds[s++].events = POLLIN;
        }
      else
        fds[s].fd = tls6_fd;
#endif

      s = poll (fds, s, -1);
      if (s == -1)
        {
          if (errno != EINTR)
            log_write ("%s", strerror (errno));
          break;
        }
      else if (s == 0)
        continue;

      if (fds[0].revents & POLLIN)
        {
          fd = accept (sockfd, (struct sockaddr *) &raddr, &slen);
          if (fd == -1)
            {
              if (errno == EMFILE || errno == ENFILE)
                log_write ("%s: %s", __FUNCTION__,
                           pwmd_strerror (gpg_error_from_errno (errno)));
              else if (errno != EAGAIN && errno != EINTR)
                {
                  if (!quit)	// probably EBADF
                    log_write ("%s: %s", __FUNCTION__,
                               pwmd_strerror (gpg_error_from_errno (errno)));

                  break;
                }

              continue;
            }

          (void) init_client (fd, NULL);
        }

      for (int n = 1; n < N_FDS; n++)
        {
          if (fds[n].fd == -1)
            continue;

          if (fds[n].fd == reload_rcfile_fds[0] && (fds[n].revents & POLLIN))
            {
              char c;
              int r = read (reload_rcfile_fds[0], &c, 1);

              if (r != -1)
                reload_rcfile ();
            }

#ifdef WITH_GNUTLS
          if (fds[n].fd == tls_fd && (fds[n].revents & POLLIN))
            (void)do_tls_accept (&fds[n]);

          if (fds[n].fd == tls6_fd && (fds[n].revents & POLLIN))
            (void)do_tls_accept (&fds[n]);
#endif
        }
    }

  /* Just in case accept() failed for some reason other than EBADF */
  quit = 1;
  return NULL;
}

static void *
cache_timer_thread (void *arg)
{
  unsigned k = 0;
#ifndef HAVE_PTHREAD_CANCEL
  INIT_SIGNAL (SIGUSR2, catch_thread_signal);
#endif

  (void)arg;

#ifdef HAVE_PR_SET_NAME
  prctl (PR_SET_NAME, "timer");
#endif
  pthread_setspecific (thread_name_key, str_asprintf ("!%s", __FUNCTION__));

  for (;;)
    {
      struct timeval tv = { 1, 0 };
      unsigned keepalive = config_get_integer ("global", "keepalive_interval");

      TEST_CANCEL ();
      select (0, NULL, NULL, NULL, &tv);
      cache_adjust_timeout ();

      if (keepalive && ++k >= keepalive)
        {
          send_status_all (STATUS_KEEPALIVE, NULL);
          k = 0;
        }
    }

  return NULL;
}

static void
init_signals (sigset_t *sigset)
{
  sigemptyset (sigset);

  /* Termination */
  sigaddset (sigset, SIGTERM);
  sigaddset (sigset, SIGINT);

  /* Clears the file cache. */
  sigaddset (sigset, SIGUSR1);

  /* Configuration file reloading. */
  sigaddset (sigset, SIGHUP);

#ifndef HAVE_PTHREAD_CANCEL
  /*
    The socket, cache and rcfile threads use this signal when
    pthread_cancel() is unavailable. Prevent the main thread from
    catching this signal from another process.
  */
  sigaddset (sigset, SIGUSR2);
#endif
  sigaddset (sigset, SIGABRT);
  pthread_sigmask (SIG_BLOCK, sigset, NULL);

#ifndef HAVE_PTHREAD_CANCEL
  /* Remove this signal from the watched signals in signal_loop(). */
  sigdelset (sigset, SIGUSR2);
#endif
}

static int
signal_loop (sigset_t sigset)
{
  int done = 0;

  do
    {
#ifdef HAVE_SIGWAITINFO
      siginfo_t info;

      memset (&info, 0, sizeof (info));
      sigwaitinfo (&sigset, &info);
      log_write (_("caught signal %i (%s), sender: pid=%li, uid=%u"),
                   info.si_signo, strsignal (info.si_signo),
                   info.si_pid <= 0 ? getpid () : info.si_pid,
                   info.si_uid <= 0 ? getuid () : info.si_uid);

      switch (info.si_signo)
#else
      int sig;

      sigwait (&sigset, &sig);
      log_write (_("caught signal %i (%s)"), sig, strsignal (sig));

      switch (sig)
#endif
	{
	case SIGHUP:
            {
              char c = 0xff;
              int r = write (reload_rcfile_fds[1], &c, 1);

              if (r == -1)
                log_write ("%s: %s: %i: %s", __FILE__, __FUNCTION__, __LINE__,
                           pwmd_strerror (gpg_err_code_from_syserror ()));
            }
	  break;
	case SIGUSR1:
	  log_write (_("clearing file cache"));
          cache_clear (NULL, NULL, 1, 0);
          send_status_all (STATUS_CACHE, NULL);
	  break;
	default:
	  done = 1;
	  break;
	}
    }
  while (!done);

  return done;
}

static void
catchsig (int sig, siginfo_t *info, void *data)
{
  static int done;

  (void)sig;
  (void)info;
  (void)data;

  if (done)
    return;

#ifdef HAVE_BACKTRACE
  BACKTRACE (__FUNCTION__);
#endif
  done = 1;
  longjmp (jmp, 1);
}

static void
cancel_all_clients ()
{
  unsigned i, t;

  MUTEX_LOCK (&cn_mutex);
  t = slist_length (cn_thread_list);
  for (i = 0; i < t; i++)
    {
      struct client_thread_s *thd = slist_nth_data (cn_thread_list, i);

#ifdef HAVE_PTHREAD_CANCEL
      pthread_cancel (thd->tid);
#else
      pthread_kill (thd->tid, SIGUSR2);
#endif
    }

  while (slist_length (cn_thread_list))
    {
      MUTEX_UNLOCK (&cn_mutex);
      usleep (50000);
      MUTEX_LOCK (&cn_mutex);
    }

  MUTEX_UNLOCK (&cn_mutex);
}

static int
server_loop (int sockfd, char **socketpath)
{
  struct sigaction sa = { 0 };
  pthread_t cache_timeout_tid;
  pthread_t accept_tid;
  int cancel_timeout_thread = 0;
  int cancel_accept_thread = 0;
  sigset_t sigset;
  int n;
  int segv = 0;
  gpg_error_t rc;

  init_commands ();
  init_signals (&sigset);

  sa.sa_sigaction = &catchsig;
  sa.sa_flags = SA_SIGINFO;

  /* An assertion failure. */
  if (sigaction (SIGABRT, &sa, NULL) == -1)
    {
      log_write ("%s(%i): sigaction(): %s", __FILE__, __LINE__,
		 pwmd_strerror (errno));
      goto done;
    }

  memset (&sa, 0, sizeof (sa));
  sa.sa_sigaction = &catchsig;
  sa.sa_flags = SA_SIGINFO;

  /* Can show a backtrace of the stack in the log. */
  if (sigaction (SIGSEGV, &sa, NULL) == -1)
    {
      log_write ("%s(%i): sigaction(): %s", __FILE__, __LINE__,
		 pwmd_strerror (errno));
      goto done;
    }

  char *p = get_username (getuid());
  log_write (_("%s started for user %s"), PACKAGE_STRING PWMD_GIT_HASH, p);
  xfree (p);

#ifdef WITH_GNUTLS
  if (config_get_boolean ("global", "enable_tcp"))
      log_write (_("Listening on TCP port %i"),
                 config_get_integer ("global", "tcp_port"));
#endif

  if (config_get_boolean ("global", "enable_socket"))
    log_write (_("Listening on %s"), *socketpath);

  rc = create_thread (cache_timer_thread, NULL, &cache_timeout_tid, 0);
  if (rc)
    {
      log_write ("%s(%i): pthread_create(): %s", __FILE__, __LINE__,
		 pwmd_strerror (rc));
      goto done;
    }

  if (pipe (reload_rcfile_fds) == -1)
    {
      log_write ("%s(%i): pipe(): %s", __FILE__, __LINE__,
		 pwmd_strerror (gpg_err_code_from_syserror ()));
      goto done;
    }

  cancel_timeout_thread = 1;
  rc = create_thread (accept_thread, &sockfd, &accept_tid, 0);
  if (rc)
    {
      log_write ("%s(%i): pthread_create(): %s", __FILE__, __LINE__,
		 pwmd_strerror (rc));
      goto done;
    }

  cancel_accept_thread = 1;
  if (!setjmp (jmp))
    signal_loop (sigset);
  else
    segv = 1;

done:
  /*
   * We're out of the main server loop. This happens when a signal was sent
   * to terminate the daemon. Cancel all clients and exit.
   */
  if (cancel_accept_thread)
    {
#ifdef HAVE_PTHREAD_CANCEL
      n = pthread_cancel (accept_tid);
#else
      n = pthread_kill (accept_tid, SIGUSR2);
#endif
      if (!n)
	pthread_join (accept_tid, NULL);
    }

  if (cancel_timeout_thread)
    {
#ifdef HAVE_PTHREAD_CANCEL
      n = pthread_cancel (cache_timeout_tid);
#else
      n = pthread_kill (cache_timeout_tid, SIGUSR2);
#endif
      if (!n)
        pthread_join (cache_timeout_tid, NULL);
    }

#ifdef WITH_GNUTLS
  tls_start_stop (1);
#endif
  shutdown (sockfd, SHUT_RDWR);
  close (sockfd);
  unlink (*socketpath);
  xfree (*socketpath);
  *socketpath = NULL;
  MUTEX_LOCK (&cn_mutex);
  n = slist_length (cn_thread_list);
  MUTEX_UNLOCK (&cn_mutex);

  if (n && !segv)
    cancel_all_clients ();
  else
    free_all_clients ();

  cache_deinit ();
  deinit_commands ();
  return segv ? EXIT_FAILURE : EXIT_SUCCESS;
}

static void
usage (const char *pn, int status)
{
  FILE *fp = status == EXIT_FAILURE ? stderr : stdout;

  fprintf (fp, _("Usage: %s [OPTIONS] [file1] [...]\n"
		 "  --homedir                    alternate pwmd home directory (~/.pwmd)\n"
		 "  -f, --rcfile=filename        load the specified configuration file\n"
		 "                               (~/.pwmd/config)\n"
                 "  --kill                       terminate an existing instance of pwmd\n"
		 "  -n, --no-fork                run as a foreground process\n"
		 "  --disable-dump               disable the LIST, XPATH and DUMP commands\n"
		 "  --ignore, --force            ignore cache pushing errors during startup\n"
		 "  -I, --import=filename        import a pwmd DTD formatted XML file)\n"
		 "  -k, --passphrase-file=file   for use when importing\n"
		 "  -o, --outfile=filename       output file when importing\n"
		 "  --keyid=fpr[,..]             public key to use when encrypting\n"
		 "  --sign-keyid=fpr             fingerprint of the signing key to use\n"
                 "  -s, --symmetric              use conventional encryption with optional signer\n"
                 "  --userid=string              name and email address to use when importing\n"
                 "  --algo=string                algorithm to use when importing (engine default)\n"
                 "  --expire=seconds             key expiry time when importing (3 years)\n"
                 "  --no-passphrase              don't require a passphrase when importing\n"
                 "  --debug=[a:..][,g:N][,t:N]   enable debugging (a:[ixedsc],g:[1-9],t:[0-N])\n"
		 "  --help                       this help text\n"
		 "  --version                    show version and compile time features\n"),
	   pn);
  exit (status);
}

static void
unlink_stale_socket (const char *sock, const char *pidfile)
{
  log_write (_ ("removing stale socket %s"), sock);
  unlink (sock);
  unlink (pidfile);
}

static int
test_pidfile (const char *path, const char *sock, char *buf, size_t buflen,
	      char **pidfile, int create, mode_t mode, int terminate)
{
  long pid;
  int fd;
  size_t len;

  if (!create)
    {
      snprintf (buf, buflen, "%s.pid", path);
      *pidfile = str_dup (buf);
      fd = open (buf, O_RDONLY);
    }
  else
    fd = open (*pidfile, O_CREAT|O_WRONLY|O_TRUNC, mode);

  if (fd == -1)
    {
      if (!create && errno != ENOENT)
	{
	  log_write ("%s: %s", buf, pwmd_strerror (errno));
	  xfree (*pidfile);
	  *pidfile = NULL;
	  return -1;
	}
      else if (!create && !terminate)
	return 0;

      log_write ("%s: %s", *pidfile, strerror (errno));
      return -1;
    }

  if (create)
    {
      pid = getpid();
      snprintf (buf, buflen, "%li", pid);
      ssize_t ret = write (fd, buf, strlen (buf));
      if (ret == -1)
        log_write ("%s (%i): %s", __FUNCTION__, __LINE__,
                   pwmd_strerror (gpg_error_from_syserror ()));
      close (fd);
      return 0;
    }

  if (buflen < 8)
    {
      close (fd);
      log_write ("%s (%i): %s", __FUNCTION__, __LINE__,
                 pwmd_strerror (GPG_ERR_BUFFER_TOO_SHORT));
      return -1;
    }

  len = read (fd, buf, buflen);
  close (fd);
  if (len == 0)
    {
      unlink_stale_socket (path, *pidfile);
      return 0;
    }

  if (sscanf (buf, "%li", &pid) != 1 || pid == 0)
    {
      if (!terminate)
        {
          unlink_stale_socket (path, *pidfile);
          return 0;
        }
    }

  if (kill (pid, 0) == -1)
    {
      unlink_stale_socket (path, *pidfile);
      return 0;
    }

  if (terminate)
    {
      if (kill (pid, SIGTERM) == -1)
        log_write ("%s: %s", path, pwmd_strerror (errno));
    }
  else
    log_write (_ ("an instance for socket %s is already running"), path);

  xfree (*pidfile);
  *pidfile = NULL;
  return 1;
}

static unsigned
parse_debug_level (const char *str, unsigned *debug, int *gpgme, int *tls)
{
  const char *p;
  unsigned level = 0;
  int gl = 0, tl = 0;

  for (p = str; p && *p; p++)
    {
      if (*p == 'a') // assuan debug flags
        {
          if (*++p != ':')
            return 1;

          while (*++p)
            {
              switch (*p)
                {
                case 'i':
                  level |= ASSUAN_LOG_INIT;
                  break;
                case 'x':
                  level |= ASSUAN_LOG_CTX;
                  break;
                case 'e':
                  level |= ASSUAN_LOG_ENGINE;
                  break;
                case 'd':
                  level |= ASSUAN_LOG_DATA;
                  break;
                case 's':
                  level |= ASSUAN_LOG_SYSIO;
                  break;
                case 'c':
                  level |= ASSUAN_LOG_CONTROL;
                  break;
                case ',':
                  break;
                default:
                  return 1;
                }

              if (*p == ',')
                break;
            }

          if (!*p)
            break;
        }
      else if (*p == 'g' || *p == 't') // gpgme and TLS debug level
        {
          int t = *p == 't';
          int n;

          if (*++p != ':')
            return 1;

          if (!isdigit (*++p))
            return 1;

          n = atoi (p);
          if (t)
            tl = n;
          else
            gl = n;

          if (tl < 0 || gl < 0 || gl > 9)
            return 1;

          while (isdigit (*p))
            p++;

          p--;
          if (*(p+1) && *(p+1) != ',')
            return 1;
          else if (*(p+1))
            p++;
        }
      else
        return 1;
    }

  if (tl)
    *tls = tl;

  if (gl)
    *gpgme = gl;

  *debug = level;
  return 0;
}

int
main (int argc, char *argv[])
{
  int opt;
  struct sockaddr_un addr;
  char buf[PATH_MAX];
  char *socketpath = NULL, *socketdir, *socketname = NULL;
  char *socketarg = NULL;
  char *datadir = NULL;
  char *pidfile = NULL;
  mode_t mode = 0600;
  int x;
  char *p;
  char **cache_push = NULL;
  char *import = NULL, *keyid = NULL, *sign_keyid = NULL;
  char *userid = NULL;
  char *algo = NULL;
  long expire = 0;
  int no_passphrase = 0;
  int estatus = EXIT_FAILURE;
  int sockfd = -1;
  char *outfile = NULL;
  int do_unlink = 0;
  int secure = 0;
  int show_version = 0;
  int force = 0;
  gpg_error_t rc;
  char *keyfile = NULL;
  int exists;
  int optindex;
  int terminate = 0;
  int sym = 0;
  int gpgme_level = -1;
  int tls_level = -1;
  /* Must maintain the same order as longopts[] */
  enum
  {
    OPT_VERSION, OPT_HELP, OPT_HOMEDIR, OPT_NO_FORK, OPT_DISABLE_DUMP,
    OPT_FORCE, OPT_RCFILE, OPT_PASSPHRASE_FILE, OPT_IMPORT, OPT_OUTFILE,
    OPT_KEYID, OPT_SIGN_KEYID, OPT_SYMMETRIC, OPT_USERID, OPT_ALGO, OPT_EXPIRE,
    OPT_NOPASSPHRASE, OPT_KILL, OPT_DEBUG
  };
  const char *optstring = "nf:C:k:I:o:s";
  const struct option longopts[] = {
    {"version", no_argument, 0, 0},
    {"help", no_argument, 0, 0},
    {"homedir", required_argument, 0, 0},
    {"no-fork", no_argument, 0, 'n'},
    {"disable_dump", no_argument, 0, 0},
    {"force", no_argument, 0, 0},
    {"rcfile", required_argument, 0, 'f'},
    {"passphrase-file", required_argument, 0, 'k'},
    {"import", required_argument, 0, 'I'},
    {"outfile", required_argument, 0, 'o'},
    {"keyid", required_argument, 0, 0},
    {"sign-keyid", required_argument, 0, 0},
    {"symmetric", no_argument, 0, 's'},
    {"userid", required_argument, 0, 0},
    {"algo", required_argument, 0, 0},
    {"expire", required_argument, 0, 0},
    {"no-passphrase", no_argument, 0, 0},
    {"kill", no_argument, 0, 0},
    {"debug", required_argument, 0, 0},
    {0, 0, 0, 0}
  };

  log_fd = -1;
  cmdline = 1;
  expire = time (NULL) + DEFAULT_EXPIRE;

#ifndef DEBUG
#ifdef HAVE_SETRLIMIT
  struct rlimit rl;

  rl.rlim_cur = rl.rlim_max = 0;

  if (setrlimit (RLIMIT_CORE, &rl) != 0)
    err (EXIT_FAILURE, "setrlimit()");
#endif

#ifdef HAVE_PR_SET_DUMPABLE
  prctl (PR_SET_DUMPABLE, 0);
#endif
#endif

#ifdef ENABLE_NLS
  setlocale (LC_ALL, "");
  bindtextdomain ("pwmd", LOCALEDIR);
  textdomain ("pwmd");
#endif

  while ((opt = getopt_long (argc, argv, optstring, longopts, &optindex))
	 != -1)
    {
      switch (opt)
	{
	case 'I':
	  import = optarg;
	  break;
	case 'k':
	  keyfile = optarg;
	  break;
	case 'o':
	  outfile = optarg;
	  break;
	case 'n':
	  nofork = 1;
	  break;
	case 'f':
	  rcfile = str_dup (optarg);
	  break;
        case 's':
          sym = 1;
          break;
	default:
	  usage (argv[0], EXIT_FAILURE);
	  break;
	case 0:
	  switch (optindex)
	    {
            case OPT_DEBUG:
              if (parse_debug_level (optarg, &assuan_level, &gpgme_level,
                                     &tls_level))
                usage (argv[0], EXIT_FAILURE);
              break;
            case OPT_SYMMETRIC:
              sym = 1;
              break;
	    case OPT_VERSION:
	      show_version = 1;
	      break;
	    case OPT_HELP:
	      usage (argv[0], EXIT_SUCCESS);
	      break;
	    case OPT_HOMEDIR:
	      homedir = str_dup (optarg);
	      break;
	    case OPT_NO_FORK:
	      nofork = 1;
	      break;
	    case OPT_DISABLE_DUMP:
	      secure = 1;
	      break;
	    case OPT_FORCE:
	      force = 1;
	      break;
	    case OPT_RCFILE:
	      rcfile = str_dup (optarg);
	      break;
	    case OPT_PASSPHRASE_FILE:
	      keyfile = optarg;
	      break;
	    case OPT_IMPORT:
	      import = optarg;
	      break;
	    case OPT_OUTFILE:
	      outfile = optarg;
	      break;
	    case OPT_KEYID:
	      keyid = optarg;
	      break;
	    case OPT_SIGN_KEYID:
	      sign_keyid = optarg;
	      break;
            case OPT_USERID:
              userid = optarg;
              break;
            case OPT_ALGO:
              algo = optarg;
              break;
            case OPT_EXPIRE:
              errno = rc = 0;
              expire = strtoul (optarg, &p, 10);

              if (!errno && p && *p)
                rc = GPG_ERR_INV_VALUE;
              else if (expire == ULONG_MAX)
                rc = GPG_ERR_INV_VALUE;
              else if (errno)
                rc = gpg_error_from_syserror ();

              if (rc)
                usage (argv[0], EXIT_FAILURE);
              break;
            case OPT_NOPASSPHRASE:
              no_passphrase = 1;
              break;
            case OPT_KILL:
              terminate = 1;
              break;
	    default:
	      usage (argv[0], EXIT_FAILURE);
	    }
	}
    }

  if (show_version)
    {
      printf (_("%s\n\n"
		"Copyright (C) 2006-2025\n"
		"%s\n"
		"Released under the terms of the GPL v2.\n\n"
		"Compile time features:\n%s"), PACKAGE_STRING PWMD_GIT_HASH,
	      PACKAGE_BUGREPORT,
#ifdef PWMD_HOMEDIR
	      "+PWMD_HOMEDIR=" PWMD_HOMEDIR "\n"
#endif
#ifdef WITH_GNUTLS
	      "+WITH_GNUTLS\n"
#else
	      "-WITH_GNUTLS\n"
#endif
#ifdef WITH_LIBACL
	      "+WITH_LIBACL\n"
#else
	      "-WITH_LIBACL\n"
#endif
#ifdef DEBUG
	      "+DEBUG\n"
#else
	      "-DEBUG\n"
#endif
#ifdef MEM_DEBUG
	      "+MEM_DEBUG\n"
#else
	      "-MEM_DEBUG\n"
#endif
#ifdef MUTEX_DEBUG
	      "+MUTEX_DEBUG\n"
#else
	      "-MUTEX_DEBUG\n"
#endif
	);
      exit (EXIT_SUCCESS);
    }

  if (gpgme_level != -1)
    {
      char s[2] = { gpgme_level + '0', 0 };

      if (getenv ("GPGME_DEBUG"))
        log_write (_ ("Overriding GPGME_DEBUG environment with level %u!"),
                   gpgme_level);

      gpgme_set_global_flag ("debug", s);
    }

  if (setup_crypto ())
    exit (EXIT_FAILURE);

#ifdef WITH_GNUTLS
  tls_level = tls_level == -1 ? 1 : tls_level;
  gnutls_global_set_log_level (tls_level);
  tls_fd = -1;
  tls6_fd = -1;
#endif
  rc = xml_init ();
  if (rc)
    errx (EXIT_FAILURE, "%s", "xml_init() failed");

  if (!homedir)
#ifdef PWMD_HOMEDIR
    homedir = str_dup(PWMD_HOMEDIR);
#else
    homedir = str_asprintf ("%s/.pwmd", get_home_dir());
#endif

  if (mkdir (homedir, 0700) == -1 && errno != EEXIST)
    err (EXIT_FAILURE, "%s", homedir);

  if (!rcfile)
    rcfile = str_asprintf ("%s/config", homedir);

  pthread_key_create (&last_error_key, free_key);
#ifndef HAVE_PTHREAD_CANCEL
  pthread_key_create (&signal_thread_key, free_key);
#endif

  pthread_mutexattr_t attr;
  pthread_mutexattr_init (&attr);
  pthread_mutexattr_settype (&attr, PTHREAD_MUTEX_RECURSIVE);
  pthread_mutex_init (&rcfile_mutex, &attr);
  global_config = config_parse (rcfile, 0);
  if (!global_config)
    {
      pthread_mutexattr_destroy (&attr);
      pthread_mutex_destroy (&rcfile_mutex);
      exit (EXIT_FAILURE);
    }

  p = config_get_string ("global", "gpg_homedir");
  if (!p)
    datadir = str_asprintf ("%s/.gnupg", homedir);
  else
    datadir = expand_homedir (p);

  xfree (p);
  if (mkdir (datadir, 0700) == -1 && errno != EEXIST)
    err (EXIT_FAILURE, "%s", datadir);

  rc = gpgme_set_engine_info (GPGME_PROTOCOL_OpenPGP, NULL, datadir);
  if (rc)
    errx (EXIT_FAILURE, "%s: %s", datadir, pwmd_strerror (rc));
  xfree (datadir);

  snprintf (buf, sizeof (buf), "%s/data", homedir);
  if (mkdir (buf, 0700) == -1 && errno != EEXIST)
    err (EXIT_FAILURE, "%s", buf);

  datadir = str_dup (buf);
  pthread_cond_init (&rcfile_cond, NULL);
  pthread_mutex_init (&cn_mutex, &attr);
  pthread_mutexattr_destroy (&attr);

  setup_logging ();

  x = config_get_int_param (global_config, "global", "priority", &exists);
  if (exists && x != atoi(INVALID_PRIORITY))
    {
      errno = 0;
      if (setpriority (PRIO_PROCESS, 0, x) == -1)
	{
	  log_write ("setpriority(): %s",
		     pwmd_strerror (gpg_error_from_errno (errno)));
	  goto do_exit;
	}
    }
#ifdef HAVE_MLOCKALL
  if (disable_mlock == 0 && mlockall (MCL_CURRENT | MCL_FUTURE) == -1)
    {
      log_write ("mlockall(): %s",
		 pwmd_strerror (gpg_error_from_errno (errno)));
      goto do_exit;
    }
#endif

  rc = cache_init ();
  if (rc)
    {
      log_write ("pwmd: ERR %i: %s", rc, pwmd_strerror (rc));
      exit (EXIT_FAILURE);
    }

  if (import)
    {
      char **keyids = NULL;

      if (!outfile || !*outfile || argc != optind)
	usage (argv[0], EXIT_FAILURE);

      if (keyid)
        keyids = str_split (keyid, ",", 0);
      else if (!userid && !sym)
        usage (argv[0], EXIT_FAILURE);

      rc = xml_import (import, outfile, keyids, sign_keyid, keyfile, userid,
                       algo, expire, no_passphrase, sym);
      strv_free (keyids);
      if (rc)
        {
          if (gpg_err_source (rc) == GPG_ERR_SOURCE_UNKNOWN)
            rc = gpg_error (rc);

          log_write ("%s: %u: %s", import, rc, pwmd_strerror (rc));
        }

      config_free (global_config);
      xfree (rcfile);
      exit (rc ? EXIT_FAILURE : EXIT_SUCCESS);
    }

  p = config_get_string ("global", "socket_path");
  if (!p)
    p = str_asprintf ("%s/socket", homedir);

  socketarg = expand_homedir (p);
  xfree (p);

  if (!secure)
    disable_list_and_dump = config_get_boolean ("global",
						"disable_list_and_dump");
  else
    disable_list_and_dump = secure;

  cache_push = config_get_list ("global", "cache_push");

  while (optind < argc)
    {
      if (strv_printf (&cache_push, "%s", argv[optind++]) == 0)
	errx (EXIT_FAILURE, "%s", pwmd_strerror (GPG_ERR_ENOMEM));
    }

  if (!strchr (socketarg, '/'))
    {
      socketdir = getcwd (buf, sizeof (buf));
      socketname = str_dup (socketarg);
      socketpath = str_asprintf ("%s/%s", socketdir, socketname);
    }
  else
    {
      socketname = str_dup (strrchr (socketarg, '/')+1);
      socketarg[strlen (socketarg) - strlen (socketname) - 1] = 0;
      socketdir = str_dup (socketarg);
      socketpath = str_asprintf ("%s/%s", socketdir, socketname);
    }

  if (chdir (datadir))
    {
      log_write ("%s: %s", datadir,
		 pwmd_strerror (gpg_error_from_errno (errno)));
      unlink (socketpath);
      goto do_exit;
    }

  x = test_pidfile (socketpath, socketname, buf, sizeof(buf), &pidfile, 0,
                    mode, terminate);
  if (!terminate && x)
    goto do_exit;
  else if (terminate)
    {
      estatus = x != 1 ? EXIT_FAILURE : EXIT_SUCCESS;
      goto do_exit;
    }

  /*
   * bind() doesn't like the full pathname of the socket or any non alphanum
   * characters so change to the directory where the socket is wanted then
   * create it then change to datadir.
   */
  if (chdir (socketdir))
    {
      log_write ("%s: %s", socketdir,
		 pwmd_strerror (gpg_error_from_errno (errno)));
      goto do_exit;
    }

  xfree (socketdir);

#ifndef WITH_GNUTLS
  config_set_boolean ("global", "enable_socket", true);
#endif

  if (config_get_boolean ("global", "enable_socket"))
    {
      if ((sockfd = socket (PF_UNIX, SOCK_STREAM, 0)) == -1)
        {
          log_write ("socket(): %s", pwmd_strerror (gpg_error_from_errno (errno)));
          goto do_exit;
        }

      addr.sun_family = AF_UNIX;
      snprintf (addr.sun_path, sizeof (addr.sun_path), "%s", socketname);
      do_unlink = 1;
      if (bind (sockfd, (struct sockaddr *) &addr, sizeof (struct sockaddr)) ==
          -1)
        {
          log_write ("bind(): %s", pwmd_strerror (gpg_error_from_errno (errno)));

          if (errno == EADDRINUSE)
            {
              do_unlink = 0;
              log_write (_("Either there is another pwmd running or '%s' is a \n"
                           "stale socket. Please remove it manually."), socketpath);
            }

          goto do_exit;
        }

        {
          char *t = config_get_string ("global", "socket_perms");
          mode_t mask;

          if (t)
            {
              mode = strtol (t, NULL, 8);
              mask = umask (0);
              xfree (t);

              if (chmod (socketname, mode) == -1)
                {
                  log_write ("%s: %s", socketname,
                             pwmd_strerror (gpg_error_from_errno (errno)));
                  close (sockfd);
                  umask (mask);
                  goto do_exit;
                }

              umask (mask);
            }
        }
    }

  if (chdir (datadir))
    {
      log_write ("%s: %s", datadir,
		 pwmd_strerror (gpg_error_from_errno (errno)));
      close (sockfd);
      goto do_exit;
    }

  xfree (datadir);
#ifdef WITH_GNUTLS
  if (config_get_boolean ("global", "enable_tcp"))
    {
      if (!tls_start_stop (0))
        {
          close (sockfd);
          goto do_exit;
        }
    }
  else if (!config_get_boolean ("global", "enable_socket"))
    {
      fprintf (stderr, "%s\n", _("Both TCP and UDS sockets are disabled in "
                                 "the configuration. Exiting."));
      goto do_exit;
    }
#endif

  /*
   * Set the cache entry for a file. Prompts for the password.
   */
  if (cache_push)
    {
      for (opt = 0; cache_push[opt]; opt++)
	{
          struct crypto_s *crypto = NULL;
          char *pw_file = config_get_string (cache_push[opt],
                                             "passphrase_file");
          rc = crypto_init (&crypto, NULL, cache_push[opt], pw_file != NULL,
                            pw_file);

          if (!rc)
            {
              crypto->flags |= pw_file ? CRYPTO_FLAG_KEYFILE : 0;
              crypto->keyfile = pw_file;
            }
          else
            xfree (pw_file);

          if (rc)
            {
              estatus = EXIT_FAILURE;
              goto do_exit;
            }

	  rc = do_cache_push (crypto);
          if (rc && !force)
	    {
              log_write ("ERR %u: %s", rc, pwmd_strerror(rc));
	      strv_free (cache_push);
              log_write (_ ("Failed to add a file to the cache. Use --force to force startup. Exiting."));
              cache_clear (NULL, NULL, 1, 0);
	      estatus = EXIT_FAILURE;
	      crypto_free (crypto);
              (void)cache_kill_scd ();
	      goto do_exit;
	    }
          else if (rc)
            log_write ("%s: %s", crypto->filename, pwmd_strerror(rc));
          else
            log_write (_("Successfully added '%s' to the cache."),
                       crypto->filename);

	  crypto_free (crypto);
	}

      (void)cache_kill_scd ();
      strv_free (cache_push);
      log_write (!nofork ? _("Done. Daemonizing...") :
		 _("Done. Waiting for connections..."));
    }

  if (sockfd != -1)
    {
      int backlog = config_get_integer ("global", "backlog");
      if (listen (sockfd, backlog) == -1)
        {
          log_write ("listen(): %s", pwmd_strerror (gpg_error_from_errno (errno)));
          goto do_exit;
        }
    }

  /* A client should set these with the OPTION command. */
  gpgrt_unsetenv ("DISPLAY");
  gpgrt_unsetenv ("GPG_TTY");
  gpgrt_unsetenv ("TERM");

  if (!nofork)
    {
      switch (fork ())
	{
	case -1:
	  log_write ("fork(): %s",
		     pwmd_strerror (gpg_error_from_errno (errno)));
	  goto do_exit;
	case 0:
	  close (0);
	  close (1);
	  close (2);
	  setsid ();
	  break;
	default:
	  _exit (EXIT_SUCCESS);
	}
    }

  (void)test_pidfile (socketpath, socketname, buf, sizeof(buf), &pidfile, 1,
		      mode, 0);
  xfree (socketname);
  cmdline = 0;
  pthread_key_create (&thread_name_key, free_key);
  pthread_setspecific (thread_name_key, str_asprintf ("!%s", __FUNCTION__));
  estatus = server_loop (sockfd, &socketpath);

do_exit:
  if (socketpath && do_unlink)
    {
      unlink (socketpath);
      xfree (socketpath);
    }

  xfree (socketarg);
#ifdef WITH_GNUTLS
  gnutls_global_deinit ();
  tls_deinit_params ();
#endif
  pthread_cond_destroy (&rcfile_cond);
  pthread_mutex_destroy (&rcfile_mutex);
  pthread_key_delete (last_error_key);
#ifndef HAVE_PTHREAD_CANCEL
  pthread_key_delete (signal_thread_key);
#endif

  if (global_config)
    config_free (global_config);

  free_invoking_users (invoking_users);
  xfree (rcfile);
  xfree (home_directory);
  xfree (homedir);
  xml_deinit ();

  if (pidfile)
    unlink (pidfile);
  xfree (pidfile);

  if (estatus == EXIT_SUCCESS && !terminate)
    log_write (_("pwmd exiting normally"));

  pthread_key_delete (thread_name_key);
  closelog ();

  if (log_fd != -1)
    close (log_fd);

  xfree (logfile);
  exit (estatus);
}

gpg_error_t lock_flock (assuan_context_t ctx, const char *filename,
                        int type, int *fd)
{
  gpg_error_t rc = 0;

#ifdef HAVE_FLOCK
  rc = open_check_file (filename, fd, NULL, 1);
  if (rc)
    return rc;

  TRY_FLOCK (ctx, *fd, type, rc);
  if (rc)
    {
      close (*fd);
      *fd = -1;
    }
#endif

  return rc;
}

void unlock_flock (int *fd)
{
#ifdef HAVE_FLOCK
  if (*fd != -1)
    close (*fd);

  *fd = -1;
#endif
}
