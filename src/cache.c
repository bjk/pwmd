/*
    Copyright (C) 2006-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of pwmd.

    Pwmd is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.

    Pwmd is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Pwmd.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#include <time.h>
#include <pthread.h>
#include <ctype.h>
#include <errno.h>

#include "pwmd-error.h"
#include <gcrypt.h>
#include "mutex.h"
#include "cache.h"
#include "status.h"
#include "mem.h"
#include "util-misc.h"
#include "util-string.h"
#include "agent.h"
#include "acl.h"

#define CACHE_FLAG_LOCKED	0x0001

struct plaintext_s
{
  xmlDocPtr plaintext;
  unsigned refcount;
  char *filename;
  int modified;
};

static pthread_mutex_t cache_mutex;
static struct slist_s *key_cache;
static struct slist_s *plaintext_list;
static unsigned char *cache_iv;
static unsigned char *cache_key;
static size_t cache_blocksize;
static size_t cache_keysize;
static struct agent_s *cache_agent;
static int exiting;

void log_write (const char *fmt, ...);

static gpg_error_t clear_once (file_cache_t * p, int agent, int force);
static int remove_entry (const char *);
static void free_entry (file_cache_t * p, int agent);

static file_cache_t *
get_entry (const char *filename)
{
  if (!filename)
    return NULL;

  MUTEX_LOCK (&cache_mutex);

  int t = slist_length (key_cache);
  int i;
  file_cache_t *p = NULL;

  for (i = 0; i < t; i++)
    {
      p = slist_nth_data (key_cache, i);
      if (!strcmp (p->filename, filename))
        break;
      p = NULL;
    }

  MUTEX_UNLOCK (&cache_mutex);
  return p;
}

gpg_error_t
cache_lock_mutex (void *ctx, const char *filename, long lock_timeout, int add,
                  long timeout)
{
  gpg_error_t rc = 0;
  file_cache_t *p = get_entry (filename);

  if (p)
    {
      MUTEX_TRYLOCK (ctx, p->mutex, rc, lock_timeout);
      if (!rc)
        p->flags |= CACHE_FLAG_LOCKED;
    }
  else if (add)
    {
      rc = cache_add_file (filename, NULL, timeout, 0);
      if (!rc)
	{
	  p = get_entry (filename);
          if (p)
            {
              MUTEX_TRYLOCK (ctx, p->mutex, rc, lock_timeout);
              if (!rc)
                p->flags |= CACHE_FLAG_LOCKED;
            }
        }
    }

  return !p && !rc ? GPG_ERR_NO_DATA : rc;
}

static int
remove_entry (const char *filename)
{
  MUTEX_LOCK (&cache_mutex);

  file_cache_t *p = get_entry (filename);

  if (p)
    {
      /* Keep a refcount because another client may be editing this same new
       * file. The entry needs to be kept in case the other client SAVE's.
       */
      if (!(p->refcount-1))
	free_entry (p, 0);
      else
        {
          p->refcount--;
          if ((p->flags & CACHE_FLAG_LOCKED))
            {
              p->flags &= ~CACHE_FLAG_LOCKED;
              MUTEX_UNLOCK (p->mutex);
            }
        }
    }

  MUTEX_UNLOCK (&cache_mutex);
  return p ? 1 : 0;
}

static gpg_error_t
free_cache_data (file_cache_t *cache, int force)
{
  gpg_error_t rc = GPG_ERR_NO_DATA;
  int i, t;
  struct client_thread_s *found = NULL;
  int self = 0;

  if (!cache->data)
    return 0;

  MUTEX_LOCK (&cn_mutex);
  t = slist_length (cn_thread_list);

  /* Prevent clearing the cache entry when there are any connected clients
   * using this cache entry to prevent clearing the plaintext which would cause
   * cache_plaintext_set() to return GPG_ERR_NO_DATA and prevent CoW commands
   * from working. */
  for (i = 0; i < t; i++)
    {
      struct client_thread_s *thd = slist_nth_data (cn_thread_list, i);

      if (!thd->cl)
	continue;

      if (thd->cl->filename && cache->filename
          && !strcmp (thd->cl->filename, cache->filename)
          && !force)
        {
          cache->defer_clear = 1;
          MUTEX_UNLOCK (&cn_mutex);
          return 0;
        }
    }

  pthread_cleanup_push (release_mutex_cb, &cn_mutex);
  MUTEX_LOCK (&cache_mutex);

  for (i = 0; i < t; i++)
    {
      struct client_thread_s *thd = slist_nth_data (cn_thread_list, i);

      if (!thd->cl)
	continue;

      self = pthread_equal (pthread_self (), thd->tid);

      if (thd->cl->filename && cache->filename
          && !strcmp (thd->cl->filename, cache->filename))
	{
	  if (self)
	    {
	      found = thd;
	      continue;
	    }

	  /* Continue trying to find a client who has the same file open and
	   * also has a lock. */
	  rc = cache_lock_mutex (thd->cl->ctx, thd->cl->filename, -1, 0, -1);
	  if (!rc)
	    {
	      found = thd;
	      break;
	    }
	}
    }

  if (self && (!rc || rc == GPG_ERR_NO_DATA) && found)
    rc = cache_lock_mutex (found->cl->ctx, found->cl->filename, -1, 0, -1);

  if (exiting || !rc || rc == GPG_ERR_NO_DATA)
    {
      cache_free_data_once (cache->data);
      cache->data = NULL;
      cache->defer_clear = 0;
      cache->timeout = (long)-1;

      if (found)
	cache_unlock_mutex (found->cl->filename, 0);

      rc = 0;
    }

  if (rc)
    cache->defer_clear = 1;

  MUTEX_UNLOCK (&cache_mutex);
  pthread_cleanup_pop (1);
  return rc;
}

gpg_error_t
cache_unlock_mutex (const char *filename, int remove)
{
  MUTEX_LOCK (&cache_mutex);
  file_cache_t *p = get_entry (filename);

  if (p)
    {
      if (remove)
	remove_entry (filename);
      else
        {
          p->flags &= ~CACHE_FLAG_LOCKED;
          MUTEX_UNLOCK (p->mutex);
        }
    }

  MUTEX_UNLOCK (&cache_mutex);
  return p ? 0 : GPG_ERR_NO_DATA;
}

static gpg_error_t
get_keyinfo (const char *grip, char ***r_fields)
{
  gpg_error_t rc;
  char *line;

  rc = agent_command (cache_agent, &line, NULL, "KEYINFO --data %s", grip);
  if (rc)
    return rc;

  *r_fields = str_split (line, " ", 0);
  xfree (line);
  return !(*r_fields) ? GPG_ERR_ENOMEM : rc;
}

gpg_error_t
cache_is_shadowed (const char *filename)
{
  MUTEX_LOCK (&cache_mutex);
  file_cache_t *f = get_entry (filename);
  char **p;
  char **fields = NULL;
  gpg_error_t rc = GPG_ERR_NO_DATA, trc;

  if (!f)
    return GPG_ERR_NOT_FOUND;

  pthread_cleanup_push (release_mutex_cb, &cache_mutex);

  for (p = f->grip; p && *p; p++)
    {
      trc = get_keyinfo (*p, &fields);
      if (!trc && *fields[1] == 'T')
        {
          rc = 0;
          break;
        }
      else if (trc)
        {
          rc = trc;
          break;
        }
    }

  strv_free (fields);
  fields = NULL;

  if (rc == GPG_ERR_NO_DATA && f->siggrip)
    {
      trc = get_keyinfo (f->siggrip, &fields);
      if (!trc && *fields[1] == 'T')
        rc = 0;
      else if (trc)
        rc = trc;
    }

  strv_free (fields);
  pthread_cleanup_pop (1);
  return rc;
}

static gpg_error_t
test_agent_cache_once (file_cache_t *p, const char *grip)
{
  gpg_error_t rc;
  char **fields = NULL;

  (void)p;
  rc = get_keyinfo (grip, &fields);
  if (rc)
    return rc;

  /* Smartcard. */
  if (*fields[1] == 'T')
    rc = GPG_ERR_NO_DATA;
  else
    rc = isdigit (*fields[4]) || *fields[5] == 'C' ? 0 : GPG_ERR_NO_DATA;

  strv_free (fields);
  return rc;
}

/* Test each keygrip in data->(sig)grip for gpg-agent cache status. The file is
 * considered cached if at least one grip is cached in the agent or one siggrip
 * is cached in the case of 'sign'.
 */
static gpg_error_t
test_agent_cache (file_cache_t *data, int sign)
{
  gpg_error_t rc = 0;
  char **p;

  if ((!data->grip && !sign) || (!data->siggrip && sign))
    return GPG_ERR_NO_DATA;

  for (p = data->grip; !sign && p && *p; p++)
    {
      rc = test_agent_cache_once (data, *p);
      if (!rc || rc != GPG_ERR_NO_DATA)
        break;
    }

  if (!rc && sign)
    rc = test_agent_cache_once (data, data->siggrip);

  return rc;
}

static gpg_error_t
extract_keygrip_once (struct crypto_s *crypto, char **keyids, int sign,
                      char ***dst)
{
  gpgme_key_t *result;
  gpgme_error_t rc;
  int i;
  char **grips = NULL;

  rc = crypto_list_keys (crypto, keyids, sign, &result);
  if (rc)
    return rc;

  for (i = 0; result[i]; i++)
    {
      gpgme_subkey_t s;

      for (s = result[i]->subkeys; s; s = s->next)
        {
          char **k;

          if (sign && !s->can_sign)
            continue;

          for (k = keyids; *k; k++)
            {
              if (!strcmp (*k, s->keyid) && s->keygrip)
                {
                  char **tmp;
                  size_t len = strv_length (grips);

                  tmp = xrealloc (grips, len+2 * sizeof (char *));
                  if (!tmp)
                    {
                      rc = GPG_ERR_ENOMEM;
                      break;
                    }

                  grips = tmp;
                  grips[len] = str_dup (s->keygrip);
                  if (!grips[len])
                    {
                      rc = GPG_ERR_ENOMEM;
                      break;
                    }

                  grips[++len] = NULL;
                }
            }
        }
    }

  if (!rc)
    {
      strv_free (*dst);
      *dst = grips;
    }
  else
    {
      strv_free (grips);
      grips = NULL;
    }

  crypto_free_key_list (result);
  return rc ? rc : grips ? rc : GPG_ERR_NO_DATA;
}

static gpg_error_t
extract_keygrips (file_cache_t *data)
{
  gpg_error_t rc = 0;
  struct crypto_s *crypto;
  char **grips = NULL, **siggrips = NULL;

  if (!data || !data->data)
    return 0;

  rc = crypto_init (&crypto, NULL, NULL, 0, NULL);
  if (rc)
    return rc;

  pthread_cleanup_push ((void *)crypto_free, crypto);

  if (data->data->pubkey)
    rc = extract_keygrip_once (crypto, data->data->pubkey, 0, &grips);

  if (!rc && data->data->sigkey)
    {
      char **tmp = NULL;

      strv_printf (&tmp, "%s", data->data->sigkey);
      rc = extract_keygrip_once (crypto, tmp, 1, &siggrips);
      strv_free (tmp);
    }

  if (!rc)
    {
      strv_free (data->grip);
      data->grip = grips;
      xfree (data->siggrip);
      data->siggrip = NULL;
      if (siggrips && *siggrips)
        data->siggrip = str_dup (*siggrips);
      strv_free (siggrips);
    }
  else
    {
      strv_free (grips);
      strv_free (siggrips);
    }

  pthread_cleanup_pop (1);
  return rc;
}

static gpg_error_t
iscached (const char *filename, int *defer, int agent, int sign)
{
  file_cache_t *p = get_entry (filename);
  gpg_error_t rc = 0;

  if (!agent)
    {
      if (!p || !p->data)
        return GPG_ERR_NO_DATA;

      if (defer)
        *defer = p->defer_clear;

      return 0;
    }

  if (!p)
    return GPG_ERR_NO_DATA;

  if (defer)
    *defer = p->defer_clear;

  if (!rc && (p->grip || p->siggrip))
    rc = test_agent_cache (p, sign);
  else if (!rc)
    rc = GPG_ERR_NO_DATA;

  return rc;
}

unsigned
cache_file_count ()
{
  MUTEX_LOCK (&cache_mutex);
  unsigned total = 0, i, n;

  pthread_cleanup_push (release_mutex_cb, &cache_mutex);
  n = slist_length (key_cache);
  for (i = 0; i < n; i++)
    {
      file_cache_t *p = slist_nth_data (key_cache, i);

      if (!iscached (p->filename, NULL, 0, 0))
	total++;
    }

  pthread_cleanup_pop (1);
  return total;
}

void
cache_adjust_timeout ()
{
  MUTEX_LOCK (&cache_mutex);
  int t = slist_length (key_cache);
  int i;

  pthread_cleanup_push (release_mutex_cb, &cache_mutex);

  for (i = 0; i < t; i++)
    {
      file_cache_t *p = slist_nth_data (key_cache, i);

      if (p->timeout > 0)
	p->timeout--;

      /* Test the cache entry timeout for expiration and also whether the file
       * is a new one (no crc) and without any associated clients (refcount ==
       * 1) and remove the cache entry accordingly. */
      if (!p->timeout || (p->defer_clear && p->timeout != (long)-1)
          || (p->timeout > 0 && p->refcount == 1
              && (!p->data || !p->data->crc)
              && !(p->flags & CACHE_FLAG_LOCKED)))
	{
	  /* The file associated with this cache entry may be locked by a
	   * client. Defer freeing this cache entry until the next timeout
	   * check. */
	  gpg_error_t rc = clear_once (p, 1, 0);

          if (!rc && !p->defer_clear)
            {
              free_entry (p, 0);
              t = slist_length (key_cache);
              send_status_all (STATUS_CACHE, NULL);
            }
	}
    }

  pthread_cleanup_pop (1);
}

static gpg_error_t
set_timeout (const char *filename, long timeout, int defer, int force)
{
  MUTEX_LOCK (&cache_mutex);
  file_cache_t *p = get_entry (filename);
  gpg_error_t rc = 0;

  pthread_cleanup_push (release_mutex_cb, &cache_mutex);

  if (p)
    {
      p->reset = timeout;
      if (!p->defer_clear && (p->timeout == -1 || force))
        p->timeout = timeout;

      if (!timeout || defer)
        {
          rc = clear_once (p, 1, 0);
          if (!rc)
            send_status_all (STATUS_CACHE, NULL);
        }
    }
  else
    rc = GPG_ERR_NOT_FOUND;

  pthread_cleanup_pop (1);
  return rc;
}

gpg_error_t
cache_set_data (const char *filename, struct cache_data_s *data)
{
  MUTEX_LOCK (&cache_mutex);
  gpg_error_t rc = GPG_ERR_NO_DATA;
  file_cache_t *p = get_entry (filename);

  pthread_cleanup_push (release_mutex_cb, &cache_mutex);

  if (p)
    {
      xfree (data->filename);
      data->filename = NULL;
      if (p->filename)
        data->filename = str_dup (p->filename);

      p->data = data;
      rc = set_timeout (filename, p->reset, p->defer_clear, 0);
      if (!rc && !p->reset)
	{
	  clear_once (p, 0, 0);
	  send_status_all (STATUS_CACHE, NULL);
	}
      else if (!rc)
        rc = extract_keygrips (p);
    }

  pthread_cleanup_pop (1);
  return p && !rc ? 0 : rc;
}

struct cache_data_s *
cache_get_data (const char *filename, int *defer)
{
  MUTEX_LOCK (&cache_mutex);
  file_cache_t *p = get_entry (filename);

  if (defer && p)
    *defer = p->defer_clear;
  MUTEX_UNLOCK (&cache_mutex);
  return p ? p->data : NULL;
}

gpg_error_t
cache_add_file (const char *filename, struct cache_data_s *data, long timeout,
                int same_file)
{
  MUTEX_LOCK (&cache_mutex);
  file_cache_t *p = get_entry (filename);
  struct slist_s *new;
  gpg_error_t rc = 0;

  pthread_cleanup_push (release_mutex_cb, &cache_mutex);

  if (p)
    {
      if (data)
        {
          char *str = p->filename ? str_dup (p->filename) : NULL;

          if (p->filename && !str)
            rc = GPG_ERR_ENOMEM;
          else
            {
              xfree (data->filename);
              data->filename = str;
            }
        }

      if (!rc)
        {
          p->data = data;

          if (!same_file)
            p->refcount++;

          rc = set_timeout (filename, timeout, p->defer_clear, 0);
        }
    }
  else
    {
      p = xcalloc (1, sizeof (file_cache_t));
      if (p)
        {
          p->mutex = (pthread_mutex_t *) xmalloc (sizeof (pthread_mutex_t));
          if (p->mutex)
            {
              pthread_mutexattr_t attr;
              pthread_mutexattr_init (&attr);
              pthread_mutexattr_settype (&attr, PTHREAD_MUTEX_RECURSIVE);
              pthread_mutex_init (p->mutex, &attr);
              pthread_mutexattr_destroy (&attr);
              p->filename = str_dup (filename);

              if (data)
                {
                  xfree (data->filename);
                  data->filename = NULL;
                  data->filename = str_dup (p->filename);
                }

              p->data = data;
              p->refcount++;
              new = slist_append (key_cache, p);
              if (new)
                {
                  key_cache = new;
                  rc = cache_set_timeout(filename, timeout);
                }
              else
                {
                  pthread_mutex_destroy (p->mutex);
                  xfree (p->mutex);
                  xfree (p->filename);
                  xfree (p);
                  p = NULL;
                  rc = GPG_ERR_ENOMEM;
                }
            }
          else
            {
              xfree (p);
              p = NULL;
              rc = GPG_ERR_ENOMEM;
            }
        }
      else
        rc = GPG_ERR_ENOMEM;
    }

  if (!rc)
    rc = extract_keygrips (p);

  pthread_cleanup_pop (1);

  if (!rc)
    send_status_all (STATUS_CACHE, NULL);

  return rc;
}

gpg_error_t
cache_clear_agent_keys (const char *filename, int decrypt, int sign)
{
  MUTEX_LOCK (&cache_mutex);
  gpg_error_t rc = 0;
  char **key;
  file_cache_t *p = get_entry (filename);

  pthread_cleanup_push (release_mutex_cb, &cache_mutex);

  if (p)
    {
      for (key = p->grip; decrypt && key && *key; key++)
        {
          rc = agent_command (cache_agent, NULL, NULL,
                              "CLEAR_PASSPHRASE --mode=normal %s", *key);
          if (rc)
            break;
        }

      if (p->siggrip && sign)
        rc = agent_command (cache_agent, NULL, NULL,
                            "CLEAR_PASSPHRASE --mode=normal %s", p->siggrip);
    }
  else
    rc = GPG_ERR_NOT_FOUND;

  pthread_cleanup_pop (1);
  return rc;
}

static gpg_error_t
clear_once (file_cache_t *p, int agent, int force)
{
  gpg_error_t rc = 0, trc;

  if (agent)
    rc = cache_clear_agent_keys (p->filename, 1, 1);

  trc = free_cache_data (p, force);
  return rc ? rc : trc;
}

static void
free_entry (file_cache_t *p, int agent)
{
  gpg_error_t rc;

  if (!p)
    return;

  rc = clear_once (p, agent, 0);
  if (rc)
    {
      log_write ("%s(): %s", __FUNCTION__, pwmd_strerror (rc));
      return;
    }

  key_cache = slist_remove (key_cache, p);

  if (p->mutex)
    {
      if ((p->flags & CACHE_FLAG_LOCKED))
        MUTEX_UNLOCK (p->mutex);

      pthread_mutex_destroy (p->mutex);
      xfree (p->mutex);
    }

  strv_free (p->grip);
  xfree (p->siggrip);
  xfree (p->filename);
  xfree (p);
}

gpg_error_t
cache_clear (struct client_s *client, const char *filename, int agent,
             int force)
{
  file_cache_t *p;
  gpg_error_t rc = 0, all_rc = 0;
  int i, t;

  MUTEX_LOCK (&cache_mutex);
  pthread_cleanup_push (release_mutex_cb, &cache_mutex);

  if (filename)
    {
      p = get_entry (filename);
      if (p)
	{
          rc = clear_once (p, agent, force);
          if (rc)
            log_write ("%s(): %s", __FUNCTION__, pwmd_strerror (rc));
        }
    }
  else
    {
      t = slist_length (key_cache);
      for (i = 0; i < t; i++)
        {
          p = slist_nth_data (key_cache, i);

          if (client)
            {
              assuan_peercred_t peer;

              rc = do_validate_peer (client->ctx, p->filename, &peer, NULL);
              if (rc == GPG_ERR_FORBIDDEN)
                rc = peer_is_invoker (client);

              if (rc)
                {
                  all_rc = !all_rc ? rc : all_rc;
                  continue;
                }
            }

          clear_once (p, agent, 0);
        }
    }

  pthread_cleanup_pop (1);
  return filename ? rc : all_rc;
}

gpg_error_t
cache_iscached (const char *filename, int *defer, int agent, int sign)
{
  gpg_error_t rc;

  if (!filename)
    return GPG_ERR_INV_PARAMETER;

  MUTEX_LOCK (&cache_mutex);
  pthread_cleanup_push (release_mutex_cb, &cache_mutex);
  rc = iscached (filename, defer, agent, sign);

  /* Test if the data file disappeared from the filesystem. */
  if ((!rc || gpg_err_code (rc) == GPG_ERR_NO_DATA)
      && access (filename, R_OK) == -1)
    {
      rc = gpg_error_from_errno (errno);
      if (gpg_err_code (rc) == GPG_ERR_ENOENT)
	{
          /* Fixes clearing the cache entry that was created during OPEN when
           * SAVE'ing a new file. */
	  if ((defer && *defer == 1) || !defer)
	    {
	      rc = cache_defer_clear (filename);
	      if (!rc && defer)
		*defer = 1;
	    }

          rc = GPG_ERR_ENOENT;
	}
    }

  pthread_cleanup_pop (1);
  return rc;
}

gpg_error_t
cache_defer_clear (const char *filename)
{
  MUTEX_LOCK (&cache_mutex);
  file_cache_t *p = get_entry (filename);
  gpg_error_t rc = 0;

  if (!p)
    rc = GPG_ERR_NOT_FOUND;
  else
    p->defer_clear = 1;

  MUTEX_UNLOCK (&cache_mutex);
  return rc;
}

gpg_error_t
cache_set_timeout (const char *filename, long timeout)
{
  return set_timeout (filename, timeout, 0, 1);
}

void
cache_deinit ()
{
  exiting = 1;
  MUTEX_LOCK (&cache_mutex);
  int i, t = slist_length (key_cache);

  pthread_cleanup_push (release_mutex_cb, &cache_mutex);
  for (i = 0; i < t; i++)
    {
      file_cache_t *p = slist_nth_data (key_cache, i);

      free_entry (p, 1);
      t = slist_length (key_cache);
      i = -1;
    }

  gcry_free (cache_key);
  xfree (cache_iv);
  cache_key = NULL;
  cache_iv = NULL;
  agent_free (cache_agent);
  cache_agent = NULL;
  pthread_cleanup_pop (1);
  pthread_mutex_destroy (&cache_mutex);
}

void
cache_mutex_init ()
{
  pthread_mutexattr_t attr;

  pthread_mutexattr_init (&attr);
  pthread_mutexattr_settype (&attr, PTHREAD_MUTEX_RECURSIVE);
  pthread_mutex_init (&cache_mutex, &attr);
  pthread_mutexattr_destroy (&attr);
}

gpg_error_t
cache_init ()
{
  gpg_error_t rc = 0;

  rc = agent_init (&cache_agent);
  if (rc)
    {
      cache_agent = NULL;
      return rc;
    }

  if (!cache_key)
    {
      rc = gcry_cipher_algo_info (GCRY_CIPHER_AES128, GCRYCTL_GET_BLKLEN, NULL,
				  &cache_blocksize);
      if (rc)
	return rc;

      rc = gcry_cipher_algo_info (GCRY_CIPHER_AES128, GCRYCTL_GET_KEYLEN, NULL,
				  &cache_keysize);
      if (rc)
	return rc;

      cache_key = gcry_malloc (cache_keysize);
      if (!cache_key)
	return GPG_ERR_ENOMEM;

      cache_iv = xmalloc (cache_blocksize);
      if (!cache_iv)
	{
	  gcry_free (cache_key);
	  cache_key = NULL;
	  return GPG_ERR_ENOMEM;
	}

      gcry_randomize (cache_key, cache_keysize, GCRY_STRONG_RANDOM);
      gcry_create_nonce (cache_iv, cache_blocksize);
    }


  cache_mutex_init ();
  return rc;
}

void
cache_lock ()
{
  MUTEX_LOCK (&cache_mutex);
}

void
cache_unlock ()
{
  MUTEX_UNLOCK (&cache_mutex);
}

static void
update_plaintext_pointer (xmlDocPtr plaintext)
{
  unsigned i;
  unsigned t;

  t = slist_length (key_cache);
  for (i = 0; i < t; i++)
    {
      file_cache_t *p = slist_nth_data (key_cache, i);

      if (p->data && p->data->plaintext == plaintext)
        p->data->plaintext = NULL;
    }
}

void
cache_free_data_once (struct cache_data_s *data)
{
  if (!data)
    return;

  xfree (data->filename);
  strv_free (data->pubkey);
  xfree (data->sigkey);
  xfree (data->crc);
  xfree (data->doc);
  xfree (data);
}

static void
release_cipher (void *arg)
{
  gcry_cipher_close ((gcry_cipher_hd_t) arg);
}

gpg_error_t
cache_encrypt (struct crypto_s *crypto)
{
  gpg_error_t rc;
  gcry_cipher_hd_t h;
  size_t len = crypto->plaintext_size;

  rc = gcry_cipher_open (&h, GCRY_CIPHER_AES128, GCRY_CIPHER_MODE_CBC, 0);
  if (rc)
    return rc;

  if (len % cache_blocksize)
    {
      unsigned char *p;

      len += cache_blocksize - (len % cache_blocksize);
      p = xrealloc (crypto->plaintext, len * sizeof (unsigned char));
      if (!p)
        {
          gcry_cipher_close (h);
          return GPG_ERR_ENOMEM;
        }

      crypto->plaintext = p;
      memset (&crypto->plaintext[crypto->plaintext_size], 0,
              len - crypto->plaintext_size);
    }

  pthread_cleanup_push (release_cipher, h);
  rc = gcry_cipher_setiv (h, cache_iv, cache_blocksize);
  if (!rc)
    {
      rc = gcry_cipher_setkey (h, cache_key, cache_keysize);
      if (!rc)
        {
          unsigned char *buf = xmalloc (len);

          pthread_cleanup_push (xfree, buf);

          if (!buf)
            rc = GPG_ERR_ENOMEM;

          if (!rc)
            {
              rc = gcry_cipher_encrypt (h, buf, len, crypto->plaintext, len);
              if (!rc)
                {
                  xfree (crypto->plaintext);
                  crypto->plaintext = buf;
                  crypto->plaintext_size = len;
                }
            }

          pthread_cleanup_pop (rc != 0);
        }
    }

  pthread_cleanup_pop (1);
  return rc;
}

gpg_error_t
cache_decrypt (struct crypto_s *crypto)
{
  gcry_cipher_hd_t h = NULL;
  gpg_error_t rc;

  rc = gcry_cipher_open (&h, GCRY_CIPHER_AES128, GCRY_CIPHER_MODE_CBC, 0);
  if (rc)
    return rc;

  if (!rc)
    rc = gcry_cipher_setiv (h, cache_iv, cache_blocksize);

  if (!rc)
    rc = gcry_cipher_setkey (h, cache_key, cache_keysize);

  pthread_cleanup_push (release_cipher, h);

  if (!rc)
    {
      rc = gcry_cipher_decrypt (h, crypto->plaintext, crypto->plaintext_size,
                                NULL, 0);
      if (rc || strncmp ((char *)crypto->plaintext, "<?xml ", 6))
        {
          if (!rc)
            rc = GPG_ERR_BAD_DATA;
        }
    }

  pthread_cleanup_pop (1);
  return rc;
}

gpg_error_t
cache_kill_scd ()
{
  gpg_error_t rc;

  MUTEX_LOCK (&cache_mutex);
  pthread_cleanup_push (release_mutex_cb, &cache_mutex);
  rc = agent_kill_scd (cache_agent);
  pthread_cleanup_pop (1);
  return rc;
}

gpg_error_t
cache_agent_command (const char *cmd)
{
  gpg_error_t rc;

  MUTEX_LOCK (&cache_mutex);
  rc = agent_command (cache_agent, NULL, NULL, "%s", cmd);
  MUTEX_UNLOCK (&cache_mutex);
  return rc;
}

static gpg_error_t
plaintext_get (struct cache_data_s *data, xmlDocPtr *r_doc)
{
  unsigned i;
  unsigned t = slist_length (plaintext_list);

  assert (r_doc != NULL);

  for (i = 0; i < t; i++)
    {
      struct plaintext_s *p = slist_nth_data (plaintext_list, i);

      if (!strcmp (p->filename, data->filename) && !p->modified)
        {
          *r_doc = p->plaintext;
          p->refcount++;
          return 0;
        }
    }

  return GPG_ERR_NO_DATA;
}

gpg_error_t
cache_plaintext_get (const char *filename, xmlDocPtr *r_doc)
{
  file_cache_t *p;
  gpg_error_t rc = 0;

  MUTEX_LOCK (&cache_mutex);

  p = get_entry (filename);
  if (p && p->data)
    rc = plaintext_get (p->data, r_doc);
  else
    rc = GPG_ERR_NO_DATA;

  MUTEX_UNLOCK (&cache_mutex);
  return rc;
}

static gpg_error_t
release_plaintext (xmlDocPtr *plaintext)
{
  unsigned i;
  unsigned t;

  if (!plaintext)
    return GPG_ERR_NO_DATA;

  t = slist_length (plaintext_list);
  for (i = 0; i < t; i++)
    {
      struct plaintext_s *p = slist_nth_data (plaintext_list, i);

      if (p->plaintext == *plaintext)
        {
          assert (p->refcount > 0);
          if (--p->refcount == 0)
            {
              xmlFreeDoc (p->plaintext);
              xfree (p->filename);
              plaintext_list = slist_remove (plaintext_list, p);
              xfree (p);
              update_plaintext_pointer (*plaintext);
              *plaintext = NULL;
            }

          return 0;
        }
    }

  return GPG_ERR_NO_DATA;
}

gpg_error_t
cache_plaintext_release (xmlDocPtr *plaintext)
{
  gpg_error_t rc = 0;

  MUTEX_LOCK (&cache_mutex);
  rc = release_plaintext (plaintext);
  MUTEX_UNLOCK (&cache_mutex);
  return rc;
}

gpg_error_t
cache_plaintext_set (const char *filename, xmlDocPtr doc, int modified)
{
  file_cache_t *p;
  gpg_error_t rc = 0;

  MUTEX_LOCK (&cache_mutex);

  p = get_entry (filename);
  if (p && p->data)
    {
      struct plaintext_s *s = xcalloc (1, sizeof (struct plaintext_s));

      if (s)
        {
          s->filename = str_dup (p->data->filename);
          if (s->filename)
            {
              struct slist_s *l;
              int i, t = slist_length (plaintext_list);

              /* Prevent cache_plaintext_get() from returning the other
               * client's document copy. */
              for (i = 0; i < t; i++)
                {
                  struct plaintext_s *x = slist_nth_data (plaintext_list, i);

                  if (!strcmp (s->filename, x->filename))
                    x->modified = 1;
                }

              l = slist_append (plaintext_list, s);
              if (l)
                {
                  plaintext_list = l;
                  s->plaintext = p->data->plaintext = doc;
                  s->modified = modified;
                  s->refcount = 1;
                }
              else
                {
                  xfree (s->filename);
                  rc = GPG_ERR_ENOMEM;
                }
            }
          else
            rc = GPG_ERR_ENOMEM;

          if (rc)
            xfree (s);
        }
      else
        rc = GPG_ERR_ENOMEM;
    }
  else
    rc = GPG_ERR_NO_DATA;

  MUTEX_UNLOCK (&cache_mutex);
  return rc;
}
