/*
    Copyright (C) 2006-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of pwmd.

    Pwmd is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.

    Pwmd is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Pwmd.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef COMMANDS_H
#define COMMANDS_H

/* Flags needed to be retained for a client across commands. */
#define FLAG_NEW		0x0001
#define FLAG_HAS_LOCK		0x0002
#define FLAG_LOCK_CMD		0x0004
#define FLAG_NO_PINENTRY	0x0008
#define FLAG_OPEN		0x0010
#define FLAG_KEEP_LOCK		0x0020
#define FLAG_ACL_IGNORE		0x0040 // ATTR LIST
#define FLAG_ACL_ERROR		0x0080 // ...
#define FLAG_NO_INHERIT_ACL	0x0100

gpg_error_t register_commands (assuan_context_t ctx);
int valid_filename (const char *filename);
void reset_client (struct client_s *client);
void init_commands ();
void deinit_commands ();

#endif
