/*
    Copyright (C) 2006-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of pwmd.

    Pwmd is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.

    Pwmd is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Pwmd.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef XML_H
#define XML_H

#include <libxml/tree.h>
#include <libxml/parser.h>
#include <libxml/xpath.h>

#include "util-misc.h"
#include "common.h"
#include "util-string.h"

struct element_list_s
{
  struct slist_s *list;		// The list of elements to be returned
  char *target;			// Placeholder for the current elements target
  struct string_s *prefix;	// Previous elements in the path.
  int recurse;			// Recurse into children of the current element
  int root_only;		// List only root elements.
  unsigned depth;		// Current recursion level
  unsigned flags;		// To determine which list flags too append
};

typedef enum {
    XML_CMD_NONE,		// No special processing in xml_find_elements()
    XML_CMD_REALPATH,
    XML_CMD_STORE,
    XML_CMD_LIST,
    XML_CMD_DELETE,
    XML_CMD_RENAME,
    XML_CMD_MOVE,
    XML_CMD_ATTR,
    XML_CMD_ATTR_TARGET,
    XML_CMD_ATTR_LIST,
    XML_CMD_ATTR_EXPIRE
} xml_command_t;

#define XML_REQUEST_FLAG_CREATED	0x0001

struct xml_request_s {
    xmlDocPtr doc;		// The document this request belongs to
    xml_command_t cmd;		// For special processing in xml_find_elements()
    char **args;		// The arguments passed to a command
    unsigned depth;		// Current depth of recursion
    unsigned flags;		// Possible request flags set when resolving
    void *data;			// Pointer for command data (LIST)
};

gpg_error_t xml_init ();
void xml_deinit ();
gpg_error_t xml_valid_username (const char *str);
gpg_error_t xml_new_request (struct client_s *client, const char *line,
                             xml_command_t cmd, struct xml_request_s **result);
void xml_free_request (struct xml_request_s *);
gpg_error_t xml_new_root_element (struct client_s *, xmlDocPtr doc, char *name);
xmlDocPtr xml_new_document (void);
int xml_valid_element (xmlChar * element);
xmlNodePtr xml_find_elements (struct client_s *client,
                              struct xml_request_s *req, xmlNodePtr node,
                              gpg_error_t *rc);
int xml_valid_element_path (char **path, int content);
xmlNodePtr xml_find_text_node (xmlNodePtr node);
gpg_error_t xml_create_path_list (struct client_s *, xmlDocPtr doc, xmlNodePtr,
                                  struct element_list_s *elements, char *path,
                                  gpg_error_t);
void xml_free_element_list (struct element_list_s *);
gpg_error_t xml_recurse_xpath_nodeset (struct client_s *, xmlDocPtr doc,
                                       xmlNodeSetPtr nodes, xmlChar * value,
                                       xmlBufferPtr * result, int,
                                       const xmlChar *);
gpg_error_t xml_add_attribute (struct client_s *, xmlNodePtr node,
                               const char *name, const char *value);
xmlChar *xml_attribute_value (xmlNodePtr n, xmlChar * attr);
gpg_error_t xml_delete_attribute (struct client_s *, xmlNodePtr n,
                                  const xmlChar * name);
gpg_error_t xml_validate_import (struct client_s *, xmlNodePtr);
xmlNodePtr xml_find_element (struct client_s *, xmlNodePtr node,
                             const char *element, gpg_error_t *);
gpg_error_t xml_update_element_mtime (struct client_s *, xmlNodePtr n);
gpg_error_t xml_parse_doc (const char *xml, size_t len, xmlDocPtr *doc);
xmlNodePtr xml_resolve_path (struct client_s *client, xmlDocPtr doc,
                             xmlChar *path, char ***result, gpg_error_t *rc);
gpg_error_t xml_validate_target (struct client_s *, xmlDocPtr doc,
                                 const char *src, xmlNodePtr dst);
gpg_error_t xml_check_recursion (struct client_s *client,
                                 struct xml_request_s *req);
int xml_valid_attribute (const char *str);
int xml_valid_attribute_value (const char *str);
gpg_error_t xml_is_element_owner (struct client_s *, xmlNodePtr, int strict);
gpg_error_t xml_unlink_node (struct client_s *, xmlNodePtr);
xmlNodePtr xml_create_element_path (struct client_s *client,
                                    struct xml_request_s *req, xmlNodePtr node,
                                    char **path, gpg_error_t *rc);
int xml_reserved_attribute (const char *);
gpg_error_t xml_acl_check (struct client_s *, xmlNodePtr);
gpg_error_t xml_remove_user_attributes (struct client_s *, xmlNodePtr);
gpg_error_t xml_protected_attr (const char *a);

#endif
