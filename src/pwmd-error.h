/*
    Copyright (C) 2006-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of pwmd.

    Pwmd is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.

    Pwmd is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Pwmd.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef PWMD_ERROR_H
#define PWMD_ERROR_H

#ifdef GPG_ERR_SOURCE_DEFAULT
#error "GPG_ERR_SOURCE_DEFAULT already defined."
#else
#define GPG_ERR_SOURCE_DEFAULT	GPG_ERR_SOURCE_USER_1
#endif

#include <gpg-error.h>
#include <pthread.h>

extern pthread_key_t last_error_key;
const char *pwmd_strerror (gpg_error_t rc);

#endif
