/*
    Copyright (C) 2012-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of pwmd.

    Pwmd is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.

    Pwmd is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Pwmd.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef UTIL_SLIST_H
#define UTIL_SLIST_H

struct slist_s
{
  void *data;
  struct slist_s *next;
};

unsigned slist_length (struct slist_s *list);
void *slist_nth_data (struct slist_s *list, unsigned n);
struct slist_s *slist_append (struct slist_s *list, void *data);
void slist_free (struct slist_s *list);
struct slist_s *slist_remove (struct slist_s *list, void *data);

#endif
