/*
    Copyright (C) 2006-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of pwmd.

    Pwmd is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.

    Pwmd is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Pwmd.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#include <sys/types.h>
#include <sys/stat.h>
#ifdef HAVE_FCNTL_H
#include <fcntl.h>
#endif
#include <ctype.h>
#include <pwd.h>

#include "pwmd-error.h"
#include <gcrypt.h>
#include "util-misc.h"
#include "util-string.h"
#include "mutex.h"
#include "cache.h"
#include "mem.h"

char *home_directory;
void log_write (const char *fmt, ...);

int
valid_filename (const char *filename)
{
  const char *p;

  if (!filename || !*filename)
    return 0;

  if (filename[0] == '-' && filename[1] == 0)
    return 0;

  for (p = filename; *p; p++)
    {
      if (*p == '/' || isspace (*p))
	return 0;
    }

  return 1;
}

/* If 'user' is null then lookup entry for 'uid'. Caller must free 'buf'. */
struct passwd *
get_pwd_struct (const char *user, uid_t uid, struct passwd *pw, char **buf,
                gpg_error_t *rc)
{
  struct passwd *result = NULL;
#ifdef HAVE_GETPWNAM_R
  size_t len;
  int err = 0;

  len = sysconf (_SC_GETPW_R_SIZE_MAX);
  if (len == -1)
    len = 16384;

  *buf = xmalloc (len);
  if (!*buf)
    return NULL;

  if (user)
    err = getpwnam_r (user, pw, *buf, len, &result);
  else
    err = getpwuid_r (uid, pw, *buf, len, &result);

  if (!err && result)
    return result;

  xfree (*buf);
  *buf = NULL;
  errno = err;
  *rc = gpg_error_from_errno (err);
  return NULL;
#else
  if (buf)
    *buf = NULL;

  errno = 0;
  if (user)
    result = getpwnam (user);
  else
    result = getpwuid (uid);

  if (!result)
    *rc = gpg_error_from_syserror ();

  return result;
#endif
}

static char *
get_pwd_entry(uid_t uid, int which)
{
  gpg_error_t rc;
  char *buf;
  struct passwd pw;
  struct passwd *result = get_pwd_struct (NULL, uid, &pw, &buf, &rc);
  char *value = NULL;

  if (!result)
    return NULL;

  if (result)
    {
      if (!which)
        value = str_dup (result->pw_dir);
      else
        value = str_dup (result->pw_name);
    }

  xfree (buf);
  return value;
}

char *
get_username (uid_t uid)
{
  return get_pwd_entry (uid, 1);
}

char *
get_home_dir ()
{
  if (home_directory)
    return home_directory;

  home_directory = get_pwd_entry (getuid(), 0);
  return home_directory;
}

char *
expand_homedir (char *str)
{
  char *p = str;

  if (*p++ == '~')
    {
      char *dir = get_pwd_entry(getuid(), 0);
      char *s;

      if (!dir)
        return NULL;

      s = str_asprintf ("%s%s", dir, p);
      xfree (dir);
      return s;
    }

  return str_dup (str);
}

/* The 'more' parameter lets the calling command process remaining
 * non-option arguments. */
gpg_error_t
parse_options (char **line, struct argv_s * args[], void *data, int more)
{
  char *p = *line;
  gpg_error_t rc = 0;

  for (; p && *p; p++)
    {
      while (*p == ' ')
	p++;

      if (!*p)
	break;

      if (*p == '-' && *(p + 1) == '-')
	{
	  p += 2;
	  char opt[255] = { 0 }, value[255] =
	  {
	  0};
	  char *tp;
	  unsigned len;

	  if (!*p || *p == ' ')
	    {
	      if (*p)
		p++;

	      break;
	    }

	  for (tp = opt, len = 0; *p; p++, len++)
	    {
	      if (len + 1 == 255)
		return GPG_ERR_LINE_TOO_LONG;

	      if (*p == ' ' || *p == '=')
		{
		  *tp = 0;

		  if (*p == '=')
		    {
                      int inquote = 0;

		      p++;

		      for (tp = value, len = 0; *p; p++, len++)
			{
			  if (len + 1 == 255)
			    return GPG_ERR_LINE_TOO_LONG;

			  if (*p == '\"')
			    {
			      inquote = !inquote;
			      continue;
			    }
			  else if (*p == ' ' && !inquote)
			    break;

			  *tp++ = *p;
			}

		      *tp = 0;
		    }

		  break;
		}

	      *tp++ = *p;
	    }

	  *tp = 0;
	  int match = 0;

	  for (int i = 0; args[i]; i++)
	    {
	      if (!strcmp (args[i]->opt, opt))
		{
		  log_write2 ("param: name='%s' value='%s'", opt, value);
		  if (args[i]->type == OPTION_TYPE_NOARG && *value)
		    return GPG_ERR_SYNTAX;
		  else if (args[i]->type == OPTION_TYPE_ARG && !*value)
		    return GPG_ERR_SYNTAX;

                  if (args[i]->func)
                    {
                      rc = args[i]->func (data, value);
                      if (rc)
                        return rc;
                    }

		  match = 1;
		  break;
		}
	    }

	  if (!match)
	    return GPG_ERR_UNKNOWN_OPTION;

	  if (!*p)
	    break;

	  continue;
	}

      if (*(p+1) && !more)
        return GPG_ERR_SYNTAX;

      break;
    }

  *line = p;
  return rc;
}

char *
bin2hex (const unsigned char *data, size_t len)
{
  size_t size = len * 2 + 1;
  char buf[size];
  size_t n;

  buf[0] = 0;

  for (n = 0; n < len; n++)
    {
      char c[3];

      sprintf (c, "%02X", data[n]);
      strcat (buf, c);
    }

  return str_dup (buf);
}

static char *
html_escape (const char *line, size_t len, int space)
{
  struct string_s *string = string_new ("");
  size_t n;
  char *tmp;

  if (!string)
    return NULL;

  for (n = 0; n < len; n++)
    {
      switch (line[n])
        {
        case '<':
          string = string_append (string, "&lt;");
          break;
        case '>':
          string = string_append (string, "&gt;");
          break;
        case ' ':
          if (space)
            {
              string = string_append (string, "%20");
              break;
            }
        default:
          string = string_append_printf (string, "%c", line[n]);
          break;
        }
    }

  tmp = string->str;
  string_free (string, 0);
  return tmp;
}

static char *
strip_texi_html (const char *line)
{
  size_t len;
  const char *p;
  char *tmp;
  struct string_s *string = string_new ("<html><body>");

  if (!string)
    return NULL;

  /* The command usage text. */
  p = strchr (line, '\n');
  if (p)
    {
      len = strlen (line)-strlen(p);
      tmp = html_escape (line, len, 0);
      string = string_append_printf (string, "<b>%s</b><p>", tmp);
      xfree (tmp);
      p++;
    }
  else
    p = line;

  for (; p && *p; p++)
    {
      int non_anchor = 1;
      int var = 0;
      int code = 0;
      int emph = 0;
      int cite = 0;

      if (*p == '@' && *(p + 1) != '@')
	{
          p++;

          if (!strncasecmp (p, "end ", 4))
            {
              p += 4;
              if (!strncasecmp (p, "table", 5))
                {
                  string = string_append (string, "</p>");
                }
              else if (!strncasecmp (p, "example", 7))
                {
                  string = string_append (string, "</code>");
                }

              while (*p++ != '\n');
              p--;
              continue;
            }
          else if (!strncasecmp (p, "table ", 6))
            {
	      while (*p++ != '\n');
	      p--;
	      continue;
            }
	  else if (!strncasecmp (p, "example", 7))
	    {
              string = string_append (string, "<code>");
	      while (*p++ != '\n');
	      p--;
	      continue;
	    }
	  else if (!strncasecmp (p, "sp ", 3))
	    {
	      while (*p++ != '\n');
	      p--;
              string = string_append (string, "<p>");
	      continue;
	    }
	  else if (!strncasecmp (p, "item ", 5))
	    {
              p += 5;
              string = string_append (string, "<p><b>");
              tmp = strchr (p, '\n');
              len = strlen (p) - strlen (tmp ? tmp : "");
	      while (len--)
                {
                  if (*p == '<')
                    {
                      string = string_append (string, "&lt;");
                      p++;
                    }
                  else if (*p == '>')
                    {
                      string = string_append (string, "&gt;");
                      p++;
                    }
                  else
                    string = string_append_printf (string, "%c", *p++);
                }

              string = string_append (string, "</b><br/>");
              continue;
	    }
	  else if (!strncasecmp (p, "pxref", 5)
                   || !strncasecmp (p, "npxref", 6))
	    {
              int n = 0;

              if (!strncasecmp (p, "npxref", 6))
                n++;

	      p += n ? 7 : 6;
              non_anchor = 0;
              string = string_append_printf (string, "%s<a href=\"",
                                             n ? "" : "see ");
	      goto close;
	    }
	  else if (!strncasecmp (p, "xref", 4))
	    {
	      p += 5;
              non_anchor = 0;
              string = string_append (string, "See <a href=\"");
	      goto close;
	    }
	  else if (!strncasecmp (p, "url", 3))
	    {
	      p += 4;
              non_anchor = 0;
              string = string_append (string, "<a href=\"");
	      goto close;
	    }
	  else if (!strncasecmp (p, "key", 3))
            {
              p += 4;
              goto close;
            }
	  else if (!strncasecmp (p, "file", 4))
            {
              p += 5;
              var = 1;
              string = string_append (string, "<var>");
              goto close;
            }
	  else if (!strncasecmp (p, "var", 3))
            {
              p += 4;
              var = 1;
              string = string_append (string, "<var>");
              goto close;
            }
	  else if (!strncasecmp (p, "option", 6))
            {
              p += 7;
              var = 1;
              string = string_append (string, "<var>");
              goto close;
            }
	  else if (!strncasecmp (p, "code", 4))
            {
              p += 5;
              code = 1;
              string = string_append (string, "<b>");
              goto close;
            }
	  else if (!strncasecmp (p, "emph", 4))
            {
              p += 5;
              emph = 1;
              string = string_append (string, "<em>");
              goto close;
            }
	  else if (!strncasecmp (p, "command", 7))
            {
              p += 8;
              emph = 1;
              string = string_append (string, "<em>");
              goto close;
            }
	  else if (!strncasecmp (p, "cite", 4))
            {
              p += 5;
              cite = 1;
              string = string_append (string, "<cite>");
              goto close;
            }
	  else if (!strncasecmp (p, "abbr", 4))
            {
              p += 5;
              emph = 1;
              string = string_append (string, "<em>");
              goto close;
            }
	  else if (!strncasecmp (p, "*", 1))
            {
              string = string_append (string, "<br/>");
              continue;
            }

	  while (*p && *p != '{')
	    {
	      if (*++p == '*')
		{
		  p++;
		  goto append;
		}
	    }

	close:
	  if (*p)
	    {
              char buf [255];

              tmp = strchr (p, '}');
              len = strlen (p) - strlen (tmp ? tmp : "");
              tmp = buf;
	      while (len--)
                *tmp++ = *p++;

              *tmp = 0;

	      if (*p)
		p++;

              if (!non_anchor)
                {
                  tmp = html_escape (buf, strlen (buf), 1);
                  string = string_append_printf (string, "%s\">%s</a>", tmp, buf);
                  xfree (tmp);
                }
              else
                string = string_append (string, buf);

              if (var)
                string = string_append (string, "</var>");
              else if (code)
                string = string_append (string, "</b>");
              else if (emph)
                string = string_append (string, "</em>");
              else if (cite)
                string = string_append (string, "</cite>");
	    }
	}
      else if (*p == '@' && *(p + 1) == '@')
	p++;

    append:
      string = string_append_printf (string, "%c", *p);

      if (!*p)
        break;
    }

  string = string_append (string, "</body></html>");
  tmp = string->str;
  string_free (string, 0);
  return tmp;
}

static char *
strip_texi (const char *str, int html)
{
  const char *p;
  char *s;
  int c = 0;

  if (html)
    return strip_texi_html (str);

  s = str_dup (str);
  if (!s)
    return NULL;

  for (p = str; *p; p++)
    {
      if (*p == '@' && *(p + 1) != '@')
	{
          p++;

	  if (!strncasecmp (p, "table", 5)
	      || !strncasecmp (p, "end ", 4))
	    {
	      while (*p++ != '\n');
	      p--;
	      continue;
	    }
	  else if (!strncasecmp (p, "example", 7)
		   || !strncasecmp (p, "end ", 4))
	    {
	      while (*p++ != '\n');
	      p--;
	      continue;
	    }
	  else if (!strncasecmp (p, "sp ", 3))
	    {
	      while (*p++ != '\n');
	      p--;
	      continue;
	    }
	  else if (!strncasecmp (p, "item", 4))
	    {
	      p += 5;
	      while (*p && *p != '\n')
		s[c++] = *p++;

              s[c++] = '\n';
	      continue;
	    }
	  else if (!strncasecmp (p, "pxref", 5))
	    {
	      p += 5;
	      strcat (s, "see ");
	      c = strlen (s);
	      goto close;
	    }
	  else if (!strncasecmp (p, "xref", 4))
	    {
	      p += 4;
	      strcat (s, "See ");
	      c = strlen (s);
	      goto close;
	    }
	  else if (!strncasecmp (p, "*", 1))
            {
              s[c++] = '\n';
              s[c] = 0;
              continue;
            }

	  while (*p && *p != '{')
	    {
	      if (*++p == '*')
		{
		  p++;
		  goto append;
		}
	    }

	close:
	  if (*p)
	    {
	      p++;
	      s[c++] = '`';
	      while (*p && *p != '}')
		s[c++] = *p++;

	      if (*p)
		p++;

	      s[c++] = '\'';
              s[c] = 0;
	    }
	}
      else if (*p == '@' && *(p + 1) == '@')
	p++;

    append:
      if (*p)
	s[c++] = *p;

      s[c] = 0;
    }

  s[c] = 0;
  return s;
}


char *
strip_texi_and_wrap (const char *str, int html)
{
  char *tmp = strip_texi (str, html);
  char *help = xcalloc (1, strlen (tmp ? tmp : "") * 2 + 1);
  char *p, *ph;
  int i;

  for (p = tmp, ph = help, i = 0; p && *p; p++, i++)
    {
      if (i == 78 || *p == '\n')
	{
	  if (!isspace (*p))
	    {
	      char *t = ph;
	      int n = 0;

	      while (*(--t) != ' ')
		n++;

	      *t++ = '\n';
	      i = -1;
	      p -= n;
	      while (n--)
		{
		  *t++ = *p++;
		  i++;
		}
	    }
	  else
	    {
	      *ph++ = '\n';
	      i = -1;
	      while (*p != '\n' && *p == ' ')
		p++;
	    }
	}

      if (*p == '\n')
        continue;
      *ph++ = *p;
    }

  *ph = 0;
  xfree (tmp);
  return help;
}

void
free_key (void *data)
{
  xfree (data);
}

gpg_error_t
create_thread (void *(*cb) (void *), void *data,
	       pthread_t * tid, int detached)
{
  pthread_attr_t attr;
  int n;

  pthread_attr_init (&attr);

  if (detached)
    pthread_attr_setdetachstate (&attr, PTHREAD_CREATE_DETACHED);

  n = pthread_create (tid, &attr, cb, data);
  pthread_attr_destroy (&attr);
  return gpg_error_from_errno (n);
}

void
release_mutex_cb (void *arg)
{
  pthread_mutex_t *m = (pthread_mutex_t *) arg;

  MUTEX_UNLOCK (m);
}

void
close_fd_cb (void *arg)
{
  int fd = *(int *)arg;

  if (fd != -1)
    close (fd);
}

gpg_error_t
get_checksum_memory (void *data, size_t size, unsigned char **r_crc,
                     size_t *r_crclen)
{
  size_t len = gcry_md_get_algo_dlen (GCRY_MD_CRC32);
  unsigned char *crc = xmalloc (len);

  *r_crc = NULL;
  *r_crclen = 0;

  if (!crc)
    return GPG_ERR_ENOMEM;

  gcry_md_hash_buffer (GCRY_MD_CRC32, crc, data, size);
  *r_crc = crc;
  *r_crclen = len;
  return 0;
}

/* The advisory lock should be obtained before calling this function. */
gpg_error_t
get_checksum (const char *filename, unsigned char **r_crc, size_t * r_crclen)
{
  int fd = -1;
  unsigned char *buf;
  gpg_error_t rc;
  struct stat st;

  rc = open_check_file (filename, &fd, &st, 1);
  if (rc)
    return rc;

  pthread_cleanup_push (close_fd_cb, &fd);
  buf = xmalloc (st.st_size);
  if (buf)
    {
      pthread_cleanup_push (xfree, buf);

      size_t len = read (fd, buf, st.st_size);
      if (len == st.st_size)
	{
	  if (buf)
            rc = get_checksum_memory (buf, st.st_size, r_crc, r_crclen);
          else
            rc = GPG_ERR_ENOMEM;
	}
      else
	rc = GPG_ERR_TOO_SHORT;

      pthread_cleanup_pop (1);
    }
  else
    rc = GPG_ERR_ENOMEM;

  pthread_cleanup_pop (1);
  return rc;
}

wchar_t *str_to_wchar (const char *str)
{
    wchar_t *wc;
    mbstate_t ps;
    const char *p = str;
    size_t len;

    memset (&ps, 0, sizeof(mbstate_t));
    len = mbsrtowcs (NULL, &p, 0, &ps);
    if (len == -1)
      return NULL;

    wc = xcalloc (len+1, sizeof(wchar_t));
    if (!wc)
      return NULL;

    p = str;
    memset (&ps, 0, sizeof(mbstate_t));
    len = mbsrtowcs (wc, &p, len, &ps);
    if (len == -1)
      {
        xfree (wc);
        return NULL;
      }

    return wc;
}

char *
gnupg_escape (const char *str)
{
  if (!str || !*str)
    return NULL;

  char *buf = xmalloc (strlen(str)*4+1), *b = buf;
  const char *p;

  if (!buf)
    return NULL;

  for (p = str; p && *p; p++)
    {
      if (*p == ':')
        {
          *b++ = '\\';
          *b++ = 'x';
          *b++ = '3';
          *b++ = 'a';
          continue;
        }
      else if (*p == '\\')
        {
          *b++ = '\\';
          *b++ = 'x';
          *b++ = '5';
          *b++ = 'c';
          continue;
        }

      *b++ = *p;
    }

  *b = 0;
  return buf;
}

/* From Beej's Guide to Network Programming. It's a good tutorial. */
void *
get_in_addr (struct sockaddr *sa)
{
  if (sa->sa_family == AF_INET)
    return &(((struct sockaddr_in *) sa)->sin_addr);

  return &(((struct sockaddr_in6 *) sa)->sin6_addr);
}

gpg_error_t
open_check_file (const char *filename, int *r_fd, struct stat *r_st, int reg)
{
  int fd;
  gpg_error_t rc = 0;

  if (r_st)
    memset (r_st, 9, sizeof (struct stat));

  if (reg)
    {
      struct stat st;

      if (lstat (filename, &st) == -1)
        return gpg_err_code (gpg_error_from_syserror ());

      if (!S_ISREG (st.st_mode))
        return GPG_ERR_ENODEV;

      if (r_st)
        memcpy (r_st, &st, sizeof (struct stat));
    }

  if (r_fd)
    {
      fd = open (filename, O_RDONLY);
      if (fd == -1)
        return gpg_err_code (gpg_error_from_syserror ());

      if (r_st && fstat (fd, r_st) == -1)
        {
          rc = gpg_err_code (gpg_error_from_syserror ());
          close (fd);
          return rc;
        }

      *r_fd = fd;
    }

  return 0;
}
