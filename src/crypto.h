/*
    Copyright (C) 2006-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of pwmd.

    Pwmd is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.

    Pwmd is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Pwmd.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef CRYPTO_H
#define CRYPTO_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <assuan.h>
#include <gpg-error.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <gpgme.h>
#include <time.h>

#ifdef HAVE_STDINT_H
#include <stdint.h>
#elif defined (HAVE_INTTYPES_H)
#include <inttypes.h>
#endif

#include "common.h"

#define DEFAULT_EXPIRE		(unsigned long)(60*60*24*365*3) // 3 years

#define CRYPTO_FLAG_NEWFILE	0x0001
#define CRYPTO_FLAG_KEYFILE	0x0002 // --passphrase-file with --import
#define CRYPTO_FLAG_SYMMETRIC   0x0004
#define CRYPTO_FLAG_PASSWD	0x0008
#define CRYPTO_FLAG_PASSWD_NEW	0x0010
#define CRYPTO_FLAG_PASSWD_SIGN 0x0020

struct save_s
{
  char **pubkey;	 	/* SAVE --keyid */
  char *sigkey; 		/* SAVE --sign-keyid */
  char *userid;			/* SAVE genkey parameters */
  gpgme_key_t *mainkey;		/* GENKEY --subkey-of */
  char *algo;
  unsigned long expire;
  unsigned flags;
};

struct crypto_s
{
  assuan_context_t client_ctx;
  gpgme_ctx_t ctx;
  unsigned char *plaintext;
  size_t plaintext_size;
  gpgme_data_t cipher;
  char **pubkey;
  char *sigkey;
  char *filename;		/* the currently opened data file */
  struct save_s save;
  gpg_error_t progress_rc;
  time_t status_timeout;
  unsigned flags;
  char *keyfile;
};

gpgme_error_t crypto_init (struct crypto_s **, void *, const char *, int,
                           char *passphrase_file);
gpgme_error_t crypto_init_ctx (struct crypto_s *, int, char *passphrase_file);
gpgme_error_t crypto_genkey (struct client_s *, struct crypto_s *);
gpgme_error_t crypto_encrypt (struct client_s *, struct crypto_s *);
gpgme_error_t crypto_decrypt (struct client_s *, struct crypto_s *);
gpgme_error_t crypto_passwd (struct client_s *, struct crypto_s *);
void crypto_free (struct crypto_s *);
void crypto_free_save (struct save_s *);
void crypto_free_non_keys (struct crypto_s *);
gpgme_error_t crypto_data_to_buf (const gpgme_data_t, unsigned char **,
                                  size_t *);
gpg_error_t crypto_write_file (struct crypto_s *, unsigned char **crc,
                               size_t *len);
gpgme_error_t crypto_list_keys (struct crypto_s *, char **, int secret,
                                gpgme_key_t **);
char *crypto_key_info (const gpgme_key_t);
void crypto_free_key_list (gpgme_key_t *);
gpg_error_t crypto_try_decrypt (struct client_s *, int);
void crypto_set_keepalive ();
gpg_error_t crypto_is_symmetric (const char *filename);
gpg_error_t crypto_keyid_to_16b (char **keys);
gpg_error_t crypto_keyid_to_16b_once (char *key);
gpg_error_t crypto_delete_key (struct client_s *client, struct crypto_s *,
                               const gpgme_key_t, int secret);

#endif
