/*
    Copyright (C) 2023 Ben Kibbey <bjk@luxsci.net>

    This file is part of pwmd.

    Pwmd is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.

    Pwmd is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Pwmd.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/* These are command option flags. */
#define OPT_INQUIRE		0x0001
#define OPT_NO_PASSPHRASE	0x0002
#define OPT_ASK 		0x0004
#define OPT_LIST_RECURSE	0x0008
#define OPT_VERBOSE		0x0010
#define OPT_LOCK		0x0020
#define OPT_LOCK_ON_OPEN	0x0040
#define OPT_SIGN		0x0080
#define OPT_LIST_ALL		0x0100
#define OPT_DATA		0x0200
#define OPT_NO_AGENT            0x0400
#define OPT_SECRET	        0x0800
#define OPT_INQUIRE_KEYID       0x1000
#define OPT_SYMMETRIC		0x4000
#define OPT_NO_SIGNER		0x8000
#define OPT_HTML		0x10000
#define OPT_CACHE_AGENT		0x20000
#define OPT_CACHE_SIGN		0x40000
#define OPT_KEYINFO_LEARN	0x80000
#define OPT_SEXP		0x100000
#define OPT_NO_INHERIT_ACL	0x200000
