/*
    Copyright (C) 2006-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of pwmd.

    Pwmd is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.

    Pwmd is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Pwmd.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef STATUS_H
#define STATUS_H

#include <gpg-error.h>
#include <assuan.h>

typedef enum
{
  STATUS_CACHE,
  STATUS_CLIENTS,
  STATUS_LOCKED,
  STATUS_ENCRYPT,
  STATUS_DECRYPT,
  STATUS_XFER,
  STATUS_GENKEY,
  STATUS_NEWFILE,
  STATUS_GPGME,
  STATUS_KEEPALIVE,
  STATUS_STATE,
  STATUS_EXPIRE,
#ifdef WITH_GNUTLS
  STATUS_REHANDSHAKE,
#endif
  STATUS_BULK,
  STATUS_MODIFIED,
} status_msg_t;

struct status_msg_s
{
  status_msg_t s;
  char *line;
  struct status_msg_s *next;
};

gpg_error_t send_status (assuan_context_t ctx, status_msg_t which,
			 const char *fmt, ...);
void send_status_all (status_msg_t s, const char *fmt, ...);
void send_status_all_not_self (status_msg_t s, const char *fmt, ...);
void send_status_modified (assuan_context_t);

#endif
