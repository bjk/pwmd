/*
    Copyright (C) 2006-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of pwmd.

    Pwmd is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.

    Pwmd is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Pwmd.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef TLS_H
#define TLS_H

#include <gnutls/gnutls.h>
#include <errno.h>

struct tls_s
{
  gnutls_session_t ses;
  char *fp;
  int nl;
  int rehandshake;
};

extern int tls_fd;
extern int tls6_fd;

struct client_s;
int tls_start_stop (int term);
struct tls_s *tls_init_client (int fd, int timeout, const char *prio);
void tls_log (int level, const char *msg);
void tls_audit_log (gnutls_session_t, const char *);
ssize_t tls_read_hook (assuan_context_t ctx, assuan_fd_t fd, void *data,
		       size_t len);
ssize_t tls_write_hook (assuan_context_t ctx, assuan_fd_t fd,
			const void *data, size_t len);
void tls_deinit_params ();
gpg_error_t tls_validate_access (struct client_s *client, const char *filename);
void tls_rehandshake ();

#endif
