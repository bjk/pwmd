/*
    Copyright (C) 2016-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of pwmd.

    Pwmd is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.

    Pwmd is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Pwmd.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>
#include <errno.h>
#include <pwd.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/wait.h>
#ifdef HAVE_FCNTL_H
#include <fcntl.h>
#endif
#include <dirent.h>

#ifdef HAVE_LIMITS_H
#include <limits.h>
#endif

#include "pwmd-error.h"
#include <gpgme.h>
#include "util-misc.h"
#include "mem.h"
#include "common.h"
#include "agent.h"
#include "util-string.h"
#include "mutex.h"
#include "rcfile.h"
#include "cache.h"

static gpg_error_t
mem_realloc_cb (void *data, const void *buffer, size_t len)
{
  membuf_t *mem = (membuf_t *) data;
  void *p;

  if (!buffer)
    return 0;

  if ((p = xrealloc (mem->buf, mem->len + len)) == NULL)
    return 1;

  mem->buf = p;
  memcpy ((char *) mem->buf + mem->len, buffer, len);
  mem->len += len;
  return 0;
}

static gpg_error_t
assuan_command (struct agent_s *a, char **result,
		size_t * len, const char *cmd)
{
  gpg_error_t rc;

  a->data.len = 0;
  a->data.buf = NULL;
  if (result)
    *result = NULL;
  if (len)
    *len = 0;

  rc = assuan_transact (a->ctx, cmd, mem_realloc_cb, &a->data, NULL, NULL,
                        NULL, NULL);
  if (rc)
    xfree (a->data.buf);
  else
    {
      if (a->data.buf)
	{
	  mem_realloc_cb (&a->data, "", 1);
	  if (result)
	    *result = (char *) a->data.buf;
	  else
	    xfree (a->data.buf);

	  if (len)
	    *len = a->data.len;
	}
    }

  return rc;
}

static char *
get_gpg_connect_agent_path (void)
{
  gpgme_engine_info_t engine;
  char *s, *p;

  gpgme_get_engine_info (&engine);
  while (engine)
    {
      if (engine->protocol == GPGME_PROTOCOL_GPGCONF)
        break;

      engine = engine->next;
    }

  if (!engine)
    return NULL;

  s = str_dup (engine->file_name);
  if (!s)
    return NULL;

  p = strrchr (s, '/');
  if (p)
    {
      *p = 0;
      p = str_asprintf ("%s/gpg-connect-agent", s);
      xfree (s);
      return p;
    }

  xfree (s);
  return NULL;
}

gpg_error_t
agent_connect (struct agent_s *agent)
{
  gpg_error_t rc;
  assuan_context_t ctx = NULL;
  static struct assuan_malloc_hooks mhooks = { xmalloc, xrealloc, xfree };

  agent->did_restart = 0;
  if (agent->restart)
    {
      gpgme_ctx_t gctx;

      rc = gpgme_new (&gctx);
      if (!rc)
        {
          char **args = NULL;

          pthread_cleanup_push ((void *)gpgme_release, gctx);
          rc = gpgme_set_protocol (gctx, GPGME_PROTOCOL_SPAWN);
          if (!rc && strv_printf (&args, ""))
            {
              if (!rc && strv_printf (&args, "--homedir"))
                {
                  char *gpghome = config_get_string ("global", "gpg_homedir");

                  if (gpghome)
                    {
                      if (!strv_printf (&args, "%s", gpghome))
                        rc = GPG_ERR_ENOMEM;
                    }
                  else
                    {
                      if (!strv_printf (&args, "%s/.gnupg", homedir))
                        rc = GPG_ERR_ENOMEM;
                    }

                  xfree (gpghome);
                }
              else if (!rc)
                rc = GPG_ERR_ENOMEM;
            }
          else if (!rc)
            rc = GPG_ERR_ENOMEM;

          pthread_cleanup_push ((void *)strv_free, args);
          if (!rc)
            {
              gpgme_data_t idata = NULL;
              size_t len;
              char *s;

              rc = gpgme_data_new (&idata);
              if (!rc)
                {
                  char *gca = get_gpg_connect_agent_path ();

                  pthread_cleanup_push (xfree, gca);
                  if (gca)
                    rc = gpgme_op_spawn (gctx, gca, (const char **)args, NULL,
                                         idata, NULL,
                                         GPGME_SPAWN_DETACHED|GPGME_SPAWN_ALLOW_SET_FG);
                  else
                    rc = GPG_ERR_ENOMEM;

                  pthread_cleanup_pop (1);
                }

              if (!rc)
                {
                  ssize_t ret = gpgme_data_write (idata, "/BYE\n", 5);

                  if (ret != 5)
                    rc = gpg_error_from_syserror ();
                }

              if (idata)
                {
                  s = gpgme_data_release_and_get_mem (idata, &len);
                  gpgme_free (s);
                }
            }

          pthread_cleanup_pop (1);
          pthread_cleanup_pop (1);
        }

      if (rc)
        return rc;
    }

  rc = assuan_new_ext (&ctx, GPG_ERR_SOURCE_DEFAULT, &mhooks, assuan_log_cb,
                       NULL);
  if (rc)
    return rc;

  assuan_set_flag (ctx, ASSUAN_CONFIDENTIAL, 1);
  pthread_cleanup_push ((void *)assuan_release, ctx);
  rc = assuan_socket_connect (ctx, agent->socket, ASSUAN_INVALID_PID, 0);
  if (!rc)
    {
      if (agent->ctx)
        assuan_release (agent->ctx);

      agent->ctx = ctx;
    }
  else
    {
      if (ctx)
	assuan_release (ctx);
    }

  pthread_cleanup_pop (0);
  return rc;
}

static gpg_error_t
send_to_agent (struct agent_s *agent, char **result, size_t *len,
               const char *cmd)
{
  gpg_error_t rc = 0;

  if (agent->ctx)
    rc = assuan_command (agent, result, len, cmd);
  else
    {
      rc = agent_connect (agent);
      if (!rc)
        rc = assuan_command (agent, result, len, cmd);
    }

  if (!agent->restart && gpg_err_source (rc) == GPG_ERR_SOURCE_DEFAULT
      && (gpg_err_code (rc) == GPG_ERR_ASS_CONNECT_FAILED
          || gpg_err_code (rc) == GPG_ERR_EPIPE))
    {
      log_write (_ ("gpg-agent connection died (rc=%u), reconnecting"), rc);
      agent->restart = 1;
      rc = agent_connect (agent);
      if (!rc)
        {
          agent->did_restart = 1;
          rc = assuan_command (agent, result, len, cmd);
        }
    }

  agent->restart = 0;
  return rc;
}

gpg_error_t
agent_command (struct agent_s *agent, char **result, size_t * len,
               const char *fmt, ...)
{
  va_list ap;
  char *cmd = NULL;
  gpg_error_t rc;

  va_start (ap, fmt);

  if (str_vasprintf (&cmd, fmt, ap) > 0)
    {
      pthread_cleanup_push (xfree, cmd);
      rc = send_to_agent (agent, result, len, cmd);
      pthread_cleanup_pop (0);
    }
  else
    rc = GPG_ERR_ENOMEM;

  xfree (cmd);
  va_end (ap);
  return rc;
}

void
agent_disconnect (struct agent_s *agent)
{
  if (!agent)
    return;

  if (agent->ctx)
    assuan_release (agent->ctx);

  agent->ctx = NULL;
}

void
agent_free (struct agent_s *agent)
{
  if (!agent)
    return;

  agent_disconnect (agent);
  xfree (agent->socket);
  xfree (agent);
}

gpg_error_t
agent_init (struct agent_s **agent)
{
  struct agent_s *new;
  FILE *fp;
  char *buf;
  char line[PATH_MAX];
  gpg_error_t rc = 0;
  int ret;
  char *gpghome = config_get_string ("global", "gpg_homedir");

  if (gpghome)
    buf = str_asprintf ("gpgconf --homedir %s --list-dirs", gpghome);
  else
    buf = str_asprintf ("gpgconf --homedir %s/.gnupg --list-dirs", homedir);

  xfree (gpghome);
  fp = popen (buf, "r");
  if (!fp)
    {
      rc = gpg_error_from_syserror ();
      xfree (buf);
      return rc;
    }

  xfree (buf);
  buf = NULL;

  while (fgets (line, sizeof(line), fp))
    {
      if (line[strlen(line)-1] == '\n')
        line[strlen(line)-1] = 0;

      if (!strncmp (line, "agent-socket:", 13))
        {
          buf = str_dup (line+13);
          break;
        }
    }

  ret = pclose (fp);
  if (ret)
    {
      xfree (buf);
      return GPG_ERR_ASS_GENERAL;
    }

  new = xcalloc (1, sizeof (struct agent_s));
  if (!new)
    {
      xfree (buf);
      return GPG_ERR_ENOMEM;
    }

  new->socket = buf;
  *agent = new;
  return 0;
}

gpg_error_t
agent_set_option (struct agent_s * agent, const char *name, const char *value)
{
  return agent_command (agent, NULL, NULL, "OPTION %s=%s", name, value);
}

gpg_error_t
agent_kill_scd (struct agent_s *agent)
{
  gpg_error_t rc = 0;

  if (config_get_boolean (NULL, "kill_scd"))
    {
      rc = agent_command (agent, NULL, NULL, "SCD KILLSCD");
      if (rc && gpg_err_code (rc) != GPG_ERR_NO_SCDAEMON
          && gpg_err_code (rc) != GPG_ERR_EPIPE
          && gpg_err_code (rc) != GPG_ERR_EOF)
	log_write ("%s: ERR %u: %s", __FUNCTION__, rc, pwmd_strerror (rc));
    }

  return rc;
}
