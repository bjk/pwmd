/*
    Copyright (C) 2006-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of pwmd.

    Pwmd is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.

    Pwmd is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Pwmd.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef MUTEX_H
#define MUTEX_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#ifdef HAVE_SYS_TIME_H
#include <sys/time.h>
#endif
#ifdef HAVE_SYS_FILE_H
#include <sys/file.h>
#endif
#ifdef HAVE_FCNTL_H
#include <fcntl.h>
#endif
#include <sys/select.h>
#include <sys/types.h>
#include <pthread.h>
#include <assert.h>
#include <errno.h>
#include <time.h>
#include "rcfile.h"

#ifndef HAVE_PTHREAD_CANCEL
extern pthread_key_t signal_thread_key;

#define TEST_CANCEL() do {						\
    int *cancel = (int *) pthread_getspecific (signal_thread_key);	\
    if (cancel && *cancel)						\
        pthread_exit (NULL);				    		\
} while (0)
#else
#define TEST_CANCEL()
#endif

#define INIT_TIMESPEC(t, ts) do {		\
    long s = (t*100000000)/1000000000;		\
    long l = (t*100000000)%1000000000;		\
    clock_gettime(CLOCK_REALTIME, &ts);		\
    ts.tv_sec += s;				\
    if (ts.tv_nsec + l >= 1000000000) {		\
        ts.tv_sec++;				\
        ts.tv_nsec = 0;				\
    }						\
    else					\
        ts.tv_nsec += l;			\
} while (0)

#ifdef MUTEX_DEBUG
#define MUTEX_LOCK_DEBUG(m) do { \
    log_write("%s(%i): %s(): LOCK %p", __FILE__, __LINE__,	\
              __FUNCTION__, m);					\
} while (0)

#define MUTEX_UNLOCK_DEBUG(m) do { \
    log_write("%s(%i): %s(): UNLOCK %p", __FILE__, __LINE__,	\
              __FUNCTION__, m);				 	\
} while (0)
#else
#define MUTEX_LOCK_DEBUG(m)
#define MUTEX_UNLOCK_DEBUG(m)
#endif

#define MUTEX_LOCK(m) do {				\
    MUTEX_LOCK_DEBUG(m);				\
    while (pthread_mutex_trylock (m) == EBUSY) {	\
        struct timeval tv = { 0, 3000 };		\
        TEST_CANCEL ();					\
        select (0, NULL, NULL, NULL, &tv);		\
    };							\
} while (0)

#define MUTEX_UNLOCK(m) do {		\
    MUTEX_UNLOCK_DEBUG(m);		\
    pthread_mutex_unlock(m);		\
} while (0)

#define TIMED_LOCK_DURATION	100000
#ifdef HAVE_FLOCK
#define TRY_FLOCK(ctx, fd, type, rc) do {				\
    long ka = (long)config_get_integer ("global", "keepalive_interval");\
    long to = config_get_long ("global", "lock_timeout");		\
    for (long elapsed = 0; ; elapsed++) {				\
        struct timeval tv = { 0, TIMED_LOCK_DURATION };			\
        int n = flock (fd, type|LOCK_NB);				\
        if (n == -1 && errno == EWOULDBLOCK) {				\
            if (to == -1 || elapsed >= to) {				\
                rc = GPG_ERR_LOCKED;					\
                break;							\
            }								\
            select (0, NULL, NULL, NULL, &tv);				\
            if (ctx && ka && elapsed && !((elapsed+1*10) % (ka*10))) {	\
                rc = send_status (ctx, STATUS_KEEPALIVE, NULL);		\
                if (rc)							\
                    break;						\
            }								\
            TEST_CANCEL();						\
          }								\
        else if (n == -1)						\
            rc = gpg_error_from_errno (errno);				\
        else								\
            break;							\
    }									\
} while (0)
#else
#define TRY_FLOCK(ctx, fd, type, rc) do {	\
    rc = 0;					\
} while (0)
#endif

#define MUTEX_TIMED_LOCK(ctx, m, timeout, rc) do {			\
    long ka = (long)config_get_integer ("global", "keepalive_interval");\
    for (long elapsed = 0; ; elapsed++) {				\
        struct timeval tv = { 0, TIMED_LOCK_DURATION };			\
        int n = pthread_mutex_trylock (m);				\
        if (n == EBUSY) {						\
            if (timeout == -1 || elapsed >= timeout) {			\
                rc = GPG_ERR_LOCKED;					\
                break;							\
            }								\
            select (0, NULL, NULL, NULL, &tv);				\
            if (ctx && ka && elapsed && !((elapsed+1*10) % (ka*10))) {	\
                rc = send_status (ctx, STATUS_KEEPALIVE, NULL);		\
                if (rc)							\
                    break;						\
            }								\
            TEST_CANCEL();						\
        }								\
        else if (n) {					      		\
            rc = gpg_error_from_errno (n);				\
            break;							\
        }								\
        else {								\
            MUTEX_LOCK_DEBUG(m);					\
            break;							\
        }								\
    }									\
} while (0)

#define MUTEX_TRYLOCK(ctx, m, rc, t) do {			\
    int err = pthread_mutex_trylock (m);			\
    rc = 0;							\
    if (err == EBUSY) {						\
        if (t != -1) {						\
            if (ctx)						\
                rc = send_status(ctx, STATUS_LOCKED, NULL);	\
            if (!rc && t != 0)					\
                MUTEX_TIMED_LOCK(ctx, m, t, rc);		\
            else if (!rc)					\
                rc = GPG_ERR_LOCKED;				\
        }							\
        else							\
            rc = GPG_ERR_LOCKED;				\
    }								\
    else if (err)						\
        rc = gpg_error_from_errno (err);			\
    else							\
        MUTEX_LOCK_DEBUG(m);					\
} while (0)

#define MUTEX_TIMED_RWLOCK(ctx, m, timeout, rc, w) do {			\
    long ka = (long)config_get_integer ("global", "keepalive_interval");\
    for (long elapsed = 0; ; elapsed++) {				\
        struct timeval tv = { 0, TIMED_LOCK_DURATION };			\
        int err;							\
        if (w)								\
            err = pthread_rwlock_trywrlock(m);				\
        else								\
            err = pthread_rwlock_tryrdlock(m);				\
        if (err == EBUSY) {						\
            if (timeout == -1 || elapsed >= timeout) {			\
                rc = GPG_ERR_LOCKED;					\
                break;							\
            }								\
            select (0, NULL, NULL, NULL, &tv);				\
            if (ctx && ka && elapsed && !((elapsed+1*10) % (ka*10))) {	\
                rc = send_status (ctx, STATUS_KEEPALIVE, NULL);		\
                if (rc)							\
                    break;						\
            }								\
            TEST_CANCEL();						\
        }								\
        else if (err) {					      		\
            rc = gpg_error_from_errno (err);				\
            break;							\
        }								\
        else {								\
            MUTEX_LOCK_DEBUG(m);					\
            break;							\
        }								\
    }									\
} while (0)

#endif
