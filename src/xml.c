/*
    Copyright (C) 2006-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of pwmd.

    Pwmd is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.

    Pwmd is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Pwmd.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#include <errno.h>
#include <err.h>
#include <sys/stat.h>
#include <string.h>
#ifdef HAVE_FCNTL_H
#include <fcntl.h>
#endif
#include <ctype.h>
#include <libxml/xmlwriter.h>
#include <wctype.h>
#include <sys/types.h>
#include <pwd.h>

#ifndef _
#include "gettext.h"
#define _(msgid) gettext(msgid)
#endif

#include "pwmd-error.h"
#include "util-misc.h"
#include "xml.h"
#include "mem.h"
#include "rcfile.h"
#include "commands.h"
#include "acl.h"
#include "command-opt.h"

#define XML_LIST_FLAG_CHILDREN	0x0001
#define XML_LIST_HAS_TARGET	0x0002
#define XML_LIST_CHILD_TARGET	0x0004
#define XML_LIST_TARGET_ERROR	0x0008
#define XML_LIST_CHECK		0x0010

const char *reserved_attributes[] = {
    "_name", "_mtime", "_ctime", "_acl", "_target",
    NULL
};

const char *protected_attributes[] = {
    "_name", "_mtime", "_ctime", NULL
};

void log_write (const char *fmt, ...);
static xmlRegexpPtr username_re;

gpg_error_t
xml_init ()
{
  xmlMemSetup (xfree, xmalloc, xrealloc, str_dup);
  xmlInitMemory ();
  xmlInitGlobals ();
  xmlInitParser ();
  xmlXPathInit ();
  username_re = xmlRegexpCompile ((xmlChar *)"[a-zA-Z#][-.a-zA-Z0-9_]*");
  return username_re ? 0 : GPG_ERR_ENOMEM;
}

void
xml_deinit ()
{
  xmlCleanupParser ();
  xmlCleanupGlobals ();
  xmlRegFreeRegexp (username_re);
}

gpg_error_t
xml_protected_attr (const char *a)
{
  int i;

  for (i = 0; protected_attributes[i]; i++)
    {
      if (!strcmp (a, protected_attributes[i]))
        return GPG_ERR_EPERM;
    }

  return 0;
}

int
xml_valid_attribute_value (const char *str)
{
  const char *p = str;

  if (!p || !*p)
    return 1;

  while (*p)
    {
      if (*p++ == '\n')
        return 0;
    }

  return 1;
}

int
xml_valid_attribute (const char *str)
{
  wchar_t *wc;
  size_t len, c;
  int ret = xml_valid_element ((xmlChar *)str);

  if (!ret)
    return ret;

  wc = str_to_wchar ((const char *)str);
  if (!wc)
    return 0;

  len = wcslen (wc);
  for (c = 0; c < len; c++)
    {
      /* Fixes cygwin compile time warning about exceeding the maximum value
       * for the type in the switch() statement below. */
      if (wc[c] >= 0x10000 && wc[c] <= 0xEFFFF)
	continue;

      switch (wc[c])
        {
          case '-':
          case '.':
          case '0' ... '9':
          case 0xB7:
          case 0x0300 ... 0x036F:
          case 0x203F ... 0x2040:
            if (!c)
              {
                xfree (wc);
                return 0;
              }
          case ':': break;
          case '_': break;
          case 'A' ... 'Z': break;
          case 'a' ... 'z': break;
          case 0xC0 ... 0xD6: break;
          case 0xD8 ... 0xF6: break;
          case 0xF8 ... 0x2FF: break;
          case 0x370 ... 0x37D: break;
          case 0x37F ... 0x1FFF: break;
          case 0x200C ... 0x200D: break;
          case 0x2070 ... 0x218F: break;
          case 0x2C00 ... 0x2FEF: break;
          case 0x3001 ... 0xD7FF: break;
          case 0xF900 ... 0xFDCF: break;
          case 0xFDF0 ... 0xFFFD: break;
          default:
            xfree (wc);
            return 0;
        }
    }

  xfree (wc);
  return 1;
}

int
xml_valid_element (xmlChar *str)
{
  wchar_t *wc;
  size_t len, c;

  if (!str || !*str)
    return 0;

  wc = str_to_wchar ((const char *)str);
  if (!wc)
    return 0;

  len = wcslen (wc);
  for (c = 0; c < len; c++)
    {
      if (iswspace(wc[c]))
        {
          xfree (wc);
          return 0;
        }
    }

  xfree (wc);
  return 1;
}

int
xml_valid_element_path (char **path, int with_content)
{
  char **dup = NULL, **p;

  if (!path || !*path)
    return 0;

  /* Save some memory by not duplicating the element content. */
  if (with_content)
    {
      int i, t = strv_length (path);

      for (i = 0; i < t - 1; i++)
	{
	  char **tmp = xrealloc (dup, (i + 2) * sizeof (char *));

	  if (!tmp)
	    {
	      strv_free (dup);
	      return 0;
	    }

	  dup = tmp;
	  dup[i] = str_dup (path[i]);
	  dup[i + 1] = NULL;
	}
    }
  else
    dup = strv_dup (path);

  if (!dup)
    return 0;

  for (p = dup; *p && *(*p); p++)
    {
      if (!xml_valid_element ((xmlChar *) * p))
	{
	  strv_free (dup);
	  return 0;
	}
    }

  strv_free (dup);
  return 1;
}

static gpg_error_t
attr_ctime (struct client_s *client, xmlNodePtr n)
{
  char *buf = str_asprintf ("%li", time (NULL));
  gpg_error_t rc;

  if (!buf)
    return GPG_ERR_ENOMEM;

  rc = xml_add_attribute (client, n, "_ctime", buf);
  xfree (buf);
  return rc;
}

gpg_error_t
xml_acl_check (struct client_s *client, xmlNodePtr n)
{
  gpg_error_t rc = GPG_ERR_EACCES;
  xmlChar *acl = xml_attribute_value (n, (xmlChar *) "_acl");
  char **users = acl ? str_split((char *)acl, ",", 0) : NULL;
  char **cmds = NULL;
  char **p;
  int allowed = 0;

  if (!acl || !*acl || !users || !*users)
    {
      xmlFree (acl);
      strv_free(users);
      return peer_is_invoker(client);
    }

  if (!peer_is_invoker(client))
    {
      xmlFree (acl);
      strv_free(users);
      return 0;
    }

  for (p = users; p && *p; p++)
    {
      char *user = *p;
      int not = 0;

      if (*user == '-' || *user == '!')
        {
          not = 1;
          user++;
        }

      if (*user == '/')
        {
          if (strv_printf (&cmds, "%s%s", not ? "!" : "", user) == 0)
            {
              xmlFree (acl);
              strv_free (cmds);
              strv_free (users);
              return GPG_ERR_ENOMEM;
            }
        }
    }

  for (p = users; p && *p; p++)
    {
      char *user = *p;

      // Skip command name until after user/group/TLS has been determined.
      if (*user == '/' || (*user == '!' && *(user+1) == '/')
          || (*user == '-' && *(user+1) == '/'))
        continue;

#ifdef WITH_GNUTLS
      rc = acl_check_common (client, user,
                             client->thd->remote ? 0 : client->thd->peer->uid,
                             client->thd->remote ? 0 : client->thd->peer->gid,
                             &allowed, NULL);
#else
      rc = acl_check_common (client, user, client->thd->peer->uid,
                             client->thd->peer->gid, &allowed, NULL);
      if (rc)
        break;
#endif
    }

  if (allowed && cmds)
    {
      allowed = 0;
      for (p = cmds; !rc && p && *p && !allowed; p++)
        {
          rc = acl_check_common (client, *p, client->thd->peer->uid,
                                 client->thd->peer->gid, &allowed,
                                 client->thd->cmdname);
        }

      if (!rc && !allowed && !xml_is_element_owner (client, n, 0))
        allowed = 1;
    }

  xmlFree(acl);
  strv_free(users);
  strv_free(cmds);

  if (rc)
    return rc;

  return allowed ? 0 : GPG_ERR_EACCES;
}

static char *
create_acl_user (struct client_s *client)
{
#ifdef WITH_GNUTLS
  if (client->thd->remote)
    return str_asprintf ("#%s", client->thd->tls->fp);
#endif

  return get_username (client->thd->peer->uid);
}

static gpg_error_t
create_new_element (struct client_s *client, int verify, xmlNodePtr parent,
                    const char *name, xmlNodePtr * result)
{
  xmlNodePtr n;
  gpg_error_t rc;
  int root = 0;

  // Allow any client to create a non-existing root element.
  if (parent->parent->type != XML_DOCUMENT_NODE)
    {
      rc = xml_acl_check(client, parent);
      if (rc)
        return rc;
    }
  else
    root = 1;

  n = xmlNewNode (NULL, (xmlChar *) "element");
  if (!n)
    return GPG_ERR_ENOMEM;

  rc = xml_add_attribute (client, n, "_name", name);
  if (!rc)
    rc = attr_ctime (client, n);

  if (!rc && verify && !root)
    rc = xml_is_element_owner (client, parent, 0);

  if (!rc)
    {
      xmlNodePtr p = xmlAddChild (parent, n);
      char *user = create_acl_user (client);

      if (result)
        *result = p;

      if (root || (client->opts & OPT_NO_INHERIT_ACL))
        rc = xml_add_attribute(client, p, "_acl", user);
      else
        {
          /* Inherit the parent _acl attribute and make the current user the
             owner. */
          xmlChar *a = xml_attribute_value (parent, (xmlChar *)"_acl");
          if (a)
            {
              // Do not include an invoking user in the _acl.
              if (peer_is_invoker (client) &&
                  xml_is_element_owner (client, parent, 1))
                {
                  char *tmp = str_asprintf ("%s,%s", user, a);

                  rc = xml_add_attribute(client, p, "_acl", tmp);
                  xfree (tmp);
                }
              else
                rc = xml_add_attribute(client, p, "_acl", (char *)a);
            }

          xmlFree (a);
        }

      xfree (user);
    }
  else
    xmlFreeNode (n);

  return rc;
}

gpg_error_t
xml_new_root_element (struct client_s *client, xmlDocPtr doc, char *name)
{
  xmlNodePtr root = xmlDocGetRootElement (doc);

  if (!name || !root)
    return GPG_ERR_BAD_DATA;

  if (!xml_valid_element ((xmlChar *) name))
    return GPG_ERR_INV_VALUE;

  return create_new_element (client, 0, root, name, NULL);
}

static xmlDocPtr
create_dtd ()
{
  xmlDocPtr doc;
  xmlTextWriterPtr wr = xmlNewTextWriterDoc (&doc, 0);

  if (!wr)
    return NULL;

  if (xmlTextWriterStartDocument (wr, NULL, "UTF-8", "yes"))
    goto fail;

  if (xmlTextWriterStartDTD (wr, (xmlChar *) "pwmd", NULL, NULL) == -1)
    goto fail;

  if (xmlTextWriterWriteDTDElement (wr, (xmlChar *) "pwmd",
				    (xmlChar *) "(element)") == -1)
    goto fail;

  xmlTextWriterEndDTDElement (wr);

  if (xmlTextWriterWriteDTDAttlist (wr, (xmlChar *) "element",
				    (xmlChar *) "_name CDATA #REQUIRED") ==
      -1)
    goto fail;

  xmlTextWriterEndDTDAttlist (wr);
  xmlTextWriterEndDTD (wr);

  if (xmlTextWriterStartElement (wr, (xmlChar *) "pwmd"))
    goto fail;

  xmlTextWriterEndElement (wr);
  xmlTextWriterEndDocument (wr);
  xmlFreeTextWriter (wr);
  return doc;

fail:
  xmlTextWriterEndDocument (wr);
  xmlFreeTextWriter (wr);
  xmlFreeDoc (doc);
  return NULL;
}

xmlDocPtr
xml_new_document ()
{
  return create_dtd ();
}

static xmlNodePtr
find_element_node (xmlNodePtr node)
{
  xmlNodePtr n = node;

  if (n && n->type == XML_ELEMENT_NODE)
    return n;

  for (n = node; n; n = n->next)
    {
      if (n->type == XML_ELEMENT_NODE)
	return n;
    }

  return NULL;
}

xmlNodePtr
xml_resolve_path (struct client_s *client, xmlDocPtr doc, xmlChar * path,
                  char ***result, gpg_error_t *rc)
{
  xmlNodePtr n;
  struct xml_request_s *req;

  *rc = xml_new_request (client, (char *)path, XML_CMD_REALPATH, &req);
  if (*rc)
    return NULL;

  n = xml_find_elements (client, req, xmlDocGetRootElement (doc), rc);
  if (!*rc)
    {
      if (result)
        {
          *result = strv_dup (req->args);
          if (!*result)
            {
              *rc = GPG_ERR_ENOMEM;
              n = NULL;
            }
        }
    }

  xml_free_request (req);
  return n;
}

xmlNodePtr
xml_find_text_node (xmlNodePtr node)
{
  xmlNodePtr n = node;

  if (n && n->type == XML_TEXT_NODE)
    return n;

  for (n = node; n; n = n->next)
    {
      if (n->type == XML_TEXT_NODE)
	return n;
    }

  return NULL;
}

/* Create an element 'path' starting at 'node'. Returns the node of the final
 * created element.
 */
xmlNodePtr
xml_create_element_path (struct client_s *client, struct xml_request_s *req,
                         xmlNodePtr node, char **path, gpg_error_t *rc)
{
  char **p;

  if (!xml_valid_element_path (path, 0))
    {
      *rc = GPG_ERR_INV_VALUE;
      return NULL;
    }

  /* The parent node will be an XML_ELEMENT_NODE. Otherwise we would be
   * creating element content. */
  if (node->type == XML_TEXT_NODE)
    node = node->parent;

  for (p = path; p && *p; p++)
    {
      xmlNodePtr n = NULL;

      if (node != xmlDocGetRootElement (req->doc))
        {
          n = xml_find_element (client, node->children, *p, rc);
          if (*rc && *rc != GPG_ERR_ELEMENT_NOT_FOUND)
            return NULL;

          *rc = 0;
        }

      /*
       * If the found element has the same parent as the current element then
       * they are siblings and the new element needs to be created as a child
       * of the current element (node).
       */
      if (n && n->parent == node->parent)
	n = NULL;

      if (!n)
	{
	  *rc = create_new_element (client, 0, node, *p, &node);
	  if (*rc)
	    return NULL;
	}
      else
	node = n;
    }

  return node;
}

/* Find the first XML_ELEMENT_NODE whose _name attribute matches 'element'. */
xmlNodePtr
xml_find_element (struct client_s *client, xmlNodePtr node, const char *element,
                  gpg_error_t *rc)
{
  xmlNodePtr n;

  *rc = 0;

  if (!node || !element)
    {
      *rc = GPG_ERR_ELEMENT_NOT_FOUND;
      return NULL;
    }

  for (n = node; n; n = n->next)
    {
      if (n->type != XML_ELEMENT_NODE)
	continue;

      xmlChar *a = xml_attribute_value (n, (xmlChar *) "_name");
      if (a && xmlStrEqual (a, (xmlChar *) element))
	{
	  xmlFree (a);
          *rc = xml_acl_check (client, n);
	  return n; // Not NULL since the node may be needed later
	}

      xmlFree (a);
    }

  *rc = GPG_ERR_ELEMENT_NOT_FOUND;
  return NULL;
}

xmlChar *
xml_attribute_value (xmlNodePtr n, xmlChar * attr)
{
  xmlAttrPtr a = xmlHasProp (n, attr);

  if (!a)
    return NULL;

  if (!a->children || !a->children->content)
    return NULL;

  return xmlGetProp (n, attr);
}

gpg_error_t
xml_remove_user_attributes (struct client_s *client, xmlNodePtr n)
{
  gpg_error_t rc = 0;
  xmlAttrPtr a = n->properties;

  while (a)
    {
      if (!xml_reserved_attribute ((const char *)a->name))
        {
          rc = xml_delete_attribute (client, n, a->name);
          if (rc)
            break;

          a = n->properties;
        }
      else
        a = a->next;
    }

  return rc;
}

gpg_error_t
xml_valid_username (const char *str)
{
  if (!str)
    return GPG_ERR_INV_VALUE;

  char **list = str_split (str, ",", 0);
  char **p;

  if (!list)
    return str && *str ? GPG_ERR_ENOMEM : GPG_ERR_INV_VALUE;

  for (p = list; *p; p++)
    {
      const char *t = *p;

      if (*t == '!' || *t == '-')
        t++;

      int e = xmlRegexpExec (username_re, (xmlChar *)t);
      if (e != 1 && *t != '/' && *t != '@') // client command name or group
        {
          strv_free (list);
          return GPG_ERR_INV_USER_ID;
        }
    }

  strv_free (list);
  return 0;
}

gpg_error_t
xml_add_attribute (struct client_s *client, xmlNodePtr node, const char *name,
                   const char *value)
{
  char *buf;
  gpg_error_t rc = 0;
  int is_target = 0;

  if (client && name && !strcmp (name, "_target"))
    {
      rc = xml_is_element_owner (client, node, 0);
      if (rc)
        return rc;

      is_target = 1;
    }
  else if (name && (!strcmp (name, "_expire")
                    || !strcmp (name, "_ctime")
                    || !strcmp (name, "_mtime")))
    {
      char *p;
      time_t e;

      if (!value || !*value)
        return GPG_ERR_INV_VALUE;

      errno = 0;
      e = strtoul (value, &p, 10);
      if (errno || *p || e == ULONG_MAX || *value == '-')
        return GPG_ERR_INV_VALUE;
    }

  /* FIXME: Chained groups like "@group1,!@group2,user" wont work to find the
   * owner of the element. */
  if (client && name && xmlStrEqual ((xmlChar *) name, (xmlChar *) "_acl"))
    if (*value == '@')
      return GPG_ERR_INV_USER_ID;

  if (name && !xmlSetProp (node, (xmlChar *) name, (xmlChar *) value))
    return GPG_ERR_BAD_DATA;

  if (client && name && xmlStrEqual ((xmlChar *) name, (xmlChar *) "_acl"))
    {
      xmlChar *acl = xml_attribute_value (node, (xmlChar *) "_acl");

      if (!acl || !*acl || *acl == '/'
          || ((*acl == '!' || *acl == '-') && *(acl+1) == '/'))
        {
          char *user = create_acl_user (client);

          if (user)
            {
              buf = str_asprintf ("%s,%s", user, acl);

              if (buf)
                rc = xml_add_attribute (client, node, (char *) "_acl", buf);
              else
                rc = GPG_ERR_ENOMEM;

              xfree (buf);
              xfree (user);
            }

          xmlFree (acl);
          return rc;
        }

      xmlFree (acl);
    }

  if (name && xmlStrEqual ((xmlChar *) name, (xmlChar *) "_mtime"))
    return 0;

  if (is_target)
    xml_remove_user_attributes (client, node);

  buf = str_asprintf ("%lu", time (NULL));
  rc = xml_add_attribute (client, node, "_mtime", buf);
  xfree (buf);
  return rc;
}

static gpg_error_t
append_path_to_list (struct client_s *client, struct element_list_s *elements,
                     const char *path, gpg_error_t rc)
{
  struct slist_s *l;
  struct string_s *line;

  (void)client;
  line = string_new (NULL);
  if (!line)
    return GPG_ERR_ENOMEM;

  if (rc == GPG_ERR_ELOOP)
    string_append (line, "O");
  else if (rc == GPG_ERR_EACCES)
    string_append (line, "P");
  else if (rc)
    string_append (line, "E");

  if (rc || elements->target)
    string_prepend (line, " ");

  if ((!rc || rc == GPG_ERR_ELOOP || rc == GPG_ERR_EACCES
       || rc == GPG_ERR_ELEMENT_NOT_FOUND) && elements->target)
    string_append_printf (line, "%sT %s",
                          (!rc && elements->flags & XML_LIST_FLAG_CHILDREN) ? "+" : "",
                          elements->target);
  else if (!rc && (elements->flags & XML_LIST_FLAG_CHILDREN))
    string_prepend (line, " +");

  if (elements->prefix && elements->prefix->str)
    string_prepend (line, elements->prefix->str);
  else
    string_prepend (line, path);

  l = slist_append (elements->list, line->str);
  if (l)
    {
      elements->list = l;
      if (!(elements->flags & XML_LIST_CHECK))
        rc = 0;
    }
  else
    rc = GPG_ERR_ENOMEM;

  string_free (line, 0);
  return rc;
}

/* Removes the final element from the element prefix. */
static void
pop_element_prefix (struct element_list_s *elements)
{
  char *p = strrchr (elements->prefix->str, '\t');

  if (p)
    string_truncate (elements->prefix,
                     elements->prefix->len - strlen (p));
}

#define RESUMABLE_ERROR(rc) (rc == GPG_ERR_ELOOP || rc == GPG_ERR_EACCES \
                             || rc == GPG_ERR_ELEMENT_NOT_FOUND || !rc)

/*
 * From the element path 'path', find sub-nodes and append them to the list.
 */
gpg_error_t
xml_create_path_list (struct client_s *client, xmlDocPtr doc, xmlNodePtr node,
                      struct element_list_s *elements, char *path,
                      gpg_error_t rc)
{
  xmlNodePtr n = NULL;
  struct xml_request_s *req = NULL;
  gpg_error_t trc = rc;
  char *target_orig;

  target_orig = rc && elements->target ? str_dup (elements->target) : NULL;
  rc = xml_new_request (client, path, XML_CMD_LIST, &req);
  if (rc)
    {
      xfree (target_orig);
      return rc;
    }

  req->data = elements;
  n = xml_find_elements (client, req,
                         node ? node : xmlDocGetRootElement (doc), &rc);
  if (!rc)
    elements->flags |=
      find_element_node (n->children) ? XML_LIST_FLAG_CHILDREN : 0;

  if (!rc && trc)
    {
      xfree (elements->target);
      elements->target = target_orig;
      elements->flags = 0;
      elements->flags |= XML_LIST_HAS_TARGET;
    }
  else
    xfree (target_orig);

  rc = append_path_to_list (client, elements, path, trc ? trc : rc);
  xmlFree (elements->target);
  elements->target = NULL;

  if (rc || trc || (elements->flags & XML_LIST_CHECK)
      || (!elements->recurse && elements->depth) || elements->root_only)
    {
      xml_free_request (req);
      return rc;
    }

  elements->flags = 0;

  if (!rc && n && n->children)
    {
      for (n = n->children; n; n = n->next)
        {
          xmlChar *target = NULL;
          xmlNodePtr tmp = NULL;
          xmlChar *name;
          struct string_s *newpath;

          if (n->type != XML_ELEMENT_NODE)
            continue;

          name = xml_attribute_value (n, (xmlChar *) "_name");
          if (!name)
            continue;

          newpath = string_new (NULL);
          if (!newpath)
            {
              xmlFree (name);
              rc = GPG_ERR_ENOMEM;
              goto fail;
            }

          target = xml_attribute_value (n, (xmlChar *) "_target");
          if (target)
            {
              rc = xml_validate_target (client, req->doc, (char *)target, n);
              elements->target = (char *)target;
              elements->flags |= rc ? XML_LIST_TARGET_ERROR : 0;
            }
          else
            tmp = n;

          if (RESUMABLE_ERROR (rc))
            {
              struct string_s *s;

              /* If there is a target for this node then resolve the full path
               * starting at the document root. Otherwise, resolve the next
               * element starting from the child node of the current node. In
               * either case, append the current element in the element path to
               * the prefix of the list string.
               */
              if (target)
                {
                  string_truncate (newpath, 0);
                  s = string_append_printf (newpath, "%s", (char *)target);
                }
              else
                s = string_append (newpath, (char *)name);

              if (!s)
                {
                  xmlFree (name);
                  string_free (newpath, 1);
                  rc = GPG_ERR_ENOMEM;
                  goto fail;
                }

              newpath = s;
              if (!elements->depth)
                {
                  /* There may be a single remaining element in the prefix left
                   * over from the previous truncation (see below). It is safe
                   * to truncate it here since depth == 0. */
                  string_truncate (elements->prefix, 0);
                  s = string_append_printf (elements->prefix, "%s\t%s",
                                            path, name);
                }
              else
                s = string_append_printf (elements->prefix, "\t%s", name);

              if (!s)
                {
                  xmlFree (name);
                  string_free (newpath, 1);
                  rc = GPG_ERR_ENOMEM;
                  goto fail;
                }

              elements->prefix = s;
              elements->depth++;
              rc = xml_create_path_list (client, doc, target ? NULL : tmp,
                                         elements, newpath->str, rc);
              elements->depth--;
              if (!rc)
                pop_element_prefix (elements);
            }

          string_free (newpath, 1);
          xmlFree (name);
          if (rc)
            break;
        }
    }

fail:
  xml_free_request (req);
  return rc;
}

gpg_error_t
xml_recurse_xpath_nodeset (struct client_s *client, xmlDocPtr doc,
                           xmlNodeSetPtr nodes, xmlChar * value,
                           xmlBufferPtr * result, int cmd, const xmlChar * attr)
{
  int i = value ? nodes->nodeNr - 1 : 0;
  xmlBufferPtr buf;

  buf = xmlBufferCreate ();

  if (!buf)
    return GPG_ERR_ENOMEM;

  for (; value ? i >= 0 : i < nodes->nodeNr; value ? i-- : i++)
    {
      xmlNodePtr n = nodes->nodeTab[i];
      gpg_error_t rc;

      if (!n)
	continue;

      if (!value && !attr)
	{
	  if (xmlNodeDump (buf, doc, n, 0, 0) == -1)
	    {
	      *result = buf;
	      return GPG_ERR_BAD_DATA;
	    }

	  continue;
	}

      if (!attr)
	{
	  xmlNodeSetContent (n, value);
	  rc = xml_update_element_mtime (client, n);

	  if (rc)
	    return rc;
	}
      else
	{
	  if (!cmd)
	    rc = xml_add_attribute (client, n, (char *) attr, (char *) value);
	  else
	    rc = xml_delete_attribute (client, n, attr);

	  if (rc)
	    return rc;
	}
    }

  *result = buf;
  return 0;
}

gpg_error_t
xml_delete_attribute (struct client_s *client, xmlNodePtr n,
                      const xmlChar *name)
{
  xmlAttrPtr a;
  gpg_error_t rc = 0;

  if ((a = xmlHasProp (n, name)) == NULL)
    return GPG_ERR_NOT_FOUND;

  if (xmlRemoveProp (a) == -1)
    return GPG_ERR_BAD_DATA;

  if (client && xmlStrEqual (name, (xmlChar *) "_acl"))
    {
      char *user = create_acl_user (client);

      rc = xml_add_attribute (client, n, (char *) "_acl", user);
      xfree (user);

      if (rc)
        return rc;
    }

  return xml_update_element_mtime (client, n);
}

gpg_error_t
xml_validate_import (struct client_s *client, xmlNodePtr node)
{
  gpg_error_t rc = 0;

  if (!node)
    return 0;

  for (xmlNodePtr n = node; n; n = n->next)
    {
      if (n->type == XML_ELEMENT_NODE)
	{
	  if (xmlStrEqual (n->name, (xmlChar *) "element"))
	    {
              xmlChar *a = xmlGetProp (n, (xmlChar *) "_target");

              if (a)
                {
                  xmlFree (a);
                  rc = xml_remove_user_attributes (client, n);
                  if (!rc)
                    rc = xml_unlink_node (client, n->children);
                }

              if (rc)
                break;

	      a = xmlGetProp (n, (xmlChar *) "_name");
	      if (!a)
		{
		  xmlChar *t = xmlGetNodePath (n);

		  log_write (_("Missing attribute '_name' at %s."), t);
		  xmlFree (t);
		  return GPG_ERR_INV_VALUE;
		}

	      if (!xml_valid_element (a))
		{
		  xmlChar *t = xmlGetNodePath (n);

		  log_write (_("'%s' is not a valid element name at %s."), a,
			     t);
		  xmlFree (a);
		  xmlFree (t);
		  return GPG_ERR_INV_VALUE;
		}

	      xmlFree (a);
	      a = xmlGetProp (n, (xmlChar *) "_ctime");
	      if (!a)
		attr_ctime (client, n);

	      xmlFree (a);
	      a = xmlGetProp (n, (xmlChar *) "_mtime");
	      if (!a)
		xml_update_element_mtime (client, n);
	      xmlFree (a);
	    }
	  else
	    {
	      xmlChar *t = xmlGetNodePath (n);
              xmlNodePtr tmp = n->next;

	      log_write (_("Warning: unknown element '%s' at %s. Removing."),
			 n->name, t);
              xmlFree (t);
              (void)xml_unlink_node (client, n);
              return xml_validate_import (client, tmp);
	    }
	}

      if (n->children)
	{
	  rc = xml_validate_import (client, n->children);
	  if (rc)
	    return rc;
	}
    }

  return rc;
}

gpg_error_t
xml_update_element_mtime (struct client_s *client, xmlNodePtr n)
{
  return xml_add_attribute (client, n, NULL, NULL);
}

gpg_error_t
xml_unlink_node (struct client_s *client, xmlNodePtr n)
{
  gpg_error_t rc = 0;

  if (!n)
    return rc;

  if (n->parent)
    rc = xml_update_element_mtime (client, n->parent);

  xmlUnlinkNode (n);
  xmlFreeNodeList (n);
  return rc;
}

static gpg_error_t
recurse_rename_attribute (xmlNodePtr root, const char *old, const char *name)
{
  xmlNodePtr n;
  gpg_error_t rc = 0;

  for (n = root; n; n = n->next)
    {
      xmlAttrPtr attr = xmlHasProp (n, (xmlChar *)old);

      if (attr)
        {
          xmlChar *a = xml_attribute_value (n, (xmlChar *)old);

          if (a && !xmlSetProp (n, (xmlChar *) name, a))
            rc = GPG_ERR_BAD_DATA;

          xmlFree (a);

          if (!rc)
            {
              if (xmlRemoveProp (attr) == -1)
                rc = GPG_ERR_BAD_DATA;
            }
        }

      if (rc)
        break;

      rc = recurse_rename_attribute (n->children, old, name);
      if (rc)
        break;
    }

  return rc;
}

static gpg_error_t
update_to_version (xmlDocPtr doc)
{
  xmlNodePtr root = xmlDocGetRootElement (doc);
  xmlChar *v = xml_attribute_value (root,  (xmlChar *)"_version");
  gpg_error_t rc;

  // Pwmd 3.2.0 or later. No updates needed ATM.
  if (v)
    {
      xmlFree (v);
      return 0;
    }

  rc = recurse_rename_attribute (root->children, "target", "_target");
  if (!rc)
    rc = recurse_rename_attribute (root->children, "expire", "_expire");

  if (!rc)
    rc = recurse_rename_attribute (root->children, "expire_increment", "_age");

  return rc;
}

gpg_error_t
xml_parse_doc (const char *xml, size_t len, xmlDocPtr *result)
{
  xmlDocPtr doc;
  gpg_error_t rc;

  xmlResetLastError ();
  doc = xmlReadMemory (xml, len, NULL, "UTF-8",
                       XML_PARSE_NOBLANKS|XML_PARSE_NONET);
  if (!doc && xmlGetLastError ())
    return GPG_ERR_BAD_DATA;

  *result = doc;
  if (doc)
    rc = update_to_version (doc);
  else
    rc = GPG_ERR_ENOMEM;

  return rc;
}

/* Resolves the 'target' attribute found in 'node' and appends 'path' to the
 * target.  'path' is any remaining elements in the element path after this
 * element 'node' as passed to xml_find_elements() or the initial command. This
 * function may be recursive as it calls xml_find_elements().
 */
static xmlNodePtr
do_realpath (struct client_s *client, struct xml_request_s *req,
             xmlNodePtr node, char **path, const xmlChar *target,
             gpg_error_t *rc) {
  char *line;
  char **result = NULL;
  struct xml_request_s *nreq = NULL;

  *rc = 0;

  result = str_split ((char *)target, "\t", 0);
  if (!result)
    {
      *rc = GPG_ERR_ENOMEM;
      return NULL;
    }

  /* Append the original path to the target path. */
  if (path)
    {
      char **p = strv_catv (result, path);

      strv_free (result);
      if (!p)
        {
          *rc = GPG_ERR_ENOMEM;
          return NULL;
        }

      result = p;
    }

  line = strv_join ("\t", result);
  if (!line)
    {
      strv_free (result);
      *rc = GPG_ERR_ENOMEM;
      return NULL;
    }

  /* To prevent overwriting any original client request flags. We are only
   * interested in the path here. */
  *rc = xml_new_request (client, line, XML_CMD_REALPATH, &nreq);
  xfree (line);
  if (*rc)
    {
      strv_free (result);
      return NULL;
    }

  nreq->depth = req->depth;
  strv_free (result);
  node = xml_find_elements (client, nreq, xmlDocGetRootElement (nreq->doc), rc);

  /* Update the original client request path with the resolved one. */
  if (!*rc)
    {
      strv_free (req->args);
      req->args = nreq->args;
      nreq->args = NULL;
    }

  xml_free_request (nreq);
  return node;
}

#if 0
static char *
node_to_element_path (xmlNodePtr node)
{
  xmlNodePtr n;
  struct string_s *str = string_new ("");
  char *result;

  for (n = node; n; n = n->parent)
    {
      xmlNodePtr child;

      for (child = n; child; child = child->next)
	{
	  if (child->type != XML_ELEMENT_NODE)
	    continue;

	  xmlChar *name = xml_attribute_value (n, (xmlChar *) "_name");
	  if (name)
	    {
	      str = string_prepend (str, (char *) name);
	      xmlFree (name);
	      name = xml_attribute_value (n, (xmlChar *) "_target");
	      if (name)
		str = string_prepend (str, "\t");
	      else
		str = string_prepend (str, "\t!");
	      xmlFree (name);
	    }
	  break;
	}
    }

  str = string_erase (str, 0, 1);
  result = str->str;
  string_free (str, 0);
  return result;
}
#endif

/* Tests if 'path' references 'node' somewhere. */
static gpg_error_t
element_related (struct client_s *client, xmlDocPtr doc, const char *path,
                 xmlNodePtr node, unsigned depth)
{
  gpg_error_t rc = GPG_ERR_NO_DATA;
  char **p, **orig;
  xmlNodePtr n = xmlDocGetRootElement (doc);

  if (max_recursion_depth >= 1 && depth > max_recursion_depth)
    return GPG_ERR_ELOOP;

  orig = p = str_split (path, "\t", 0);
  if (!p)
    return GPG_ERR_ENOMEM;

  for (n = n->children; n && *p; p++, n = n->next)
    {
      xmlNodePtr t;
      xmlChar *target;
      struct string_s *str = NULL;

      t = xml_find_element (client, n, *p, &rc);
      if (rc)
        {
          rc = GPG_ERR_NO_DATA;
          break;
        }

      if (t == node)
        {
          rc = 0;
          break;
        }

      target = xml_attribute_value (t, (xmlChar *)"_target");
      if (!target)
        continue;

      str = string_new ((char *)target);
      xmlFree (target);
      if (!str)
        {
          rc = GPG_ERR_ENOMEM;
          break;
        }

      if (*(p+1))
        {
          while (*(++p))
            {
              struct string_s *tmp;

              tmp = string_append_printf (str, "\t%s", *p);
              if (!tmp)
                {
                  string_free (str, 1);
                  strv_free (p);
                  return GPG_ERR_ENOMEM;
                }

              str = tmp;
            }
        }

      rc = element_related (client, doc, str->str, n->children, ++depth);
      if (rc == GPG_ERR_ELOOP)
        rc = GPG_ERR_NO_DATA;

      string_free(str, 1);
      break;
    }

  strv_free (orig);
  return rc;
}

/*
 * Recurse the element tree beginning at 'node' and find elements who point
 * back to 'dst'. Also follows target attributes.
 */
static gpg_error_t
find_child_to_target (struct client_s *client, xmlDocPtr doc, xmlNodePtr node,
		      xmlNodePtr dst, unsigned depth, int is_target)
{
  xmlNodePtr n;
  gpg_error_t rc = 0;

  if (max_recursion_depth >= 1 && depth > max_recursion_depth)
    return node == dst ? GPG_ERR_ELOOP : 0;

  for (n = node; n; n = n->next)
    {
      xmlChar *target;

      if (n->type != XML_ELEMENT_NODE)
	continue;

      if (n == dst && depth)
	return GPG_ERR_ELOOP;

      target = xml_attribute_value (n, (xmlChar *) "_target");
      if (target)
	{
	  xmlNodePtr tmp;
	  char **result = NULL;

	  tmp = xml_resolve_path (client, doc, target, &result, &rc);
	  strv_free (result);
	  if (!rc)
	    {
	      rc = find_child_to_target (client, doc, tmp, dst, ++depth, 1);
	      depth--;
	    }
          else if (rc == GPG_ERR_ELOOP)
            {
              gpg_error_t trc = element_related (client, doc, (char *)target,
                                                 dst, 0);

              if (trc && trc != GPG_ERR_NO_DATA)
                rc = trc;
              else if (trc == GPG_ERR_NO_DATA)
                rc = 0;
            }

	  xmlFree (target);

	  if (rc && rc != GPG_ERR_ELEMENT_NOT_FOUND)
	    return rc;

          rc = 0;
	  if (is_target)
	    break;

	  continue;
	}

      if (n->children)
	{
	  rc = find_child_to_target (client, doc, n->children, dst, ++depth, 0);
	  depth--;
	  if (rc)
            return rc;
	}

      if (is_target)
	break;
    }

  return rc;
}

static gpg_error_t
find_child_of_parent (xmlDocPtr doc, xmlNodePtr src, xmlNodePtr dst)
{
  xmlNodePtr n;
  gpg_error_t rc = 0;

  for (n = src; n; n = n->next)
    {
      if (n->type != XML_ELEMENT_NODE)
	continue;

      if (n == dst)
	{
	  rc = GPG_ERR_ELOOP;
	  break;
	}

      rc = find_child_of_parent (doc, n->children, dst);
    }

  return rc;
}

static gpg_error_t
find_parent_of_child (xmlDocPtr doc, xmlNodePtr node, xmlNodePtr dst)
{
  xmlNodePtr n;
  gpg_error_t rc = 0;

  (void)doc;
  for (n = node; n; n = n->parent)
    {
      if (n->type != XML_ELEMENT_NODE)
	{
	  xmlNodePtr tmp;

	  for (tmp = n->next; tmp; n = n->next)
	    {
	      if (n->type != XML_ELEMENT_NODE)
		continue;

	      if (tmp == dst)
		return GPG_ERR_ELOOP;
	    }
	}

      if (n == dst)
	return GPG_ERR_ELOOP;
    }

  return rc;
}

void
xml_free_element_list (struct element_list_s *elements)
{
  if (elements)
    {
      if (elements->list)
	{
	  int total = slist_length (elements->list);
	  int i;

	  for (i = 0; i < total; i++)
	    {
	      char *tmp = slist_nth_data (elements->list, i);
	      xfree (tmp);
	    }

	  slist_free (elements->list);
	}

      string_free (elements->prefix, 1);
      xfree (elements->target);
      xfree (elements);
    }
}

/* This behaves like the LIST command with a specified path except it returns
 * an error.
 */
gpg_error_t
xml_check_recursion (struct client_s *client, struct xml_request_s *req)
{
  gpg_error_t rc;
  struct element_list_s *elements;
  char *path = NULL;
  char **dup = strv_dup (req->args);

  if (!dup)
    return GPG_ERR_ENOMEM;


  elements = xcalloc (1, sizeof (struct element_list_s));
  if (!elements)
    {
      rc = GPG_ERR_ENOMEM;
      goto fail;
    }

  path = strv_join ("\t", dup);
  if (!path)
    {
      rc = GPG_ERR_ENOMEM;
      goto fail;
    }

  elements->flags |= XML_LIST_CHECK;
  elements->prefix = string_new (NULL);
  if (!elements->prefix)
    {
      rc = GPG_ERR_ENOMEM;
      goto fail;
    }

  rc = xml_create_path_list (client, req->doc, xmlDocGetRootElement (req->doc),
                             elements, path, 0);

fail:
  xml_free_element_list (elements);
  strv_free (dup);
  xfree (path);
  return rc;
}

gpg_error_t
xml_validate_target (struct client_s *client, xmlDocPtr doc, const char *src,
                     xmlNodePtr dst)
{
  gpg_error_t rc;
  xmlNodePtr src_node;
  char **src_req = NULL;

  src_node = xml_resolve_path (client, doc, (xmlChar *) src, &src_req, &rc);
  if (rc)
    goto fail;

  client->flags |= FLAG_ACL_IGNORE;
  /* A destination element is a child of the source element. */
  rc = find_child_of_parent (doc, src_node->children, dst);
  if (rc)
    goto fail;

  /* The destination element is a parent of the source element. */
  rc = find_parent_of_child (doc, src_node->parent, dst);
  if (rc)
    goto fail;

  /* A destination child element contains a target to the source element. */
  if (dst)
    rc = find_child_to_target (client, doc,
                               dst->children ? dst->children : dst, dst, 0, 0);
  if (rc)
    goto fail;

fail:
  strv_free (src_req);
  client->flags &= ~(FLAG_ACL_IGNORE | FLAG_ACL_ERROR);
  return rc;
}

/* The owner of the element is the first user listed in the _acl attribute
 * list. xml_acl_check() should be called before calling this function. An
 * empty ACL is an error if the client is not an invoking_user. The 'strict'
 * parameter will not check if the current user is an invoking one.
 */
gpg_error_t
xml_is_element_owner (struct client_s *client, xmlNodePtr n, int strict)
{
  xmlChar *acl = xml_attribute_value (n, (xmlChar *) "_acl");
  char **users;
  gpg_error_t rc = GPG_ERR_EACCES;

  if (!acl || !*acl)
    {
      xmlFree (acl);
      return peer_is_invoker (client);
    }

  users = str_split((char *)acl, ",", 0);
  if (users && *users)
    {
      char *user;

#ifdef WITH_GNUTLS
      if (client->thd->remote)
        user = str_asprintf ("#%s", client->thd->tls->fp);
      else
        user = get_username (client->thd->peer->uid);
#else
      user = get_username (client->thd->peer->uid);
#endif

      if (*user == '#')
        rc = !strcasecmp (*users, user) ? 0 : GPG_ERR_EACCES;
      else
        rc = !strcmp (*users, user) ? 0 : GPG_ERR_EACCES;

      if (rc && !strict)
        rc = peer_is_invoker (client);

      xfree (user);
    }

  xmlFree (acl);
  strv_free (users);
  return rc;
}

/* Find elements by matching element names in req->args starting at 'node'.
 * Returns the node of the final element in req->args on success or NULL on
 * failure and sets 'rc' to the error code. Some commands may need special
 * handling and will return the node of the previous found element.
 */
xmlNodePtr
xml_find_elements (struct client_s *client, struct xml_request_s *req,
                    xmlNodePtr node, gpg_error_t *rc)
{
  xmlNodePtr n, last;
  char **p;

  *rc = 0;
  req->depth++;

  if (max_recursion_depth >= 1 && req->depth > max_recursion_depth)
    {
      req->depth--;
      *rc = GPG_ERR_ELOOP;
      return NULL;
    }

  /* Beginning of a command/root element (<pwmd>). */
  if (node == xmlDocGetRootElement (req->doc))
    {
      if (!node->children) // Empty tree
        {
          if (req->cmd == XML_CMD_STORE)
            return xml_create_element_path (client, req, node, req->args, rc);

          *rc = GPG_ERR_ELEMENT_NOT_FOUND;
          req->depth--;
          return NULL;
        }

      node = node->children; // Start searching at the first child of the root
    }

  for (last = n = node, p = req->args; *p; p++)
    {
      xmlNodePtr tmp = NULL;
      struct element_list_s *elements = NULL;

      // FIXME should not be reached in this function?
      if (!*(*p))
	{
	  *rc = GPG_ERR_ELEMENT_NOT_FOUND;
          req->depth--;
	  return NULL;
	}

      n = xml_find_element (client, n, *p, rc);
      if (*rc)
	{
          if (*rc != GPG_ERR_ELEMENT_NOT_FOUND && *rc != GPG_ERR_EACCES)
            {
              req->depth--;
              return NULL;
            }

          if (*rc == GPG_ERR_EACCES
              && (req->cmd == XML_CMD_ATTR_LIST || req->cmd == XML_CMD_ATTR
                  || req->cmd == XML_CMD_ATTR_EXPIRE))
            {
              req->depth--;
              if (*(p+1))
                return NULL;

              *rc = 0;
              return n;
            }

          /* Fixes ATTR LIST when an element does not exist and the parent
           * denies access to children. Fixes leaking information about the
           * current elements children. */
          if (*rc == GPG_ERR_ELEMENT_NOT_FOUND && last != n && last != node)
            {
              gpg_error_t trc = xml_acl_check (client, last);
              if (trc)
                *rc = trc;
            }

          if (*rc == GPG_ERR_ELEMENT_NOT_FOUND)
            {
              if (req->cmd == XML_CMD_STORE || req->cmd == XML_CMD_ATTR_TARGET)
                {
                  *rc = 0;
                  n = xml_create_element_path (client, req,
                                               *p == *req->args
                                               && node->parent == xmlDocGetRootElement (req->doc)
                                               ? node->parent : last, p, rc);
                  if (!*rc)
                    req->flags |= XML_REQUEST_FLAG_CREATED;

                  req->depth--;
                  return n;
                }
            }

          if (req->cmd != XML_CMD_LIST && req->cmd != XML_CMD_REALPATH)
            {
              req->depth--;
              return NULL;
            }

          // Fall through to let LIST flags be appended.
	}

      last = n;

      /* Follow a "_target" attribute for each one found in each element. */
      xmlChar *target = xml_attribute_value (n, (xmlChar *) "_target");
      if (!target || !*target)
        {
          xmlFree (target);

          if (*rc)
            {
              req->depth--;
              return NULL;
            }

          /* This is the final element in req->args. Done. */
          if (!*(p+1))
            {
              req->depth--;
              return n;
            }

          n = n->children;
          continue;
        }

      if (req->cmd == XML_CMD_REALPATH)
        {
          n = do_realpath (client, req, n, p+1, target, rc);
          xmlFree (target);
          req->depth--;
          return n;
        }
      else if (req->cmd == XML_CMD_DELETE || req->cmd == XML_CMD_RENAME
               || req->cmd == XML_CMD_MOVE || req->cmd == XML_CMD_ATTR
               || req->cmd == XML_CMD_ATTR_TARGET
               || req->cmd == XML_CMD_ATTR_LIST)
        {
          /* No more elements to resolve. Return the node with the target. */
          if (!*(p+1))
          {
            xmlFree (target);
            req->depth--;
            return n;
          }
        }
      else if (req->cmd == XML_CMD_LIST)
        {
          elements = req->data;
          if (elements && !(elements->flags & XML_LIST_CHILD_TARGET))
            {
              /* No further elements. Use this target in the list flags. */
              if (!*(p+1))
                elements->flags |= XML_LIST_HAS_TARGET;
              else
                {
                  /* Prevent replacing any following target value with the
                   * current target value. */
                  req->data = NULL;
                }
            }
          else if (!elements)
            {
              struct element_list_s *e = xcalloc (1, sizeof (struct element_list_s));

              e->target = str_dup ((char *)target);
              e->flags |= XML_LIST_CHILD_TARGET;

              if (!*(p+1))
                  e->flags |= XML_LIST_HAS_TARGET;

              req->data = elements = e;
            }

          gpg_error_t rc_orig = *rc;
          *rc = xml_validate_target (client, req->doc, (char *)target, n);
          /* Ignore errors for elements elsewhere in the document. */
          if (*rc == GPG_ERR_EACCES)
            *rc = 0;

          if (rc_orig != *rc)
            *rc = *rc ? *rc : rc_orig;

          if (*rc)
            goto update_target;
        }

      /* Create a new path based on the value of the current "_target"
       * attribute. */
      char **nargs = str_split ((char *)target, "\t", 0);
      if (!nargs)
        {
          xmlFree (target);
          *rc = GPG_ERR_INV_VALUE;
          req->data = req->cmd == XML_CMD_LIST ? elements : req->data;
          req->depth--;
          return NULL;
        }

      /* Append the remaining elements to the target attributes path. */
      char **nnargs = strv_catv (nargs, p+1);
      strv_free (nargs);
      char **orig = req->args;
      req->args = nnargs;
      tmp = xml_find_elements (client, req, xmlDocGetRootElement (req->doc),
                                rc);
      strv_free (nnargs);
      req->args = orig;

update_target:
      if (req->cmd == XML_CMD_LIST && elements)
        {
          /* A child node contained a target. Use this value rather than the
           * current target value as the target value for the list flag. */
          if (req->data && req->data != elements)
            {
              struct element_list_s *e = req->data;

              xmlFree (target);
              target = xmlStrdup ((xmlChar *)e->target);
              xfree (e->target);

              if ((e->flags & XML_LIST_HAS_TARGET))
                elements->flags |= XML_LIST_HAS_TARGET;

              xfree (e);
            }

          req->data = elements;
          if (req->data)
            {
              if (!(elements->flags & XML_LIST_TARGET_ERROR))
                {
                  xfree (elements->target);
                  elements->target = NULL;

                  /* Restore the original target flag. */
                  if (target && (elements->flags & XML_LIST_HAS_TARGET))
                    elements->target = str_dup ((char *)target);
                }
            }
        }

      xmlFree (target);
      req->depth--;
      return *rc ? NULL : tmp;
    }

  req->depth--;
  return n;
}

gpg_error_t
xml_new_request (struct client_s *client, const char *line, xml_command_t cmd,
                 struct xml_request_s **result)
{
  struct xml_request_s *req;
  char **pp = str_split ((char *) line, "\t", 0);

  if (!pp || !*pp)
    {
      strv_free (pp);
      return GPG_ERR_SYNTAX;
    }

  req = xcalloc (1, sizeof (struct xml_request_s));
  if (!req)
    {
      strv_free (pp);
      return GPG_ERR_ENOMEM;
    }

  req->cmd = cmd;
  req->args = pp;
  req->doc = client ? client->doc : NULL;
  *result = req;
  return 0;
}

void
xml_free_request (struct xml_request_s *r)
{
  if (!r)
    return;

  strv_free (r->args);
  xfree (r);
}

int
xml_reserved_attribute (const char *name)
{
  int i;

  for (i = 0; reserved_attributes[i]; i++)
    {
      if (!strcmp (name, reserved_attributes[i]))
        return 1;
    }

  return 0;
}
