/*
    Copyright (C) 2012-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of pwmd.

    Pwmd is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.

    Pwmd is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Pwmd.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <ctype.h>

#include "util-string.h"
#include "mem.h"

#define ALLOC_LARGE 4096

void
string_free (struct string_s *s, int with_data)
{
  if (!s)
    return;

  if (with_data)
    xfree (s->str);

  xfree (s);
}

struct string_s *
string_erase (struct string_s *s, ssize_t pos, ssize_t len)
{
  if (pos > s->len)
    return s;

  if (len == -1)
    {
      s->str[pos] = 0;
      s->len = s->len - pos;
      return s;
    }

  int n = s->len - pos;
  n -= len;
  memmove (&s->str[pos], &s->str[pos + len], n);
  s->len -= len;
  s->str[s->len] = 0;
  return s;
}

/* Be careful about allocations since other string_ functions may
 * realloc the 'str' pointer. */
struct string_s *
string_new_content (char *str)
{
  struct string_s *s = xcalloc (1, sizeof (struct string_s));

  s->str = str;
  s->len = strlen (s->str);
  s->allocated = s->len + 1;
  return s;
}

gpg_error_t
string_large (struct string_s *str)
{
  char *s;

  if (!str)
    return GPG_ERR_INV_ARG;

  s = xrealloc (str->str, (str->len + ALLOC_LARGE) * sizeof (char));
  if (!s)
    return GPG_ERR_ENOMEM;

  str->str = s;
  str->large = 1;
  str->allocated = str->len + ALLOC_LARGE;
  return 0;
}

struct string_s *
string_new (const char *str)
{
  struct string_s *s = xcalloc (1, sizeof (struct string_s));

  if (str)
    {
      s->str = str_dup (str);
      if (!s->str)
	{
	  xfree (s);
	  return NULL;
	}

      s->len = strlen (s->str);
      s->allocated = s->len + 1;
    }

  return s;
}

struct string_s *
string_append (struct string_s *s, const char *str)
{
  size_t len = strlen (str);

  if (s->allocated < len + s->len + 1)
    {
      char *p;
      size_t size = len + s->len + 1;

      size += s->large ? ALLOC_LARGE : 0;

      p = xrealloc (s->str, size * sizeof (char));
      if (!p)
	return NULL;

      s->str = p;
      s->allocated = size;
    }

  memcpy (&s->str[s->len], str, len);
  s->len += len;
  s->str[s->len] = 0;
  return s;
}

struct string_s *
string_truncate (struct string_s *s, size_t n)
{
  if (!s->str)
    return s;

  if (s->len < n)
    return s;

  s->str[n] = 0;
  s->len = n;
  return s;
}

struct string_s *
string_prepend (struct string_s *s, const char *str)
{
  size_t len = strlen (str);

  if (s->allocated < s->len + len + 1)
    {
      size_t size = len + s->len + 1;

      size += s->large ? ALLOC_LARGE : 0;
      char *p = xrealloc (s->str, size * sizeof (char));

      if (!p)
	return NULL;

      s->str = p;
      s->allocated = size;
    }

  memmove (&s->str[len], s->str, s->len);
  memcpy (s->str, str, len);
  s->len += len;
  s->str[s->len] = 0;
  return s;
}

struct string_s *
string_append_printf (struct string_s *s, const char *fmt, ...)
{
  char *buf = NULL;
  size_t len;
  va_list ap;

  va_start (ap, fmt);
  len = str_vasprintf (&buf, fmt, ap);
  va_end (ap);
  if (len == -1)
    return NULL;

  s = string_append (s, buf);
  xfree (buf);
  return s;
}

struct string_s *
string_insert_c (struct string_s *s, ssize_t pos, char c)
{
  size_t len = s->len + 2;

  if (pos >= s->allocated)
    len = pos + 2;

  if (s->allocated < len)
    {
      size_t size = s->large ? len + ALLOC_LARGE : len;
      char *p = xrealloc (s->str, size * sizeof (char));

      if (!p)
	return NULL;

      s->str = p;
      s->len = len;
      s->allocated = size;
    }

  memmove (&s->str[pos + 1], &s->str[pos], s->len - pos);
  s->str[pos] = c;
  s->len = pos + 1 == s->allocated ? pos + 1 : s->len + 1;
  s->str[s->len] = 0;
  return s;
}

int
strv_length (char **a)
{
  int n = 0;

  while (a && *a++)
    n++;

  return n;
}

/* 'str' must already be allocated. */
char **
strv_cat (char **a, char *str)
{
  int len = a ? strv_length (a) : 0;
  char **dst;

  dst = xrealloc (a, (len + 2) * sizeof (char *));
  if (!dst)
    return NULL;

  dst[len++] = str;
  dst[len] = NULL;
  return dst;
}

int
strv_printf (char ***array, const char *fmt, ...)
{
  char **a;
  va_list ap;
  char *buf;
  int ret;

  va_start (ap, fmt);
  ret = str_vasprintf (&buf, fmt, ap);
  va_end (ap);

  if (ret == -1)
    return 0;

  a = strv_cat (*array, buf);
  if (!a)
    {
      xfree (buf);
      return 0;
    }

  *array = a;
  return 1;
}

void
strv_free (char **a)
{
  char **p;

  for (p = a; p && *p; p++)
    xfree (*p);

  xfree (a);
}

char **
strv_dup (char **src)
{
  char **dst = NULL;
  char **p;

  for (p = src; p && *p; p++)
    {
      char **tmp;
      char *xp = str_dup (*p);

      if (!xp)
	{
	  strv_free (dst);
	  return NULL;
	}

      tmp = strv_cat (dst, xp);
      if (!tmp)
	{
	  xfree (xp);
	  strv_free (dst);
	  return NULL;
	}

      dst = tmp;
    }

  return dst;
}

char **
strv_catv (char **dst, char **src)
{
  char **p;
  char **d = NULL;

  if (dst && *dst)
    {
      d = strv_dup (dst);
      if (!d)
	return NULL;
    }

  for (p = src; p && *p; p++)
    {
      char **tmp;
      char *xp = str_dup (*p);

      if (!xp)
	{
	  strv_free (d);
	  return NULL;
	}

      tmp = strv_cat (d, xp);
      if (!tmp)
	{
	  xfree (xp);
	  strv_free (d);
	  return NULL;
	}

      d = tmp;
    }

  return d;
}

static char *
trim (char *str)
{
  size_t len;
  char *p = str;

  if (!str || !*str)
    return str;

  len = strlen (str);
  while (len && isspace (str[--len]))
    str[len] = 0;

  while (p && *p && isspace (*p))
    p++;

  return p;
}

static char **
str_split_common (const char *src, const char *delim, int count, int ws)
{
  char **dst = NULL;
  int index = 0;
  const char *dp = delim;
  char *str = str_dup (src);
  char *p = str;
  size_t dlen = strlen (delim);
  size_t pos = 0, lastpos = 0;

  if (!str || !*str)
    {
      xfree (str);
      return NULL;
    }

  for (; *p; p++)
    {
      if (*p == *dp++)
	{
	  if (!*dp)
	    {
	      char **tmp;
	      char *xp, *s;
	      size_t len = pos - lastpos - dlen + 1;

	      index++;
	      xp = xmalloc (len + 1);
	      if (!xp)
		{
		  strv_free (dst);
                  xfree (str);
		  return NULL;
		}

	      memcpy (xp, &str[lastpos], len);
	      xp[len] = 0;
              s = xp;

              if (ws)
                s = trim (xp);

	      tmp = strv_cat (dst, s);
	      if (!tmp)
		{
		  xfree (xp);
		  strv_free (dst);
                  xfree (str);
		  return NULL;
		}

	      dst = tmp;
	      lastpos = pos + 1;

	      if (count > 0 && index+1 == count && *(p + 1))
		{
		  if (!strv_printf (&dst, "%s", p + 1))
		    {
		      strv_free (dst);
                      xfree (str);
		      return NULL;
		    }

                  xfree (str);
		  return dst;
		}

	      dp = delim;
	    }

	  pos++;
	  continue;
	}

      pos++;
      dp = delim;
    }

  p = str + lastpos;
  if (ws)
    p = trim (p);

  if (!strv_printf (&dst, "%s", p))
    {
      strv_free (dst);
      xfree (str);
      return NULL;
    }

  xfree (str);
  return dst;
}

/* Like str_split() but trims whitespace between 'delim'. */
char **
str_split_ws (const char *str, const char *delim, int count)
{
  return str_split_common (str, delim, count, 1);
}

char **
str_split (const char *str, const char *delim, int count)
{
  return str_split_common (str, delim, count, 0);
}

char *
strv_join (const char *delim, char **a)
{
  char **p;
  char *dst = NULL;
  struct string_s *s = string_new ("");

  if (!s)
    return NULL;

  string_large (s);

  for (p = a; p && *p; p++)
    {
      struct string_s *sp;

      sp = string_append_printf (s, "%s%s", *p, delim
				 && *(p + 1) ? delim : "");
      if (!sp)
	{
	  string_free (s, 1);
	  return NULL;
	}

      s = sp;
    }

  dst = s->str;
  string_free (s, 0);
  return dst;
}

char *
str_down (char *str)
{
  char *p;

  for (p = str; *p; p++)
    {
      if (isascii (*p))
	*p = tolower (*p);
    }

  return str;
}

char *
str_chomp (char *str)
{
  int len = strlen (str);
  char *p = str + len - 1;

  while (len && isspace (*p))
    {
      *p-- = 0;
      len--;
    }

  p = str;
  while (isspace (*p))
    {
      p++;
      len--;
    }

  memmove (str, p, len);
  str[len] = 0;
  return str;
}

char *
str_dup (const char *str)
{
  char *t;
  size_t len;
  register size_t c;

  len = strlen (str);
  t = xmalloc ((len + 1) * sizeof (char));
  if (!t)
    return NULL;

  for (c = 0; c < len; c++)
    t[c] = str[c];

  t[c] = 0;
  return t;
}

char *
str_asprintf (const char *fmt, ...)
{
  va_list ap;
  char *result;
  int len;

  va_start (ap, fmt);
  len = str_vasprintf (&result, fmt, ap);
  va_end (ap);
  return len == -1 ? NULL : result;
}

int
str_vasprintf (char **result, const char *fmt, va_list ap)
{
  char *buf = NULL;
  int len, len2;
  va_list cp;

  va_copy (cp, ap);
  len = vsnprintf (NULL, 0, fmt, cp);
  va_end (cp);

  buf = xmalloc (++len);
  if (!buf)
    return -1;

  va_copy (cp, ap);
  len2 = vsnprintf (buf, len, fmt, cp);
  va_end (cp);
  if (len-1 != len2)
    {
      xfree (buf);
      return -1;
    }
  *result = buf;
  return len2;
}
